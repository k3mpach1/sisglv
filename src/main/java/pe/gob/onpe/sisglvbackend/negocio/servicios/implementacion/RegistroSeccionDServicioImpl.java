package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.CabRegistroSeccionDMapper;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.DetRegistroSeccionDMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionDServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class RegistroSeccionDServicioImpl implements RegistroSeccionDServicio {

	@Autowired
	private DetRegistroSeccionDMapper detRegistroSeccionDMapper;
	
	@Autowired
	private CabRegistroSeccionDMapper cabRegistroSeccionDMapper;
	
	
	@Override
	public void registrarSeccionD(DetRegistroSeccionD param) throws Exception {
				
		detRegistroSeccionDMapper.registrar(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionDServicioImpl.class, "registrarSeccionD");
	}

	@Override
	public void listarSeccionD(DetRegistroSeccionD param) throws Exception {
		
		detRegistroSeccionDMapper.listar(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionDServicioImpl.class, "listarSeccionD");
	}

	@Override
	public void actualizarSeccionD(DetRegistroSeccionD param) throws Exception {
		
		detRegistroSeccionDMapper.actualizar(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionDServicioImpl.class, "actualizarSeccionD");
		
	}

	@Override
	public void listarCabeceraPorVerificacion(CabRegistroSeccionD param) throws Exception {
		cabRegistroSeccionDMapper.listarPorVerificacion(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionDServicioImpl.class, "listarCabeceraPorVerificacion");
	}



	@Override
	public void eliminarArea(DetRegistroSeccionD param) throws Exception {
		detRegistroSeccionDMapper.eliminarArea(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionDServicioImpl.class, "eliminarArea");
		
	}

	@Override
	public void validarSeccioD(CabRegistroSeccionD param) {
		cabRegistroSeccionDMapper.validarSeccionD(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionDServicioImpl.class, "validarSeccioD");

		
	}

	@Override
	public void actualizarEstadoTerminado(CabRegistroSeccionD param) {
		cabRegistroSeccionDMapper.actualizarEstadoTerminado(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionDServicioImpl.class, "actualizarEstadoTerminado");
	}
}
