package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfProcesoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.TabConfProcesoElectoralMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabConfProcesoElectoralServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class TabConfProcesoElectoralServicioImpl implements TabConfProcesoElectoralServicio {

    @Autowired
    private TabConfProcesoElectoralMapper tabConfProcesoElectoralMapper;

    @Override
    public TabConfProcesoElectoral crearTabConfProcesoElectoral(TabConfProcesoElectoral tabConfProcesoElectoral) throws Exception {
        tabConfProcesoElectoralMapper.crearTabConfProcesoElectoral(tabConfProcesoElectoral);
        Funciones.validarOperacionConBaseDatos(tabConfProcesoElectoral, TabConfProcesoElectoralServicioImpl.class, "crearTabConfProcesoElectoral");
        return tabConfProcesoElectoral;
    }

}
