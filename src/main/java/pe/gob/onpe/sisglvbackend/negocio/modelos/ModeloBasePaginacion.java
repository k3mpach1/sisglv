package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;


import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.negocio.beans.Paginacion;

@Getter
@Setter
public class ModeloBasePaginacion extends Paginacion {
	private String estado;
	private String usuarioCreacion;
	private String usuarioModificacion;
	private Date fechaCreacion;
	private Date fechaModificacion;

}
