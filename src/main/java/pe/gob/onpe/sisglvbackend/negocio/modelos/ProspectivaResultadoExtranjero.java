package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProspectivaResultadoExtranjero extends ModeloBase {

	private Long   idProyeccion;
	private String ubigeo;
	private String departamento;
	private String provincia;
	private String distrito;
	private Double desvEst;
	private Double promedio;
	private Double cv;
	private List<ProspectivaResultadoExtranjero> listadoResultadoExtranjero;
}
