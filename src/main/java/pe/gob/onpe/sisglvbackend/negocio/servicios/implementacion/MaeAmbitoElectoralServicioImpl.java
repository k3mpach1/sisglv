package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeAmbitoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeAmbitoElectoralMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeAmbitoElectoralServicio;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotFoundRegisterInDb;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class MaeAmbitoElectoralServicioImpl implements MaeAmbitoElectoralServicio {
	
	@Autowired
	private MaeAmbitoElectoralMapper maeAmbitoElectoralMapper;
	
	@Override
	public void listarAmbitoPorTipo(MaeAmbitoElectoral param) throws Exception {
		// TODO Auto-generated method stub
		maeAmbitoElectoralMapper.listarAmbitoPorTipo(param);
		Funciones.validarOperacionConBaseDatos(param, MaeAmbitoElectoralServicioImpl.class, "listarAmbitoPorTipo");
		
	}

	@Override
	public MaeAmbitoElectoral crearMaeAmbitoElectoral(MaeAmbitoElectoral maeAmbitoElectoral) throws Exception {
		maeAmbitoElectoralMapper.crearMaeAmbitoElectoral(maeAmbitoElectoral);
		Funciones.validarOperacionConBaseDatos(maeAmbitoElectoral, MaeAmbitoElectoralServicioImpl.class, "crearMaeAmbitoElectoral");
		return maeAmbitoElectoral;
	}

	@Override
	public MaeAmbitoElectoral obtenerMaeAmbitoElectoralPorId(Integer idAmbitoElectoral) throws Exception {
		MaeAmbitoElectoral maeAmbitoElectoral = MaeAmbitoElectoral.builder().idAmbitoElectoral(idAmbitoElectoral).build();
		maeAmbitoElectoralMapper.obtenerMaeAmbitoElectoralPorId(maeAmbitoElectoral);
		Funciones.validarOperacionConBaseDatos(maeAmbitoElectoral, MaeAmbitoElectoralServicioImpl.class, "obtenerMaeAmbitoElectoralPorId");
		if(maeAmbitoElectoral.getLista().size() == 0) {
			throw new NotFoundRegisterInDb("MAE_AMBITO_ELECTORAL", "N_AMBITO_ELECTORAL_PK", idAmbitoElectoral);
		}
		return maeAmbitoElectoral.getLista().get(0);
	}

	@Override
	public MaeAmbitoElectoral obtenerMaeAmbitoElectoralPorNombre(String nombre, Integer tipoAmbitoElectoral) throws Exception {
		MaeAmbitoElectoral maeAmbitoElectoral = MaeAmbitoElectoral.builder().nombre(nombre).build();
		maeAmbitoElectoral.setActivo(1);
		maeAmbitoElectoral.setTipoAmbitoElectoral(tipoAmbitoElectoral);
		maeAmbitoElectoralMapper.obtenerMaeAmbitoElectoralPorNombre(maeAmbitoElectoral);
		Funciones.validarOperacionConBaseDatos(maeAmbitoElectoral, MaeAmbitoElectoralServicioImpl.class, "obtenerMaeAmbitoElectoralPorNombre");
		if(maeAmbitoElectoral.getLista().size() == 0) {
			return null;
			//throw new NotFoundRegisterInDb("MAE_AMBITO_ELECTORAL", "C_NOMBRE", nombre);
		}
		return maeAmbitoElectoral.getLista().get(0);
	}

	@Override
	public List<MaeAmbitoElectoral> listarAmbitoElectoralPorProceso(Integer idProcesoElectoral) throws Exception {
		MaeAmbitoElectoral ambitoElectoral = new MaeAmbitoElectoral();
		ambitoElectoral.setIdProcesoElectoral(idProcesoElectoral);
		
		maeAmbitoElectoralMapper.listarAmbitoElectoralPorProcesoElectoral(ambitoElectoral);
		Funciones.validarOperacionConBaseDatos(ambitoElectoral, MaeAmbitoElectoralServicioImpl.class, "listarAmbitoElectoralPorProceso");
		
		return ambitoElectoral.getLista();
	}

	@Override
	public List<MaeAmbitoElectoral> listarAmbitoElectoralPorTipoYProceso(Integer idProcesoElectoral,Integer tipoProceso) throws Exception {
		MaeAmbitoElectoral ambitoElectoral = new MaeAmbitoElectoral();
		ambitoElectoral.setIdProcesoElectoral(idProcesoElectoral);
		ambitoElectoral.setTipoProceso(tipoProceso);
		
		maeAmbitoElectoralMapper.listarAmbitoElectoralPorProcesoTipo(ambitoElectoral);
		Funciones.validarOperacionConBaseDatos(ambitoElectoral, MaeAmbitoElectoralServicioImpl.class, "listarAmbitoElectoralPorTipoYProceso");
		
		return ambitoElectoral.getLista();
	}
}
