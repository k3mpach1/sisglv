package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;


import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeCentroPoblado;

@Mapper
public interface MaeCentroPobladoMapper {
	
	public void listarCentroPobladoPorUbigeo(MaeCentroPoblado centroPoblado) throws Exception;
	public void listarCentroPobladoPorIdUbigeo(MaeCentroPoblado centroPoblado) throws Exception;

}
