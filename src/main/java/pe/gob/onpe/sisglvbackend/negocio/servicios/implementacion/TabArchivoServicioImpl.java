package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabArchivo;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.TabArchivoMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabArchivoServicio;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotFoundRegisterInDb;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

/**
 * @author glennlq
 * @created 9/21/21
 */
@Service
public class TabArchivoServicioImpl implements TabArchivoServicio {

    @Autowired
    TabArchivoMapper tabArchivoMapper;

    @Override
    public TabArchivo crearTabArchivo(TabArchivo tabArchivo) throws Exception {
        tabArchivoMapper.crearTabArchivo(tabArchivo);
        Funciones.validarOperacionConBaseDatos(tabArchivo, TabArchivoServicioImpl.class, "crearTabArchivo");
        return tabArchivo;
    }

    @Override
    public TabArchivo obtenerTabArchivoPorId(Integer idArchivo) throws Exception {
        TabArchivo tabArchivo = TabArchivo.builder().idArchivo(idArchivo).build();
        tabArchivoMapper.obtenerTabArchivoPorId(tabArchivo);
        Funciones.validarOperacionConBaseDatos(tabArchivo, TabArchivoServicioImpl.class, "obtenerTabArchivoPorId");
        if(tabArchivo.getLista().size() == 0)
            throw new NotFoundRegisterInDb("TAB_ARCHIVO", "N_TAB_ARCHIVO_PK", idArchivo);
        return tabArchivo.getLista().get(0);
    }
}
