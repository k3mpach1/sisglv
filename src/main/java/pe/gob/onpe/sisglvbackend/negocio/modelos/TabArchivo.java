package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

import java.util.List;

/**
 * @author glennlq
 * @created 9/21/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabArchivo extends ModeloBase {
    private Integer idArchivo;
    private String guidArchivo;
    private String rutaArchivo;
    private String nombreArchivo;
    private String formatoArchivo;
    private String pesoArchivo;
    private String nombreOriginalArchivo;
    private List<TabArchivo> lista;
}
