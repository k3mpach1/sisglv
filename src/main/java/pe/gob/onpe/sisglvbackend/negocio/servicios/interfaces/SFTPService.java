package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import java.io.InputStream;


public interface SFTPService {

    void uploadFileToFTP(InputStream fileInputStream, String ftpHostDir , String serverFilename);
    void deleteFileToFTP(String dir, String ftpRelativePath);
    byte[] downloadFileFromFTP(String dir, String ftpRelativePath);
  
	
}
