package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCargaArchivo;

/**
 * @author glennlq
 * @created 9/22/21
 */
public interface CabCargaArchivoServicio {
    CabCargaArchivo crearCabCargaArchivo(CabCargaArchivo param) throws Exception;
    CabCargaArchivo getCabCargaArchivoPorId(Integer idCargarArchivo) throws Exception;
}
