package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.beans.ApiLocalVotacion;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Mapper
public interface ApiLocalVotacionMapper {
    void getLocalVotacionGenericoPorPE(ApiLocalVotacion apiLocalVotacion) throws Exception;
}
