package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.*;

/**
 * @author glennlq
 * @created 9/15/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OdpeOcrExcel {
    private String ubigeo;
    private Integer tipo; // 1=ODPE, 2=OCR
    private String tipoStr; // 1=ODPE, 2=OCR
    private String nombre;
    private Integer nro;
}
