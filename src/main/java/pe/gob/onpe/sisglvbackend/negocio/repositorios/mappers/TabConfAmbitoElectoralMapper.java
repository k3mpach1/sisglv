package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaGenerica;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfAmbitoElectoral;

@Mapper
public interface TabConfAmbitoElectoralMapper {
    void crearTabConfAmbitoElectoral(TabConfAmbitoElectoral tabConfAmbitoElectoral) throws Exception;
}
