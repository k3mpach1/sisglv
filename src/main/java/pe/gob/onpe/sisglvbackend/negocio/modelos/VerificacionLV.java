package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VerificacionLV extends ModeloBasePaginacion {
	private Integer idTipoProceso;
	private Integer idVerificacionLV;
	private MaeProcesoVerificacion maeProcesoVerificacion;
	private MaeProcesoElectoral maeProcesoElectoral;
	private MaeAmbitoElectoral maeAmbitoElectoral;
	private MaeUbigeo maeUbigeo;
	private MaeUbigeo maeUbigeoNivel1;//Departamento
	private MaeUbigeo maeUbigeoNivel2;//Provincia
	private MaeUbigeo maeUbigeoNivel3;//Distrito
	private MaeCentroPoblado maeCentroPoblado;
	private MaeLocalVotacion maeLocalVotacion;
	private Integer estadoAvanceSeccionA;
	private Integer estadoAvanceSeccionB;
	private Integer estadoAvanceSeccionC;
	private Integer estadoAvanceSeccionD;
	private Integer seleccionETLV;
	private Integer seleccionJornadaCap01;
	private Integer seleccionJornadaCap02;
	private Integer seleccionJEL;
	private String codigoUbigeoNivel1;
	private String codigoUbigeoNivel2;
	private String codigoUbigeoNivel3;
	private String obsEstadoDisponible;
        
        private Integer estadoDisponible;
        private String nombreEstadoDisponible;
        
	private Integer estadoVerificacion;
        private String nombreEstadoLV;
        
	
	private RegistroSeccionA registroSeccionA;
	private RegistroSeccionB registroSeccionB;
	private CabRegistroSeccionC registroSeccionC;
	private CabRegistroSeccionD registroSeccionD;
	private CabRegistroSeccionE registroSeccionE;

        private Integer localesRegistrando;
        private Integer localesPorValidar;
        private Integer localesValidados;
        private Integer localesObsevados;
        private Integer totalLocales;


	private List<VerificacionLV> verificaciones;
}
