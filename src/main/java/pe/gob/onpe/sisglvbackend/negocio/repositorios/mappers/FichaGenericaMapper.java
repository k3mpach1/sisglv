package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaGenerica;

/**
 * @author glennlq
 * @created 9/27/21
 */
@Mapper
public interface FichaGenericaMapper {
    void copiarFichasABCDE(FichaGenerica fichaGenerica) throws Exception;
    void copiarFichasABCDEPorLocalVotacion(FichaGenerica fichaGenerica) throws Exception;
}
