package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormtatoPlantilla extends ModeloBase {
	
	private Integer id;
	private Integer tipoPlantilla;
	private String cabecera;
	private String columna;
	private String criterioValidacion;
	private String mensajeValidacion;
	private String comentario;
	private Integer indicadorUnico;
	private Integer obligatorio;
	private Integer orden;
	
	

}
