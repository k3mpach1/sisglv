package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ModeloBase;

import java.util.List;

/**
 * @author glennlq
 * @created 1/6/22
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReporteSeccionE extends ModeloBase {
    private String codigoHistoricoLV;
    private String nombreLv;
    private String categoria;
    private String observacionLv;
    List<ReporteSeccionE> lista;
}
