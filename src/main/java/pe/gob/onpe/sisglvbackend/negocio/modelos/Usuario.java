package pe.gob.onpe.sisglvbackend.negocio.modelos;


import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Usuario extends ModeloBase {
	
	private Integer idUsuario;
	private Integer superAdmin;
	private Integer tipoAutenticacion;
	private Integer claveNueva;
	private String usuario;
	private String claveTemporal;
	private String clave;
	//private Persona persona;
	private Aplicacion aplicacion;
	//private Perfil perfil;
	
	private Integer sesionUnica;

	private Integer envioCorreo;
	

	private String token;
	
	//private AplicacionUsuario aplicacionUsuario;

	
	private List<Usuario> usuarios = new ArrayList<>();
	
	//private List<UsuarioPerfil> usuariosPerfiles = new ArrayList<>();
	private Usuario oUsuario;
	
	//private List<Perfil> perfiles = new ArrayList<>();
	///private List<Modulo> modulos = new ArrayList<>();
	
	
	///private List<Opcion> opciones = new ArrayList<>();	
	
	
	//private List<Accion> acciones = new ArrayList<>();
	
	
	//private UnidadOrganica unidadOrganica;
	private Integer personaAsignada;
	//private ProcesoElectoral procesoElectoral;
	
    private Integer idTipoGeneracion;
    
    private UsuarioSesion usuarioSesion;

}
