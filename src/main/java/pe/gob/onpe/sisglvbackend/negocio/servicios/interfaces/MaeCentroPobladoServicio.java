package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeCentroPoblado;

public interface MaeCentroPobladoServicio {

	public void listarCentroPobladoPorUbigeo(MaeCentroPoblado centroPoblado) throws Exception;
	public void listarCentroPobladoPorIdUbigeo(MaeCentroPoblado centroPoblado) throws Exception;
}
