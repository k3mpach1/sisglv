package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfProcesoElectoral;

@Mapper
public interface TabConfProcesoElectoralMapper {
    void crearTabConfProcesoElectoral(TabConfProcesoElectoral tabConfProcesoElectoral) throws Exception;
    
}
