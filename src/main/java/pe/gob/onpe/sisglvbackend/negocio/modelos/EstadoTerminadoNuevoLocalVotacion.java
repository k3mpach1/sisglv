package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

import java.util.List;

/**
 * @author glennlq
 * @created 12/14/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstadoTerminadoNuevoLocalVotacion extends ModeloBase{
    Integer idVerificacionLv;
    Integer terminadoA;
    Integer terminadoB;
    Integer terminadoC;
    Integer terminadoD;
    Integer termiandoE;
    List<EstadoTerminadoNuevoLocalVotacion> lista;
}
