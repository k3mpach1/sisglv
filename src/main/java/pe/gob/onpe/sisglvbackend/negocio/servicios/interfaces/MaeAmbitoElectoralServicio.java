package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import java.util.List;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeAmbitoElectoral;

public interface MaeAmbitoElectoralServicio {
	
	public void listarAmbitoPorTipo(MaeAmbitoElectoral param) throws Exception;
	MaeAmbitoElectoral crearMaeAmbitoElectoral(MaeAmbitoElectoral maeAmbitoElectoral) throws Exception;
	MaeAmbitoElectoral obtenerMaeAmbitoElectoralPorId(Integer idAmbitoElectoral) throws Exception;
	MaeAmbitoElectoral obtenerMaeAmbitoElectoralPorNombre(String nombre, Integer tipoAmbitoElectoral) throws Exception;
	List<MaeAmbitoElectoral> listarAmbitoElectoralPorProceso(Integer idProcesoElectoral) throws Exception;
	List<MaeAmbitoElectoral> listarAmbitoElectoralPorTipoYProceso(Integer idProcesoElectoral, Integer tipoProceso) throws Exception;
}
