package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;

@Mapper
public interface MaeProcesoElectoralMapper {
	List<MaeProcesoElectoral> listarActivos(MaeProcesoElectoral param);
	void crearProcesoElectoral(MaeProcesoElectoral param);
	void actualizarEstadoProcesoElectoral(MaeProcesoElectoral param);
	void getProcesoElectoralById(MaeProcesoElectoral param);
	
	void getProcesoElectoralByAcronimoNombre(MaeProcesoElectoral param);
}
