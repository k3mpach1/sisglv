package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfProcesoElectoral;

public interface TabConfProcesoElectoralServicio {
    TabConfProcesoElectoral crearTabConfProcesoElectoral(TabConfProcesoElectoral tabConfProcesoElectoral) throws Exception;
}
