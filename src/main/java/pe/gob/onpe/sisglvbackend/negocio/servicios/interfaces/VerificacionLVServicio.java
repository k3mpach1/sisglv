package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.EstadoTerminadoNuevoLocalInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.GenericoOutputDto;

import java.util.List;

public interface VerificacionLVServicio {

    public List<VerificacionLV> listarVerificacion(VerificacionLV param) throws Exception;

    public void enviarAValidacionLV(VerificacionLV param) throws Exception;

    public void listarEditarNuevoLocalVotacion(VerificacionLV param) throws Exception;
    
    public void listarEditarVerificacionLocalVotacion(VerificacionLV param) throws Exception;


    public void listarValidarNuevoLocalVotacion(VerificacionLV param) throws Exception;

    public void validarNuevoRegistroLV(VerificacionLV param) throws Exception;
    
    public void listarLocalVotacionVerificacion(VerificacionLV param) throws Exception;

    public void totalLocalesVotacion(VerificacionLV param) throws Exception;

    public void listarLocalVotacionValidacion(VerificacionLV param) throws Exception;

    public void totalLocalesValidacion(VerificacionLV param) throws Exception;
    
    public void listarValidarLocalVotacionHistorico(VerificacionLV param) throws Exception;
    
    
    public void validarVerificacionLV(VerificacionLV param) throws Exception;
    
    public void actualizarEstadoDisponiblel(VerificacionLV param) throws Exception;
    
    public void cancelarRegistroVerificacion(VerificacionLV param) throws Exception;

    GenericoOutputDto obtenerEstadoTerminadoNuevoLocal(EstadoTerminadoNuevoLocalInputDto param) throws Exception;

}

