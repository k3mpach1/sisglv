package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import java.util.List;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.AsignarOdpeAProcesoElectoralDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.ProcesarArchivoOdpesDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.AsignarOdpeAProcesoElectoralOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.CargarArchivoOdpesOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.ProcesarArchivoOdpesOutputDto;

public interface ProcesoElectoralServicio {
	List<MaeProcesoElectoral> listarActivos(MaeProcesoElectoral param);

	MaeProcesoElectoral crearProcesoElectoral(MaeProcesoElectoral maeProcesoElectoral, String numeroDocumento);

	MaeProcesoElectoral getProcesoElectoralById(Integer idProcesoElectoral) throws Exception;

	ProcesarArchivoOdpesOutputDto cargarArchivoOdpesAFtp(Integer idProcesoElectoral, MultipartFile multipartFile, String numeroDocumento) throws Exception;

	ProcesarArchivoOdpesOutputDto procesarArchivoOdpes(ProcesarArchivoOdpesDto procesarArchivoOdpesDto, String numeroDocumento) throws Exception;

	@Async("threadPoolTaskExecutor")
	AsignarOdpeAProcesoElectoralOutputDto asignarOdpesAProcesoElectoral(AsignarOdpeAProcesoElectoralDto asignarOdpeAProcesoElectoralDto,
																		String token, String numeroDocumento) throws Exception;

	BaseOutputDto registrarProcesoAsignacionOdpeEnSasa(AsignarOdpeAProcesoElectoralDto asignarOdpeAProcesoElectoralDto,
			String token) throws Exception;

	MaeProcesoElectoral getProcesoElectoralPorAcronimoYNombre(String acronimo, String nombre) throws Exception;
}
