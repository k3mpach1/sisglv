package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiBitacora;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.ApiBitacoraMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiBitacoraServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;


/**
 * @author glennlq
 * @created 10/26/21
 */
@Service
public class ApiBitacoraServicioImpl implements ApiBitacoraServicio {

    @Autowired
    ApiBitacoraMapper apiBitacoraMapper;

    @Override
    public void insertarApiBitacora(ApiBitacora apiBitacora) throws Exception {
        apiBitacoraMapper.insertarApiBitacora(apiBitacora);
        Funciones.validarOperacionConBaseDatos(apiBitacora, ApiBitacoraServicioImpl.class, "insertarApiBitacora");
    }

    @Override
    public void insertarApiBitacora(ApiToken apiToken, Integer estado, String descripcion) throws Exception {
        ApiBitacora apiBitacora = ApiBitacora.builder()
                .apiUsuario(apiToken.getApiUsuario())
                .apiToken(apiToken)
                .estadoResultado(estado)
                .descripcionResultado(descripcion).build();
        apiBitacora.setUsuarioCreacion("ADMIN");
        this.insertarApiBitacora(apiBitacora);
    }
}
