package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.api.ApiLocalVotacionInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiBasicOutputDto;

/**
 * @author glennlq
 * @created 10/21/21
 */
public interface ApiLocalVotacionServicio {
    ApiBasicOutputDto getLocalVotacionPorProcesoElectoral(ApiToken apiToken, ApiLocalVotacionInputDto apiLocalVotacionInputDto) throws Exception;
}
