package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;

@Mapper
public interface UbigeoMapper {
    void listarDepartamentosActivos(MaeUbigeo param) throws Exception;
    void listarProvinciasActivos(MaeUbigeo param) throws Exception;
    void listarDistritosActivos(MaeUbigeo param) throws Exception;
}
