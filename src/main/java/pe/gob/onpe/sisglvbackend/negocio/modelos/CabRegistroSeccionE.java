package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabRegistroSeccionE extends ModeloBase{
	
	private Integer idCabRegistroSeccionE;
	private String  seccion;
	private Integer nVerificacion;
	private String  validado;
	private Integer idCategoriaSeleccionada;
	private Integer idArchivoConsultado;
	private Integer activo;
	private String observacionRegistro;
	private String observacionLv;
	private ArchivoLV archivoAgregado;
	private Integer idEstado;
	private Integer terminado;
	private Integer observado;
	private VerificacionLV verificacionLv;
	private Integer estadoVerificacion;
	private List<DetRegistroSeccionE> registrosDetSeccionE;
}
