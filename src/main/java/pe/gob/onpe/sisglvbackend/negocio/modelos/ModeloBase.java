package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;
import pe.gob.onpe.sisglvbackend.negocio.beans.Mensaje;

@Getter
@Setter
public class ModeloBase extends Mensaje {

	private String estado;
	private String usuarioCreacion;
	private String usuarioModificacion;
	private Date fechaCreacion;
	private Date fechaModificacion;
	private Integer activo;
	


}
