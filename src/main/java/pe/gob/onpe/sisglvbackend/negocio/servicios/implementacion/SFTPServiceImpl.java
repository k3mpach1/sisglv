package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Vector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.SFTPService;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.CustomException;
import pe.gob.onpe.sisglvbackend.transversal.properties.FtpProperties;

@Service
public class SFTPServiceImpl implements SFTPService {

	@Autowired
	private FtpProperties ftpProperties;

//	@Autowired
//	private Environment env;
//	@Value("${MODE_DEV}")
//	protected String MODE_DEV;
//	@Value("${FTP_IPSERVER}")
//    protected String FTP_IPSERVER;
//	@Value("${FTP_USUARIO}")
//    protected String FTP_USUARIO;
//	@Value("${FTP_PASSWORD}")
//    protected String FTP_PASSWORD;
//	@Value("${FTP_PORT}")
//    protected String FTP_PORT;
//	@Value("${FTP_DIRHOME}")
//    protected String FTP_DIRHOME;
	
	@Override
	public void uploadFileToFTP(InputStream fileInputStream, String ftpHostDir, String serverFilename) {
		String directorio = "";
		String ipserver = "";
		String port = "";
		String user = "";
		String password = "";
		String home = "";
		
//		if (MODE_DEV == null || MODE_DEV.equals("false")) {
//			ipserver = env.getProperty("FTP_IPSERVER");
//			port = env.getProperty("FTP_PORT");
//			user = env.getProperty("FTP_USUARIO");
//			password = env.getProperty("FTP_PASSWORD");
//			home = env.getProperty("FTP_DIRHOME");
//		} else {
//			ipserver = FTP_IPSERVER;
//			port = FTP_PORT;
//			user = FTP_USUARIO;
//			password = FTP_PASSWORD;
//			home = FTP_DIRHOME;
//		}

		ipserver = ftpProperties.getFtpIpServer();
		port = ftpProperties.getFtpPort();
		user = ftpProperties.getFtpUsuario();
		password = ftpProperties.getFtpPassword();
		home = ftpProperties.getFtpDirhome();
		
		Session session = null;
		Channel channel = null;
		ChannelSftp c = null;

		JSch jsch = new JSch();

		try {

			if (null == session) {

				session = jsch.getSession(user, ipserver, Integer.parseInt(port));

				session.setPassword(password);

				Properties config = new Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.connect();

			}

			channel = session.openChannel("sftp");
			channel.connect();
			c = (ChannelSftp) channel;
			
			directorio = home + ftpHostDir;
			
			try {
				Vector<?> content = c.ls(directorio);
				if(content == null){
					c.mkdir(directorio);
				}
			} catch (SftpException e) {
				c.mkdir(directorio);
			}
			
			
			c.cd(directorio);
			
			c.put(fileInputStream, serverFilename);
			

		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} catch (JSchException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} catch (SftpException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} finally {
			if (session != null)
				session.disconnect();
			if (session != null)
				session.disconnect();

			if (channel != null)
				channel.disconnect();

			if (c != null)
				c.disconnect();

		}
	}

	@Override
	public byte[] downloadFileFromFTP(String dir,String ftpRelativePath){

		Session session = null;
		Channel channel = null;
		ChannelSftp c = null;
		InputStream is = null;
		byte[] resultado = null;

		JSch jsch = new JSch();
		String ipserver = "";
		String port = "";
		String user = "";
		String password = "";
		String home = "";
//		if (MODE_DEV == null || MODE_DEV.equals("false")) {
//			ipserver = env.getProperty("FTP_IPSERVER");
//			port = env.getProperty("FTP_PORT");
//			user = env.getProperty("FTP_USUARIO");
//			password = env.getProperty("FTP_PASSWORD");
//			home = env.getProperty("FTP_DIRHOME");
//		}else {
//			ipserver = FTP_IPSERVER;
//			port = FTP_PORT;
//			user = FTP_USUARIO;
//			password = FTP_PASSWORD;
//			home = FTP_DIRHOME;
//		}
		ipserver = ftpProperties.getFtpIpServer();
		port = ftpProperties.getFtpPort();
		user = ftpProperties.getFtpUsuario();
		password = ftpProperties.getFtpPassword();
		home = ftpProperties.getFtpDirhome();
		
		try {
			if (null == session) {
				session = jsch.getSession(user, ipserver, Integer.parseInt(port));
				session.setPassword(password);
				Properties config = new Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.connect();
			}
			channel = session.openChannel("sftp");
			channel.connect();
			c = (ChannelSftp) channel;
			resultado = null;
			c.cd(home+dir);
			try {
	             //Abrir el archivo
				is = c.get(ftpRelativePath);
	         }catch (SftpException e){
	             resultado = null;
	             return resultado;
	         }
			if(is!=null) {
				ByteArrayOutputStream buffer = new ByteArrayOutputStream();
				byte[] data = new byte[1024];
				int nRead;
				while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
				}
				buffer.flush();
				resultado = buffer.toByteArray();
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} catch (JSchException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} catch (SftpException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		}finally {
			if (session != null)
				session.disconnect();
			if (session != null)
				session.disconnect();

			if (channel != null)
				channel.disconnect();

			if (c != null)
				c.disconnect();

		}
		
		return resultado;

	}
	
	
	@Override
	public void deleteFileToFTP(String dir, String nombreArchivo) {
//		System.out.println("El FTP_IPSERVER es...:"+FTP_IPSERVER);
		String ipserver = "";
		String port = "";
		String user = "";
		String home = "";
		String password = "";
//		if (MODE_DEV == null || MODE_DEV.equals("false")) {
//			ipserver = env.getProperty("FTP_IPSERVER");
//			port = env.getProperty("FTP_PORT");
//			user = env.getProperty("FTP_USUARIO");
//			password = env.getProperty("FTP_PASSWORD");
//			home = env.getProperty("FTP_DIRHOME");
//		}else {
//			ipserver = FTP_IPSERVER;
//			port = FTP_PORT;
//			user = FTP_USUARIO;
//			password = FTP_PASSWORD;
//			home = FTP_DIRHOME;
//		}
		ipserver = ftpProperties.getFtpIpServer();
		port = ftpProperties.getFtpPort();
		user = ftpProperties.getFtpUsuario();
		password = ftpProperties.getFtpPassword();
		home = ftpProperties.getFtpDirhome();
		
		Session session = null;
		Channel channel = null;
		ChannelSftp c = null;

		JSch jsch = new JSch();

		try {

			if (null == session) {

				session = jsch.getSession(user, ipserver, Integer.parseInt(port));

				session.setPassword(password);

				Properties config = new Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.connect();

			}

			channel = session.openChannel("sftp");
			channel.connect();
			c = (ChannelSftp) channel;
			
			c.cd(home+dir);

			c.rm(nombreArchivo);
			

		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} catch (JSchException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} catch (SftpException e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage());
		} finally {
			if (session != null)
				session.disconnect();
			if (session != null)
				session.disconnect();

			if (channel != null)
				channel.disconnect();

			if (c != null)
				c.disconnect();

		}

	}


}
