package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfProcesoVerificacion;

public interface TabConfProcesoVerificacionServicio {
    TabConfProcesoVerificacion crearTabConfProcesoVerificacion(TabConfProcesoVerificacion tabConfProcesoVerificacion) throws Exception;
}
