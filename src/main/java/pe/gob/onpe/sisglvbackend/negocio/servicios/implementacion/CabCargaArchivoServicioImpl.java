package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCargaArchivo;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.CabCargaArchivoMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CabCargaArchivoServicio;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotFoundRegisterInDb;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

/**
 * @author glennlq
 * @created 9/22/21
 */
@Service
public class CabCargaArchivoServicioImpl implements CabCargaArchivoServicio {

    @Autowired
    private CabCargaArchivoMapper cabCargaArchivoMapper;

    @Override
    public CabCargaArchivo crearCabCargaArchivo(CabCargaArchivo param) throws Exception {
        cabCargaArchivoMapper.crearCabCargaArchivo(param);
        Funciones.validarOperacionConBaseDatos(param, CabCargaArchivoServicioImpl.class, "crearCabCargaArchivo");
        return param;
    }

    @Override
    public CabCargaArchivo getCabCargaArchivoPorId(Integer idCargarArchivo) throws Exception {
        CabCargaArchivo cabCargaArchivo = CabCargaArchivo.builder().idCargarArchivo(idCargarArchivo).build();
        cabCargaArchivoMapper.obtenerCabCargaArchivoPorId(cabCargaArchivo);
        Funciones.validarOperacionConBaseDatos(cabCargaArchivo, CabCargaArchivoServicioImpl.class, "getCabCargaArchivoPorId");
        if(cabCargaArchivo.getLista().size() == 0)
            throw new NotFoundRegisterInDb("CAB_CARGA_ARCHIVO", "N_CARGA_ARCHIVO_PK", idCargarArchivo);
        return cabCargaArchivo.getLista().get(0);
    }
}
