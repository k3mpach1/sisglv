package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FichaSeccionB extends ModeloBase{
	
	private Integer idFichaSeccionB;
	private MaeLocalVotacion maeLocalVotacion;
	private String seccion;
	private Integer tieneCercoPerimetrico;
	private Integer idEstadoCercoPerimetrico;
	private String nombreEstadoCercoPerimetrico;
	private Integer cantidadSshh;
	private Integer idEstadoSshh;
	private String nombreEstadoSshh;

	private Integer cantidadPuertaAcceso;
	private Integer idEstadoPuestaAcceso;
	private String nombreEstadoPuestaAcceso;

	private Integer idEstadoPuertaAula;
	private String nombreEstadoPuertaAula;

	private Integer idEstadoTechoAula;
	private String nombreEstadoTechoAula;

	private Integer idEstadoParedAula;
	private String nombreEstadoParedAula;

	private Integer idEstadoVentanaAula;
	private String nombreEstadoVentanaAula;

	private Integer idEstadoPisoAula;
	private String nombreEstadoPisoAula;


	private Integer tieneAgua;
	private String horarioInicioAgua;
	private String HorarioTerminoAgua;
	private Integer tieneLuz;
	private String horarioInicioLuz;
	private String HorarioTerminoLuz;
	private Integer tieneInternet;
	private String nombreProveedorInternet;

	private Integer cantidadCirculoSeguridad;
	private String suministroLuz;
	private Integer tienePatio;
	private Integer cantidadPatio;

	private String usuarioAprobacion;
	private Date fechaAprobacion;

	private Float areaAproximada;
	private Float areaConstruida;
	private Float areaSinContruir;

}
