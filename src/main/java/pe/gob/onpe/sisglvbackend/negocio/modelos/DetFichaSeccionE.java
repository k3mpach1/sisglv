package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetFichaSeccionE extends ModeloBase{
    
    private Integer idDetFichaSeccionE;
    private CabFichaSeccionE cabFichaSeccionE;
    private Integer archivo;
    private Integer categoria;
    private String detalle;
    
    private List<DetFichaSeccionE> lista;
        
}
