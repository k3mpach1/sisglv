package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.beans.ApiOdpe;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.ApiOdpeMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiBitacoraServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiOdpeServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiTokenServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.api.ApiOdpeInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiBasicOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiOdpeOutputDto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Service
public class ApiOdpeServicioImpl implements ApiOdpeServicio {

    @Autowired
    ApiOdpeMapper apiOdpeMapper;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    ApiTokenServicio apiTokenServicio;

    @Autowired
    ApiBitacoraServicio apiBitacoraServicio;

    @Override
    public ApiBasicOutputDto getOdpePorProcesoElectoral(ApiToken apiToken, ApiOdpeInputDto apiOdpeInputDto) throws Exception {
        ApiOdpe apiOdpe = ApiOdpe.builder()
                .acronimo(apiOdpeInputDto.getAcronimo())
                .apiToken(apiToken)
                .build();
        try {
            apiOdpeMapper.getOdpePorProcesoElectoral(apiOdpe);
            Funciones.validarOperacionConBaseDatos(apiOdpe, ApiOdpeServicioImpl.class, "getLocalVotacionGenericoPorPE");
            apiBitacoraServicio.insertarApiBitacora(apiToken, 1, "OK");
            List<ApiOdpeOutputDto> lista = buildListOdpeOutputDto(apiOdpe);
            return ApiBasicOutputDto.builder()
                    .estado(true)
                    .codigoError(0)
                    .mensaje("")
                    .cantidad(lista.size())
                    .resultado(lista)
                    .build();
        } catch (Exception e) {
            apiBitacoraServicio.insertarApiBitacora(apiToken, 0, e.getMessage().substring(0,255));
            return ApiBasicOutputDto.builder()
                    .estado(false)
                    .codigoError(1)
                    .mensaje(e.getMessage())
                    .resultado(null)
                    .build();
        }
    }


    private List<ApiOdpeOutputDto> buildListOdpeOutputDto(ApiOdpe apiOdpe) throws Exception {
        List<ApiOdpeOutputDto> listaResultado = new ArrayList<>();
        for(ApiOdpe odpetmp : apiOdpe.getLista()) {
            listaResultado.add(modelMapper.map(odpetmp, ApiOdpeOutputDto.class));
        }
        return listaResultado;
    }
}
