package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionB;

@Mapper
public interface RegistroSeccionBMapper {
	public void registrarCaracteristicaPrimerPaso(RegistroSeccionB param);
	public void registrarCaracteristicaSegundoPaso(RegistroSeccionB param);
	
	public void obtenerCaracteristicaPrimerPaso(RegistroSeccionB param);
	public void obtenerCaracteristicaSegundoPaso(RegistroSeccionB param);
	
	public void obtenerCaracteristicas(RegistroSeccionB param);
	public void validarSeccionB(RegistroSeccionB param);
	public void actualizarCaracteristicaPrimerPaso(RegistroSeccionB param);
}
