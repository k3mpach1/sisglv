package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.TabArchivo;

/**
 * @author glennlq
 * @created 9/21/21
 */
public interface TabArchivoServicio {
    TabArchivo crearTabArchivo(TabArchivo param) throws Exception;
    TabArchivo obtenerTabArchivoPorId(Integer idArchivo) throws Exception;
}
