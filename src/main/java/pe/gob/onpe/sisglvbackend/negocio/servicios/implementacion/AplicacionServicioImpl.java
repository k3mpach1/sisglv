package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.Aplicacion;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.AplicacionMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.AplicacionServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class AplicacionServicioImpl implements AplicacionServicio{

	@Autowired
	AplicacionMapper aplicativoMapper;
	
	
	@Override
	public void listarAplicaciones(Aplicacion param) throws Exception {
		// TODO Auto-generated method stub
		aplicativoMapper.listarAplicaciones(param);
		Funciones.validarOperacionConBaseDatos(param, AplicacionServicioImpl.class, "listarAplicaciones");
	}

	@Override
	public void listarAplicacionesActivos(Aplicacion param) throws Exception {
		// TODO Auto-generated method stub
		aplicativoMapper.listarAplicacionesActivos(param);
		Funciones.validarOperacionConBaseDatos(param, AplicacionServicioImpl.class, "listarAplicacionesActivos");
	}
	@Override
	public void registrarAplicacion(Aplicacion param) throws Exception {
		// TODO Auto-generated method stub
		aplicativoMapper.registrarAplicacion(param);
		Funciones.validarOperacionConBaseDatos(param, AplicacionServicioImpl.class, "registrarAplicacion");
		
	}

	@Override
	public void eliminarAplicacion(Aplicacion param) throws Exception {
		// TODO Auto-generated method stub
		aplicativoMapper.eliminarAplicacion(param);
		Funciones.validarOperacionConBaseDatos(param, AplicacionServicioImpl.class, "eliminarAplicacion");
	}

	@Override
	public void actualizarAplicacion(Aplicacion param) throws Exception {
		// TODO Auto-generated method stub
		aplicativoMapper.actualizarAplicacion(param);
		Funciones.validarOperacionConBaseDatos(param, AplicacionServicioImpl.class, "actualizarAplicacion");
	}

	@Override
	public void actualizarEstadoAplicacion(Aplicacion param) throws Exception {
		// TODO Auto-generated method stub
		aplicativoMapper.actualizarEstadoAplicacion(param);
		Funciones.validarOperacionConBaseDatos(param, AplicacionServicioImpl.class, "actualizarEstadoAplicacion");
	}

	@Override
	public void listarAplicacionesActivosPorPerfilOpcion(Aplicacion param) throws Exception {
		// TODO Auto-generated method stub
				aplicativoMapper.listarAplicacionesActivosPorPerfilOpcion(param);
				Funciones.validarOperacionConBaseDatos(param, AplicacionServicioImpl.class, "listarAplicacionesActivosPorPerfilOpcion");
			}
	




}
