package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;
import java.util.Date;
import java.util.List;


/**
 * @author glennlq
 * @created 10/25/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiToken extends ModeloBase {
    private Integer idToken;
    private String token;
    private Date fechaInicio;
    private Date fechaFin;
    private ApiUsuario apiUsuario;
    private List<ApiToken> lista;
}
