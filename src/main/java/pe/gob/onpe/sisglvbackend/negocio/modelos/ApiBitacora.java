package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

/**
 * @author glennlq
 * @created 10/26/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiBitacora extends ModeloBase  {
    Integer idBitacora;
    ApiUsuario apiUsuario;
    ApiToken apiToken;
    Integer estadoResultado;
    String descripcionResultado;
}
