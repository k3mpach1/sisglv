package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiBitacora;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Mapper
public interface ApiBitacoraMapper {
    void insertarApiBitacora(ApiBitacora apiBitacora) throws Exception;

}
