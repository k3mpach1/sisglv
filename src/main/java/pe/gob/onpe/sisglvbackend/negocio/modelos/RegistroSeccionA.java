package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistroSeccionA extends ModeloBasePaginacion {
	//LV = local de votacion
	
	private Integer idRegistroSeccionA;
	private VerificacionLV verificacionLV;
	private String seccion;
	private String codigoLV;
	private String codigoLVHistorico;
	private String codigoMinedu;
	private String dniRegistrador;
	private String nombreApellidoRegistrador;
	private Integer idUnidadOrganica;
	private String nombreUnidadOrganica;
	private Integer idTipoAmbitoGeografico;
	private MaeUbigeo ubigeo;
	private MaeUbigeo ubigeoProvincia;
	private MaeUbigeo ubigeoDepartamento;
	private Integer idTipoDistrito;
	private Integer estadoVerificacion;
	
	private MaeLocalVotacion maeLocalVotacion;
	
	
	private String latitudDistrito;
	private String longitudDistrito;
	
	private Integer vraem;
	
	private MaeCentroPoblado centroPoblado;
	private Direccion direccion;
	private String latitudPuertaAcceso;
	private String longitudPuertaAcceso;
	private Float distanciaCentroDistrito;
	private String tiempoCentroDistrito;
	private Integer idTipoLocalLV;
        private String nombreTipoLocalLV;
	private String numeroLocaLV;
	private String nombreLocalLV;
	private Integer idNivelInstruccion;
	private Integer idTipoInstitucion;
	private Integer idCategoriaInstitucion;
	private String dniRepresentanteLV;
	private String nombreApeRepresentanteLV;
	private String cargoRepresentanteLV;
	private String telefonoRepresentateLV;
	private String correoRepresentateLV;
	
	private String dniRepresentanteApafa;
	private String nombreApeRepresentanteApafa;
	private String telefonoRepresentanteApafa;
	private Integer cantidadAula;
	private Integer cantidadMesa;
	private Integer idTipoTecnologia;
	private String observacionRegistro;
	private String usuarioObservacion;
	private Date fechaObservacion;
	private String usuarioAprobacion;
	private Date fechaAprobacion;
	
	
	private Integer idTipoAmbitoElectoral;
	private Integer idAmbitoElectoral;
	private String otraCategoria;
	

	private Integer terminado;
	private Integer idEstado;

    private List<RegistroSeccionA> registrosSeccionA  = new ArrayList<>();;
	
	

	
	
	
}  

