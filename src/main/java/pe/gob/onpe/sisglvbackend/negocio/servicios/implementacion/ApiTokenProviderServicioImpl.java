package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import io.jsonwebtoken.MalformedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiTokenServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteJwt;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.TokenApiException;
import pe.gob.onpe.sisglvbackend.transversal.properties.ApiKeyProperties;
import org.springframework.util.StringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author glennlq
 * @created 10/25/21
 */
@Service
public class ApiTokenProviderServicioImpl {

    @Autowired
    private ApiKeyProperties apiKeyProperties;

    @Autowired
    ApiTokenServicio apiTokenServicio;

    public String resolveToken(HttpServletRequest req) throws TokenApiException{
        String bearerToken = req.getHeader(apiKeyProperties.getHeader());
        if (bearerToken != null && bearerToken.startsWith(apiKeyProperties.getPrefix())) {
            return bearerToken.replace(apiKeyProperties.getPrefix()+" ", "");
        }
        throw new TokenApiException("Acceso restringido", HttpStatus.FORBIDDEN);
    }

    public void validateToken(HttpServletRequest request, HttpServletResponse response) {
        String token = resolveToken(request);
        if(token != null) {
            apiTokenServicio.getApiTokenByToken(token);
        }
    }

    public String getJWTFromRequest(HttpServletRequest request) {
        final String bearerToken = request.getHeader(ConstanteJwt.HEADER_STRING);

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(ConstanteJwt.BEARER_TOKEN_PREFIX)) {
            return bearerToken.substring(ConstanteJwt.BEARER_TOKEN_PREFIX.length());
        }
        throw new MalformedJwtException("Authentication Header doesn't have Bearer Token");
    }



}
