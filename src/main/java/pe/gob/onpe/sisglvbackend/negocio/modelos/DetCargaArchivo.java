package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetCargaArchivo extends ModeloBase {
	private Integer idDetCargaArchivo;
	private CabCargaArchivo cabCargaArchivo;
	private Integer fila;
	private String clave;
	private String valor;
	private List<DetCargaArchivo> lista;
}
