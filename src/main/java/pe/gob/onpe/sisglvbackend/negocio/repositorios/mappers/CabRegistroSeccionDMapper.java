package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionD;

@Mapper
public interface CabRegistroSeccionDMapper {
	
	 void listarPorVerificacion(CabRegistroSeccionD cabRegistroSeccionD) throws Exception;
	 void validarSeccionD(CabRegistroSeccionD param);
	 void actualizarEstadoTerminado(CabRegistroSeccionD cabRegistroSeccionD);
}
