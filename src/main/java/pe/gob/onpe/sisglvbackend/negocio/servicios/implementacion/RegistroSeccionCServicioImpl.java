package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.CabRegistroSeccionCMapper;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.DetRegistroSeccionCMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionCServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class RegistroSeccionCServicioImpl implements RegistroSeccionCServicio {

	@Autowired
	private DetRegistroSeccionCMapper detRegistroSeccionMapper;
	
	@Autowired
	private CabRegistroSeccionCMapper cabRegistroSeccionMapper;
	
	@Override
	public void registrarSeccionC(DetRegistroSeccionC datosAula) throws Exception {
		// TODO Auto-generated method stub
		
		detRegistroSeccionMapper.registrar(datosAula);
		Funciones.validarOperacionConBaseDatos(datosAula, RegistroSeccionCServicioImpl.class, "registrarSeccionC");
	}

	@Override
	public void listarSeccionC(DetRegistroSeccionC param) throws Exception {
		detRegistroSeccionMapper.listar(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionCServicioImpl.class, "listarSeccionC");
	}

	@Override
	public void actualizarSeccionC(DetRegistroSeccionC datosAula) throws Exception {
		detRegistroSeccionMapper.actualizar(datosAula);
		Funciones.validarOperacionConBaseDatos(datosAula, RegistroSeccionCServicioImpl.class, "actualizarSeccionC");
		
	}

	@Override
	public void listarCabeceraRegistroSeccionC(CabRegistroSeccionC cabecera) throws Exception {
		cabRegistroSeccionMapper.listarPorVerificacion(cabecera);
		Funciones.validarOperacionConBaseDatos(cabecera, RegistroSeccionCServicioImpl.class, "listarCabeceraRegistroSeccionC");
		
	}

	@Override
	public void eliminarAula(DetRegistroSeccionC param) throws Exception {
		detRegistroSeccionMapper.eliminarAula(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionCServicioImpl.class, "eliminarAula");
		
	}

	@Override
	public void validarSeccionC(CabRegistroSeccionC param) {
		cabRegistroSeccionMapper.validarSeccionC(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionCServicioImpl.class, "validarSeccionC");

		
	}
	
	@Override
	public void actualizarEstadoTerminado(CabRegistroSeccionC param) {
		cabRegistroSeccionMapper.actualizarEstadoTerminado(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionCServicioImpl.class, "actualizarEstadoTerminado");
	}

}
