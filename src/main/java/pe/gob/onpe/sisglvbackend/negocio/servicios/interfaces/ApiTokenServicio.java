package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.TokenApiException;

/**
 * @author glennlq
 * @created 10/25/21
 */
public interface ApiTokenServicio {
    ApiToken getApiTokenByToken(String token) throws TokenApiException;
}
