package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;


import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;

@Mapper
public interface MaeUbigeoMapper {
	
	
	public void listarDepartamento(MaeUbigeo ubigeo) throws Exception;
	public void listarProvincia(MaeUbigeo ubigeo) throws Exception;
	public void listarDistrito(MaeUbigeo param)throws Exception;
	void obtenerMaeUbigeoPorUbigeo(MaeUbigeo maeUbigeo) throws Exception;
	public void listarDepartamentoPorProcesoAmbito(MaeUbigeo ubigeo) throws Exception;
	
	public void listarProvinciaPorProcesoAmbito(MaeUbigeo ubigeo) throws Exception;
	
	public void listarDistritoPorProcesoAmbito(MaeUbigeo ubigeo) throws Exception;
}
