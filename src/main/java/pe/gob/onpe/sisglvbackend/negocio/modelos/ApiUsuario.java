package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

/**
 * @author glennlq
 * @created 10/25/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiUsuario extends ModeloBase {
    private Integer idUsuario;
    private String nombre;
}
