package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ModeloBase;

@Getter
@Setter
public class Persona  extends ModeloBase{
	
	private String nombres;
	private String numeroDocumento;
	
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String soloNombres;

}
