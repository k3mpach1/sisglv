package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabArchivo;

/**
 * @author glennlq
 * @created 9/21/21
 */
@Mapper
public interface TabArchivoMapper {
    void obtenerTabArchivoPorId(TabArchivo param);
    void crearTabArchivo(TabArchivo param);
}
