package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCargaArchivo;
import java.util.List;

public interface DetCargaArchivoServicio {
    DetCargaArchivo crearDetCargaArchivo(DetCargaArchivo detCargaArchivo) throws Exception;
    DetCargaArchivo obtenerDetCargaArchivoPorId(Integer idDetCargaArchivo) throws Exception;
    List<DetCargaArchivo> listaDetCargaArchivoPorIdCargarArchivo(Integer idCargarArchivo) throws Exception;
}
