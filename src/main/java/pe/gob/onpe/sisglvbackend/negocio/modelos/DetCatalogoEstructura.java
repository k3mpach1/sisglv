package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetCatalogoEstructura extends ModeloBase{

	private Integer idDetCatalogoEstructura;
	private CabCatalogo cabCatalogo;
	private String columna;
	private String nombre;
	private Integer codigo;
	private String codigoCadena;
	private Integer orden;
	private Integer tipo;
	private String informacionAdicional;
	private Integer obligatorio;
	
	private List<DetCatalogoEstructura> lista = new ArrayList<>();
	


}
