package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeUbigeoMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeUbigeoServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class MaeUbigeoServicioImpl implements MaeUbigeoServicio{

	@Autowired
	private MaeUbigeoMapper maeUbigeoMapper;
	
	@Override
	public void listarDepartamento(MaeUbigeo param) throws Exception {
	
		maeUbigeoMapper.listarDepartamento(param);
		Funciones.validarOperacionConBaseDatos(param, MaeUbigeoServicioImpl.class, "listarDepartamento");
			
	}

	@Override
	public void listarProvincia(MaeUbigeo maeUbigeo) throws Exception {
		maeUbigeoMapper.listarProvincia(maeUbigeo);
		Funciones.validarOperacionConBaseDatos(maeUbigeo, MaeUbigeoServicioImpl.class, "listarProvincia");
		
	}

	@Override
	public void listarDistrito(MaeUbigeo param) throws Exception {
		maeUbigeoMapper.listarDistrito(param);
		Funciones.validarOperacionConBaseDatos(param, MaeUbigeoServicioImpl.class, "listarDistrito");
		
	}

	@Override
	public void listarDepartamentoPorProcesoAmbito(MaeUbigeo maeUbigeo) throws Exception {
		maeUbigeoMapper.listarDepartamentoPorProcesoAmbito(maeUbigeo);
		Funciones.validarOperacionConBaseDatos(maeUbigeo, MaeUbigeoServicioImpl.class, "listarDepartamentoPorProcesoAmbito");
		
	}

	@Override
	public void listarProvinciaPorProcesoAmbito(MaeUbigeo maeUbigeo) throws Exception {
		maeUbigeoMapper.listarProvinciaPorProcesoAmbito(maeUbigeo);
		Funciones.validarOperacionConBaseDatos(maeUbigeo, MaeUbigeoServicioImpl.class, "listarProvinciaPorProcesoAmbito");
	
		
	}

	@Override
	public void listarDistritoPorProcesoAmbito(MaeUbigeo maeUbigeo) throws Exception {
		maeUbigeoMapper.listarDistritoPorProcesoAmbito(maeUbigeo);
		Funciones.validarOperacionConBaseDatos(maeUbigeo, MaeUbigeoServicioImpl.class, "listarDistritoPorProcesoAmbito");
	
		
	}

}
