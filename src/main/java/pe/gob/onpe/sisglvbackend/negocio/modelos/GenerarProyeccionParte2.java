package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenerarProyeccionParte2 extends ModeloBase {
    Integer idPadronProyeccion;
}
