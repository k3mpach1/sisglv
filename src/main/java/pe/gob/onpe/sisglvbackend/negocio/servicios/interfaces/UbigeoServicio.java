package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;

public interface UbigeoServicio {

    void listarDepartamentosActivos(MaeUbigeo param) throws Exception;
    void listarProvinciasActivos(MaeUbigeo param) throws Exception;
    void listarDistritosActivos(MaeUbigeo param) throws Exception;
    MaeUbigeo obtenerMaeUbigeoPorUbigeo(String ubigeo) throws Exception;
}
