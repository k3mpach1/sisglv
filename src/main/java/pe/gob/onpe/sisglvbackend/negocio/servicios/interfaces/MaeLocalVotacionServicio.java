package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.LocalVotacionGenerico;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.ListaLocalVotacionDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.ListaLocalVotacionOutputDto;
import java.util.List;

public interface MaeLocalVotacionServicio {
    List<LocalVotacionGenerico> getListaLocalVotacionGenericoPorPE(LocalVotacionGenerico localVotacionGenerico) throws Exception;
    List<LocalVotacionGenerico> getLocalVotacionOrcGenericoPorPE(LocalVotacionGenerico localVotacionGenerico) throws Exception;
    ListaLocalVotacionOutputDto getListaLocalVotacionGenerico(ListaLocalVotacionDto listaLocalVotacionDto) throws Exception;
    ListaLocalVotacionOutputDto getListaLocalVotacionOrcGenerico(ListaLocalVotacionDto listaLocalVotacionDto) throws Exception;
}
