package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabConfAmbitoElectoral extends ModeloBase {
	private Integer idTabConfAmbitoElectoral;
	private TabConfProcesoElectoral tabConfProcesoElectoral;
	private MaeUbigeo maeUbigeo;
	private String capital;
}
