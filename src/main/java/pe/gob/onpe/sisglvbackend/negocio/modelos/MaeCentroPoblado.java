package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MaeCentroPoblado extends ModeloBase {
	
	private Integer id;
	private String nombre;
	private String ubigeo;
	private Long   idUbigeo;
	private List<MaeCentroPoblado> lista = new ArrayList<>();


   

}
