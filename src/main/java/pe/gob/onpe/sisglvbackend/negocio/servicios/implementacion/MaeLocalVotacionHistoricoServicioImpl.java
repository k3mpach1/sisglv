package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeLocalVotacionHistoricoMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeLocalVotacionHistoricoServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

import java.util.List;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.DetFichaSeccionCMapper;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.DetFichaSeccionDMapper;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.DetFichaSeccionEMapper;

@Service
public class MaeLocalVotacionHistoricoServicioImpl implements MaeLocalVotacionHistoricoServicio {

    @Autowired
    MaeLocalVotacionHistoricoMapper maeLocalVotacionHistoricoMapper;
    @Autowired
    DetFichaSeccionCMapper detFichaSeccionCMapper;
    @Autowired
    DetFichaSeccionDMapper detFichaSeccionDMapper;
    @Autowired
    DetFichaSeccionEMapper detFichaSeccionEMapper;

    @Override
    public List<MaeLocalVotacion> listarLocalesVotacionHistorico(MaeLocalVotacion param) throws Exception {
        maeLocalVotacionHistoricoMapper.listarLocalesVotacionHistorico(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "listarLocalesVotacionHistorico");
        return param.getLocalesVotaciones().size() == 0 ? null : param.getLocalesVotaciones();
    }

    @Override
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionA(MaeLocalVotacion param) throws Exception {
        maeLocalVotacionHistoricoMapper.obtenerLocalVotacionHistoricoSeccionA(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistoricoSeccionA");
        return param.getLocalesVotaciones().size() == 0 ? null : param.getLocalesVotaciones().get(0);
    }

    @Override
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionB(MaeLocalVotacion param) throws Exception {
        maeLocalVotacionHistoricoMapper.obtenerLocalVotacionHistoricoSeccionB(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistoricoSeccionB");
        return param.getLocalesVotaciones().size() == 0 ? null : param.getLocalesVotaciones().get(0);
    }

    @Override
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionC(MaeLocalVotacion param) throws Exception {
        maeLocalVotacionHistoricoMapper.obtenerLocalVotacionHistoricoCabSeccionC(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistoricoCabSeccionC");
        return param.getLocalesVotaciones().size() == 0 ? null : param.getLocalesVotaciones().get(0);
    }

    @Override
    public void obtenerLocalVotacionHistoricoDetSeccionC(DetFichaSeccionC param) throws Exception {
        detFichaSeccionCMapper.obtenerLocalVotacionHistDetSecC(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistDetSecC");
    }

    @Override
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionD(MaeLocalVotacion param) throws Exception {
        maeLocalVotacionHistoricoMapper.obtenerLocalVotacionHistoricoCabSeccionD(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistoricoCabSeccionD");
        return param.getLocalesVotaciones().size() == 0 ? null : param.getLocalesVotaciones().get(0);
    }

    @Override
    public void obtenerLocalVotacionHistoricoDetSeccionD(DetFichaSeccionD param) throws Exception {
        detFichaSeccionDMapper.obtenerLocalVotacionHistDetSecD(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistDetSecD");
    }

    @Override
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionE(MaeLocalVotacion param) throws Exception {
        maeLocalVotacionHistoricoMapper.obtenerLocalVotacionHistoricoCabSeccionE(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistoricoCabSeccionE");
        return param.getLocalesVotaciones().size() == 0 ? null : param.getLocalesVotaciones().get(0);
    }

    @Override
    public void obtenerLocalVotacionHistoricoDetSeccionE(DetFichaSeccionE param) throws Exception {
        detFichaSeccionEMapper.obtenerLocalVotacionHistDetSecE(param);
        Funciones.validarOperacionConBaseDatos(param, MaeLocalVotacionHistoricoServicioImpl.class, "obtenerLocalVotacionHistDetSecE");
    }

}
