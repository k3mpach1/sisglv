package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArchivoLV extends Archivo {

	private Integer idCategoria;
	private String  categoria;
	private Integer obligatorio;
	private Integer activo;
	private Integer orden;
	
}
