package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ModeloBase;

import java.util.List;

/**
 * @author glennlq
 * @created 1/6/22
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReporteSeccionD extends ModeloBase {
    private String codigoHistoricoLV;
    private String nombreLv;
    private String tipoArea;
    private Double largo;
    private Double ancho;
    private String referencia;
    private String tieneTecho;
    private String tienePuertaSeccionD;
    private Integer cantidadPuertaD;
    private String tieneLuzD;
    List<ReporteSeccionD> lista;
}
