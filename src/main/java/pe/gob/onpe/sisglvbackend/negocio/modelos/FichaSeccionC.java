package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FichaSeccionC extends ModeloBase {

    private MaeLocalVotacion maeLocalVotacion;
    private Integer cantidadMesa;
    private String observacionRegistro;
    private Integer tipoAula;
    private String aula;
    private String pabellon;
    private Integer piso;
    private Integer requiereToldo;
    private Integer usoEleccion;
    private Integer tieneLuminaria;
    private Integer cantidadLuminariaBE;
    private Integer cantidadLuminariaME;
    private Integer tieneTomacorriente;
    private Integer cantidadTomacorrienteBE;
    private Integer cantidadTomacorrienteME;
    private Integer tieneInterruptor;
    private Integer cantidadInterruptorBE;
    private Integer cantidadInterruptorME;
    private Integer tienePuerta;
    private Integer cantidadPuertaBE;
    private Integer cantidadPuertaME;
    private Integer tieneVentana;
    private Integer cantidadVentanaBE;
    private Integer cantidadVentanaME;
    private Integer tienePuntoInternet;
    private Integer tieneAccesoWifi;
    private Integer activo;

    private List<FichaSeccionC> lista = new ArrayList<>();

}
