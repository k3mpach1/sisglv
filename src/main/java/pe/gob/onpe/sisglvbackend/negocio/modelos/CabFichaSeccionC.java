package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabFichaSeccionC extends ModeloBase {

    private Integer idFichaSeccionC;
    private MaeLocalVotacion maeLocalVotacion;
    private String seccion;
    private Integer cantidadAula;
    private Integer cantidadMesa;
    private String observacionRegistro;

}
