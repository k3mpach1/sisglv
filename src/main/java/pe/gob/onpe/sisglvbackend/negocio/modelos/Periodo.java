package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Periodo extends ModeloBase {

	private Long id;
	private String etiqueta;
	private Long orden;
	private List<Periodo> periodos;
	
}
