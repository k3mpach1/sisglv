package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;

@Mapper
public interface MaeProcesoVerificacionMapper {
	List<MaeProcesoVerificacion> listarActivos(MaeProcesoVerificacion param);
	
	void registrarProcesoVerificacion(MaeProcesoVerificacion param);
	
	void getProcesoVerificacionById(MaeProcesoVerificacion param);
	void getProcesoVerificacionHabilitado(MaeProcesoVerificacion param);
	

}
