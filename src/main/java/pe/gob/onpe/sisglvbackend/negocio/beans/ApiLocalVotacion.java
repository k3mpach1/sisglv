package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ModeloBase;
import java.util.List;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiLocalVotacion extends ModeloBase {
    Integer idProcesoElectoral;
    String acronimo;
    String codigo;
    String ubigeo;
    String nombre;
    String direccion;
    String mesas;
    String electores;
    String ccpp;
    String latitud;
    String longitud;
    String referencia;
    String tipoSolucion;
    String mesasEspeciales;
    List<ApiLocalVotacion> lista;
    ApiToken apiToken;
}
