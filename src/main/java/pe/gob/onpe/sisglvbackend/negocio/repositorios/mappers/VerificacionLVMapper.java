package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.EstadoTerminadoNuevoLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;

@Mapper
public interface VerificacionLVMapper {

    void listarVerificacionActivos(VerificacionLV param) throws Exception;

    void enviarAValidacionLV(VerificacionLV param) throws Exception;

    void listarEditarNuevoLocalVotacion(VerificacionLV param) throws Exception;

    void listarValidarNuevoLocalVotacion(VerificacionLV param) throws Exception;

    void validarNuevoRegistroLV(VerificacionLV param) throws Exception;
    
    void validarVerificacionLV(VerificacionLV param) throws Exception;
    

    void listarLocalVotacionVerificacion(VerificacionLV param) throws Exception;
    
    void totalLocalesVotacion(VerificacionLV param) throws Exception;
    
    void listarLocalVotacionValidacion(VerificacionLV param) throws Exception;
    
    void totalLocalesValidacion(VerificacionLV param) throws Exception;
    
    void listarEditarVerificacionLocalVotacion(VerificacionLV param) throws Exception;
    
    
    void listarValidarLocalVotacionHistorico(VerificacionLV param) throws Exception;
    
    void actualizarEstadoDisponible(VerificacionLV param) throws Exception;
    
    
    void cancelarRegistroVerificacion(VerificacionLV param) throws Exception;

    void obtenerEstadoTerminadoNuevoLocal(EstadoTerminadoNuevoLocalVotacion param) throws Exception;
    
}
