package pe.gob.onpe.sisglvbackend.negocio.modelos;



import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProspectivaSeleccion extends ModeloBase {

	private Long periodoPadronInicio;
	private Long periodoPadronFinal;
	private Long periodoPadronReferencia;
	
}
