package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;


import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FiltroRegistroSeccionE;

public interface CabRegistroSeccionEServicio {

	void listarPorId(FiltroRegistroSeccionE filtroRegistroSeccionE);
	void actualizarObservacionReg(CabRegistroSeccionE cabRegistroSeccionE);
	void actualizarObservacionLv(CabRegistroSeccionE cabRegistroSeccionE);
	void agregarArchivo(CabRegistroSeccionE cabRegistroSeccionE);
	void desactivarArchivo(CabRegistroSeccionE cabRegistroSeccionE);
	void listarArchivos(CabRegistroSeccionE cabRegistroSeccionE);
	void obtenerArchivoPorId(CabRegistroSeccionE cabRegistroSeccionE);
	void validarRegistroSeccionE(CabRegistroSeccionE cabRegistroSeccionE);
	void listarCategoriaArchivoSeccionE(CabRegistroSeccionE cabRegistroSeccionE);
	
}
