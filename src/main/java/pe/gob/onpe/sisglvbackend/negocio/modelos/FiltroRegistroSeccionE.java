package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiltroRegistroSeccionE extends ModeloBase{

	private Long idCabRegistroSeccionE;
	private List<CabRegistroSeccionE> registrosSeccionE;
	
}
