package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.FichaSeccionCMapper;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.FichaSeccionDMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.FichaSeccionSevicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class FichaSeccionServicioImpl implements FichaSeccionSevicio{

    @Autowired
    FichaSeccionCMapper fichaSeccionCMapper;
    @Autowired
    FichaSeccionDMapper fichaSeccionDMapper;
    
    @Override
    public void listarFichaSeccionC(FichaSeccionC param) throws Exception {
        fichaSeccionCMapper.listarFichaSeccionC(param);
        Funciones.validarOperacionConBaseDatos(param, FichaSeccionServicioImpl.class, "listarFichaSeccionC");
    }

    @Override
    public void listarFichaSeccionD(FichaSeccionD param) throws Exception {
        fichaSeccionDMapper.listarFichaSeccionD(param);
        Funciones.validarOperacionConBaseDatos(param, FichaSeccionServicioImpl.class, "listarFichaSeccionD");
    }
    
}
