package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.Aplicacion;

@Mapper
public interface AplicacionMapper {
	void listarAplicaciones(Aplicacion param) throws Exception;
	void listarAplicacionesActivos(Aplicacion param) throws Exception;
	void listarAplicacionesActivosPorPerfilOpcion(Aplicacion param) throws Exception;
	void registrarAplicacion(Aplicacion param) throws Exception;
	void eliminarAplicacion(Aplicacion param) throws Exception;
	void actualizarAplicacion(Aplicacion param) throws Exception;
	void actualizarEstadoAplicacion(Aplicacion param) throws Exception;
}
