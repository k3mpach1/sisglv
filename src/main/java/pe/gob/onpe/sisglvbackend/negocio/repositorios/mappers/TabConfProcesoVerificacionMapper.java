package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfProcesoVerificacion;

@Mapper
public interface TabConfProcesoVerificacionMapper {
    void crearTabConfProcesoVerificacion(TabConfProcesoVerificacion tabConfProcesoVerificacion) throws Exception;
}
