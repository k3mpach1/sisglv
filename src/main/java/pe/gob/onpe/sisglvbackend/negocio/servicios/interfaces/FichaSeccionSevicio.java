package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionD;

public interface FichaSeccionSevicio {
    
    void listarFichaSeccionC(FichaSeccionC param) throws Exception;
    
    void listarFichaSeccionD(FichaSeccionD param) throws Exception;
    
}
