package pe.gob.onpe.sisglvbackend.negocio.modelos;


import java.io.InputStream;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Archivo extends ModeloBase {

	private Integer idArchivo;
	private String guid;
	private String path;
	private String filename;
	private String filenameOriginal;
	private String extension;
	private Long peso;
	private String pesoStr;
	private String archivoBase64;
	private InputStream archivoStream;
	private String mimeType;

}
