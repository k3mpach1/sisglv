package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenerarProyeccionParte1 extends ModeloBase {
    Integer idPadronProyeccion;
}
