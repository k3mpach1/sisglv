package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FichaSeccionE {
    
    
    private MaeLocalVotacion maeLocalVotacion;
    private String observacionRegistro;
    
    
    
}
