package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import pe.gob.onpe.sisglvbackend.ftp.ArchivoFTP;
import pe.gob.onpe.sisglvbackend.ftp.ConfArchivoFTP;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCargaArchivo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabFormatoPlantilla;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCargaArchivo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaGenerica;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeAmbitoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabArchivo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfAmbitoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeProcesoVerificacionMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CabCargaArchivoServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.DetCargaArchivoServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.FichaGenericaServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeAmbitoElectoralServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProcesoVerificacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.SFTPService;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabArchivoServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabConfAmbitoVerificacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabConfProcesoVerificacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.UbigeoServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteFTP;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotFoundRegisterInDb;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.AsignarOrcAVerificarProcesoDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.AsignarOrcAVerficacionProcesoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.ProcesarArchivoOrcOutputDto;

@Service
public class ProcesoVerificacionServicioImpl implements ProcesoVerificacionServicio {

	private final Integer COL_TIPO_PROCESO = 2;
	private final Integer COL_TIPO_AMBITO = 2;
	private final Integer COL_ACTIVO = 1;
	private final boolean FTP_LOCAL = false;
	private final String  FTP_LOCAL_RUTA = "/home/glennlq/dev/SISGLV_FTP/";
//	private final String USUARIO_CREACION = "444444";
	private static final Logger logger = Logger.getLogger(ProcesoVerificacionServicioImpl.class.getCanonicalName());
	
	@Autowired
	MaeProcesoVerificacionMapper procesoVerificacionMapper;
	
	@Autowired
	private SFTPService ftpService;
	
	@Autowired
	private TabArchivoServicio tabArchivoServicio;
	
	@Autowired
	private CabCargaArchivoServicio cabCargaArchivoServicio;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private DetCargaArchivoServicio detCargaArchivoServicio;
	
	@Autowired
	private MaeAmbitoElectoralServicio maeAmbitoElectoralServicio;
	
	@Autowired
	private TabConfProcesoVerificacionServicio tabConfProcesoVerificacionServicio;
	
	@Autowired
	private TabConfAmbitoVerificacionServicio tabConfAmbitoVerificacionServicio;
	
	@Autowired
	private FichaGenericaServicio fichaGenericaServicio;
	
	@Autowired
	private UbigeoServicio ubigeoServicio;
	
	@Override
	public List<MaeProcesoVerificacion> listarActivos(MaeProcesoVerificacion param) {
		// TODO Auto-generated method stub
		procesoVerificacionMapper.listarActivos(param);
		Funciones.validarOperacionConBaseDatos(param, ProcesoElectoralServicioImpl.class, "listarActivos");
		return param.getProcesosVerificaciones();
	}

	@Override
	public void registrarProcesoVerificacion(MaeProcesoVerificacion param) {
		
		procesoVerificacionMapper.registrarProcesoVerificacion(param);
		Funciones.validarOperacionConBaseDatos(param, ProcesoElectoralServicioImpl.class, "registrarProcesoVerificacion");
		
		
	}

	// cargar archivos
	
	@Override
	public MaeProcesoVerificacion getProcesoVerificacionById(Integer idProcesoVerificacion) throws Exception {
		MaeProcesoVerificacion maeProcesoVerificacion = MaeProcesoVerificacion.builder().idProcesoVerificacion(idProcesoVerificacion).build();
		procesoVerificacionMapper.getProcesoVerificacionById(maeProcesoVerificacion); // Falta implementar el dao
		Funciones.validarOperacionConBaseDatos(maeProcesoVerificacion, TabArchivoServicioImpl.class, "getProcesoVerificacionById");
		if(maeProcesoVerificacion.getProcesosVerificaciones().size() == 0)
			throw new NotFoundRegisterInDb("MAE_PROCESO_VERIFICACION", "N_PROCESO_VERIFICACION_PK", idProcesoVerificacion);
		return maeProcesoVerificacion.getProcesosVerificaciones().get(0);
	}
	
	@Override
	public MaeProcesoVerificacion getProcesoVerificacionHabilitado() throws Exception {
		MaeProcesoVerificacion maeProcesoVerificacion = MaeProcesoVerificacion.builder().build();
		procesoVerificacionMapper.getProcesoVerificacionHabilitado(maeProcesoVerificacion); // Falta implementar el dao
		Funciones.validarOperacionConBaseDatos(maeProcesoVerificacion, MaeProcesoVerificacion.class, "MaeProcesoVerificacion");
		MaeProcesoVerificacion encontrado = (maeProcesoVerificacion.getProcesosVerificaciones()) != null && ( maeProcesoVerificacion.getProcesosVerificaciones().size() > 0 ) ? maeProcesoVerificacion.getProcesosVerificaciones().get(0): null;
		return encontrado;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ProcesarArchivoOrcOutputDto cargarArchivoOrcsAFtp(Integer idProcesoVerificacion, MultipartFile multipartFile, String numeroDocumento) throws Exception {
		MaeProcesoVerificacion maeProcesoVerificacion = getProcesoVerificacionById(idProcesoVerificacion);
		TabArchivo tabArchivo = buildTabArchivo(multipartFile, numeroDocumento);
		guardarArchivoEnFtp(multipartFile, tabArchivo);
		tabArchivo = tabArchivoServicio.crearTabArchivo(tabArchivo);
		return ___procesarArchivoOrcs(maeProcesoVerificacion, tabArchivo, numeroDocumento);
	}
	
	private TabArchivo buildTabArchivo(MultipartFile multipartFile, String numeroDocumento) throws Exception {
		ArchivoFTP archivoFTP = new ArchivoFTP(multipartFile, getConfArchivoFTP());
		archivoFTP.validateAll();
		TabArchivo tabArchivo = TabArchivo.builder()
				.guidArchivo(archivoFTP.getUUID())
				.rutaArchivo(ConstanteFTP.CARPETA_ORCS)
				.nombreArchivo(archivoFTP.getFileName())
				.formatoArchivo(archivoFTP.getFileExtension())
				.pesoArchivo(archivoFTP.getSize().toString())
				.nombreOriginalArchivo(archivoFTP.getFileNameGenerated())
				.build();
		tabArchivo.setActivo(COL_ACTIVO);
		tabArchivo.setUsuarioCreacion(numeroDocumento);
		return tabArchivo;
	}
	
	private ConfArchivoFTP getConfArchivoFTP() {
		ConfArchivoFTP conf = new ConfArchivoFTP();
		conf.setMaxCharacters(50L);
		conf.setValidatedExtensions(new String[] { "xls", "xlsx"});
		conf.setMaxSize(1048576L*6L); // 6MB
		return conf;
	}
	
	private void guardarArchivoEnFtp(MultipartFile multipartFile, TabArchivo tabArchivo) throws Exception {
		if(FTP_LOCAL) {
			File file = new File(FTP_LOCAL_RUTA, tabArchivo.getRutaArchivo());
			File excel = new File(file, tabArchivo.getNombreOriginalArchivo());
			FileUtils.writeByteArrayToFile(excel, multipartFile.getBytes());
		} else {
			ftpService.uploadFileToFTP(multipartFile.getInputStream(), tabArchivo.getRutaArchivo(), tabArchivo.getNombreOriginalArchivo());
		}
	}
	
	public ProcesarArchivoOrcOutputDto ___procesarArchivoOrcs(MaeProcesoVerificacion maeProcesoVerificacion, TabArchivo tabArchivo,
															  String numeroDocumento) throws Exception {
		InputStream inputStreamFileExcel =  getInputStreamFileExcel(tabArchivo);
		CabFormatoPlantilla cabFormatoPlantilla = new CabFormatoPlantilla();
		CabCargaArchivo cabCargaArchivo = cabCargaArchivoServicio.crearCabCargaArchivo(buildCabCargaArchivo(maeProcesoVerificacion, tabArchivo, cabFormatoPlantilla, numeroDocumento));
		List<DetCargaArchivo> detCargaArchivos = guardarDetCargaArchivo(leerArchivoOrcs(tabArchivo, cabCargaArchivo, inputStreamFileExcel, numeroDocumento));
		return modelMapper.map(cabCargaArchivo, ProcesarArchivoOrcOutputDto.class);
	}
	
	private InputStream getInputStreamFileExcel(TabArchivo tabArchivo) throws Exception {
		if(FTP_LOCAL) {
			File path = new File(FTP_LOCAL_RUTA, tabArchivo.getRutaArchivo());
			File fileExcel = new File(path, tabArchivo.getNombreOriginalArchivo());
			return new FileInputStream(fileExcel);
		} else {
			byte[] data = ftpService.downloadFileFromFTP(tabArchivo.getRutaArchivo(), tabArchivo.getNombreOriginalArchivo());
			return new ByteArrayInputStream(data);
		}
	}
	
	private CabCargaArchivo buildCabCargaArchivo(MaeProcesoVerificacion maeProcesoVerificacion, TabArchivo tabArchivo,
			CabFormatoPlantilla cabFormatoPlantilla, String numeroDocumento) throws Exception {
		CabCargaArchivo cabCargaArchivo = CabCargaArchivo.builder().maeProcesoElectoral( new MaeProcesoElectoral())
				.maeProcesoVerificacion(maeProcesoVerificacion).tipoProceso(COL_TIPO_PROCESO)
				.cabFormatoPlantilla(cabFormatoPlantilla).tabArchivo(tabArchivo).build();
		cabCargaArchivo.setActivo(COL_ACTIVO);
		cabCargaArchivo.setUsuarioCreacion(numeroDocumento);
		return cabCargaArchivo;
	}
	
	private List<DetCargaArchivo> leerArchivoOrcs(TabArchivo tabArchivo, CabCargaArchivo cabCargaArchivo, InputStream inputStreamFile,
												  String numeroDocumento) throws Exception {
		Workbook workbook = new XSSFWorkbook(inputStreamFile);
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter dataFormatter = new DataFormatter();
		String[] heads = getHeadExcel(sheet, dataFormatter);
		List<DetCargaArchivo> detCargaArchivos = new ArrayList<>();
		int fila = 0;
		for (Row row : sheet) {
			if(fila++ == 0) continue;
			boolean rowEmpty = getIfEmptyRow(row, heads, dataFormatter);
			if(!rowEmpty) {
				for (int i = 0; i < heads.length; i++) {
					detCargaArchivos.add(buildDetCargaArchivo(cabCargaArchivo, fila, heads[i], dataFormatter.formatCellValue(row.getCell(i)), numeroDocumento));
				}
			}
		}
		return detCargaArchivos;
	}
	
	private DetCargaArchivo buildDetCargaArchivo(CabCargaArchivo cabCargaArchivo, int fila, String clave, String valor,
												 String numeroDocumento) throws Exception {
		DetCargaArchivo detCargaArchivo = DetCargaArchivo.builder()
				.cabCargaArchivo(cabCargaArchivo)
				.fila(fila)
				.clave(clave)
				.valor(valor).build();
		detCargaArchivo.setUsuarioCreacion(numeroDocumento);
		return detCargaArchivo;
	}

	private List<DetCargaArchivo> guardarDetCargaArchivo(List<DetCargaArchivo> detCargaArchivos) throws Exception {
		Iterator<DetCargaArchivo> iter = detCargaArchivos.iterator();
		while(iter.hasNext()) {
			DetCargaArchivo tmp = iter.next();
			detCargaArchivoServicio.crearDetCargaArchivo(tmp);
		}
		return detCargaArchivos;
	}
	
	private boolean getIfEmptyRow(Row row, String[] heads, DataFormatter dataFormatter) throws Exception {
		boolean rowEmpty = false;
		for(int i = 0; i < heads.length; i++) {
			String tmp = dataFormatter.formatCellValue(row.getCell(i));
			if(tmp != null && !tmp.trim().equals("")){
			} else {
				rowEmpty = true;
			}
		}
		return rowEmpty;
	}

	private String[] getHeadExcel(Sheet sheet, DataFormatter dataFormatter) throws Exception {
		List<String> heads = new ArrayList<>();
		Row row = sheet.getRow(0);
		for(int i = 0 ; i < 30 ; i++) {
			String value = dataFormatter.formatCellValue(row.getCell(i));
			if(value != null && !value.equals("")) {
				heads.add(value);
			}
		}
		return heads.toArray(new String[0]);
	}
	
	// ====================================================================================================================
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public AsignarOrcAVerficacionProcesoOutputDto asignarOrcsAVerificarProceso(AsignarOrcAVerificarProcesoDto asignarOdpeAProcesoElectoralDto,
																			   String numeroDocumento) throws Exception {
		CabCargaArchivo cabCargaArchivo = cabCargaArchivoServicio.getCabCargaArchivoPorId(asignarOdpeAProcesoElectoralDto.getIdCargarArchivo());
		MaeProcesoVerificacion maeProcesoVerificacion = this.getProcesoVerificacionById(cabCargaArchivo.getMaeProcesoVerificacion().getIdProcesoVerificacion());
		List<DetCargaArchivo> detCargaArchivos = detCargaArchivoServicio.listaDetCargaArchivoPorIdCargarArchivo(cabCargaArchivo.getIdCargarArchivo());
		List<Map<String, DetCargaArchivo>> ordenados = ordenarPorFilaDetCargaArchivo(detCargaArchivos);
		registrarMaeAmbitoElectoral(ordenados, maeProcesoVerificacion, numeroDocumento);
		return AsignarOrcAVerficacionProcesoOutputDto.builder().exito(true).mensaje("Proceso exitoso").build();
	}
	
	private List<Map<String, DetCargaArchivo>> ordenarPorFilaDetCargaArchivo(List<DetCargaArchivo> detCargaArchivos) throws Exception {
		int filaTmp = -1;
		List<Map<String, DetCargaArchivo>> lista = new ArrayList<>();
		for(DetCargaArchivo detCargaArchivo : detCargaArchivos) {
			if(detCargaArchivo.getFila() != filaTmp) {
				Map<String, DetCargaArchivo> tmp = new HashMap<>();
				tmp.put(detCargaArchivo.getClave().trim(), detCargaArchivo);
				lista.add(tmp);
				filaTmp = detCargaArchivo.getFila();
			} else {
				Map<String, DetCargaArchivo> tmp2 = lista.get(lista.size()-1);
				tmp2.put(detCargaArchivo.getClave().trim(), detCargaArchivo);
				lista.set(lista.size()-1, tmp2);
			}
		}
		return lista;
	}
	
	private void registrarMaeAmbitoElectoral(List<Map<String, DetCargaArchivo>> lista, MaeProcesoVerificacion maeProcesoVerificacion,
											 String numeroDocumento) throws Exception {
		for(Map<String, DetCargaArchivo> mapRow : lista) {
			MaeAmbitoElectoral maeAmbitoElectoral = maeAmbitoElectoralServicio.obtenerMaeAmbitoElectoralPorNombre(mapRow.get("NOMBRE_ORC").getValor(), COL_TIPO_AMBITO);
			if(maeAmbitoElectoral == null) {
				maeAmbitoElectoral =maeAmbitoElectoralServicio.crearMaeAmbitoElectoral(buildMaeAmbitoElectoralFromMap(mapRow, numeroDocumento));
			}
			// TAB_CONF_PROCESO_ELECTORAL
			registrarTabConfProcesoVerificacion(maeAmbitoElectoral, maeProcesoVerificacion, mapRow, numeroDocumento); // descomentar
		}
	}
	
	private MaeAmbitoElectoral buildMaeAmbitoElectoralFromMap(Map<String, DetCargaArchivo> mapRow, String numeroDocumento) {
		MaeAmbitoElectoral ambitoElectoral = MaeAmbitoElectoral.builder()
				.idAmbitoElectoralPadre(null)
				.nombre(mapRow.get("NOMBRE_ORC").getValor())
				.abreviatura(mapRow.get("NOMBRE_ORC").getValor().replaceAll("\\s",""))
				.tipoAmbitoElectoral(COL_TIPO_AMBITO)
				.build();
		ambitoElectoral.setActivo(COL_ACTIVO);
		ambitoElectoral.setUsuarioCreacion(numeroDocumento);
		return ambitoElectoral;
	}
	
	private void registrarTabConfProcesoVerificacion(MaeAmbitoElectoral maeAmbitoElectoral, MaeProcesoVerificacion maeProcesoVerificacion,
			  Map<String, DetCargaArchivo> mapRow, String numeroDocumento) throws Exception {
		TabConfProcesoVerificacion tabConfProcesoVerificacion = buildTabConfProcesoVerificacion(maeProcesoVerificacion, maeAmbitoElectoral, mapRow, numeroDocumento);
		tabConfProcesoVerificacion = tabConfProcesoVerificacionServicio.crearTabConfProcesoVerificacion(tabConfProcesoVerificacion);
		// TAB_CONF_AMBITO_VERIFICACION
		registrarTabConfAmbitoVerificacion(maeProcesoVerificacion, maeAmbitoElectoral, tabConfProcesoVerificacion, mapRow, numeroDocumento);
	}

	private TabConfProcesoVerificacion buildTabConfProcesoVerificacion(MaeProcesoVerificacion maeProcesoVerificacion, MaeAmbitoElectoral ambitoElectoral,
			 Map<String, DetCargaArchivo> mapRow, String numeroDocumento) throws Exception {
		TabConfProcesoVerificacion tabConfProcesoElectoral = TabConfProcesoVerificacion.builder()
									.maeProcesoVerificacion(maeProcesoVerificacion)
									.maeAmbitoElectoral(ambitoElectoral)
									.notificacion(0)
									.activo(1)
									.build();
									tabConfProcesoElectoral.setActivo(COL_ACTIVO);
									tabConfProcesoElectoral.setUsuarioCreacion(numeroDocumento);
		return tabConfProcesoElectoral;
	}
	
	private void registrarTabConfAmbitoVerificacion(MaeProcesoVerificacion maeProcesoVerificacion,
			MaeAmbitoElectoral maeAmbitoElectoral, TabConfProcesoVerificacion tabConfProcesoVerificacion,
			Map<String, DetCargaArchivo> mapRow, String numeroDocumento) throws Exception {
		TabConfAmbitoVerificacion tabConfAmbitoVerificacion = buildTabConfAmbitoVerificacion(tabConfProcesoVerificacion, mapRow, numeroDocumento);
		tabConfAmbitoVerificacionServicio.crearTabConfAmbitoVerificacion(tabConfAmbitoVerificacion);
		copiarFichasABCDE(maeProcesoVerificacion, maeAmbitoElectoral, tabConfAmbitoVerificacion);
	}

	private TabConfAmbitoVerificacion buildTabConfAmbitoVerificacion(TabConfProcesoVerificacion tabConfProcesoVerificacion,
			Map<String, DetCargaArchivo> mapRow, String numeroDocumento) throws Exception {
		MaeUbigeo maeUbigeo = ubigeoServicio.obtenerMaeUbigeoPorUbigeo(mapRow.get("N_DIST_PK").getValor());
		TabConfAmbitoVerificacion tabConfAmbitoVerificacion = TabConfAmbitoVerificacion.builder()
				.tabConfProcesoVerificacion(tabConfProcesoVerificacion)
				.maeUbigeo(maeUbigeo).build();
		tabConfAmbitoVerificacion.setActivo(COL_ACTIVO);
		tabConfAmbitoVerificacion.setUsuarioCreacion(numeroDocumento);
		return tabConfAmbitoVerificacion;
	}
	
	private void copiarFichasABCDE(MaeProcesoVerificacion maeProcesoVerificacion, MaeAmbitoElectoral maeAmbitoElectoral,
			TabConfAmbitoVerificacion tabConfAmbitoVerificacion) throws Exception {
		logger.warning("Proceso elect:" + maeProcesoVerificacion.getIdProcesoVerificacion() + ",  " 
				+ "ORC :"+ maeAmbitoElectoral.getIdAmbitoElectoral() + " - " + maeAmbitoElectoral.getNombre() 
				+ "UBIGEO: " + tabConfAmbitoVerificacion.getMaeUbigeo().getIdUbigeo() + " -> " + tabConfAmbitoVerificacion.getMaeUbigeo().getUbigeo()
				+ "TIPO DE AMBITO: "+ maeAmbitoElectoral.getTipoAmbitoElectoral());
		FichaGenerica fichaGenerica = FichaGenerica.builder()
				.maeProcesoVerificacion(maeProcesoVerificacion)
				.maeAmbitoElectoral(maeAmbitoElectoral)
				.maeUbigeo(tabConfAmbitoVerificacion.getMaeUbigeo())
				.build();
		fichaGenerica.setUsuarioCreacion(tabConfAmbitoVerificacion.getUsuarioCreacion());
		logger.warning(new Gson().toJson(fichaGenerica));
		fichaGenerica = fichaGenericaServicio.copiarFichasABCDE(fichaGenerica);
	}

	}
