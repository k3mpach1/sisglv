package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetRegistroSeccionD extends ModeloBase{
	private Integer idDetRegistroSeccionD;
	private CabRegistroSeccionD cabRegistroSeccionD;
	private Integer tipoArea;
	private BigDecimal largo;
	private BigDecimal ancho;
	private String referencia;
	private Integer tieneTecho;
	private Integer tienePuerta;
	private Integer cantidadPuerta;
	private Integer tieneAccesoLuz;
	private Integer terminado;
	private String observacion;
	
	//Descripciones de catalogo
	private String descripcionTipoArea;
	
	//Lista de detalles
	private List<DetRegistroSeccionD> registrosD;
	
	
	
}
