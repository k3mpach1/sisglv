package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProspectivaProyeccionParte2 extends ModeloBase {
    Integer idPadronProyeccion;
    Double toleranciaNacional;
    Double toleranciaExtranjero;
    Double toleranciaLimaCallao;
    Double toleranciaProvinciaDis;
    Double toleranciaExtranjeroDis;
    Integer idPadronParametro;
}
