package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LocalVotacionGenerico extends ModeloBase {
    private Integer idProcesoElectoral;
    private Integer idProcesoVerificacion;
    private String orc;
    private String odpe;
    private String departamento;
    private String provincia;
    private String distrito;
    private String localVotacion;
    private String correlativo;
    private List<LocalVotacionGenerico> lista;
}
