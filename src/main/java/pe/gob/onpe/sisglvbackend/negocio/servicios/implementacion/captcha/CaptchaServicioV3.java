package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion.captcha;

import java.net.URI;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import lombok.extern.log4j.Log4j2;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ReCaptchaInvalidException;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ReCaptchaUnavailableException;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.captcha.GoogleOutputDto;


@Log4j2
@Service("captchaServiceV3")
public class CaptchaServicioV3 extends AbstractCaptchaServicio{
	  public static final String REGISTER_ACTION = "register";
	    public static final String IMPORTANT_ACTION = "importantAction";
	    
	    @Override
	    public void processResponse(String response, final String action) throws ReCaptchaInvalidException {
	        securityCheck(response);
	        
	        //final URI verifyUri = URI.create(String.format(RECAPTCHA_URL_TEMPLATE, getReCaptchaSecret(), response, getClientIP()));
	        final URI verifyUri = URI.create(String.format(RECAPTCHA_URL_TEMPLATE, getReCaptchaSecret(), response, "localhost"));
	        try {
	        	log.info("INGRESE: 1");
	        	log.info("INGRESE: URL" + verifyUri.toString());
	        	
	        	final GoogleOutputDto googleResponse = restTemplate.getForObject(verifyUri, GoogleOutputDto.class);
	        	log.info("INGRESE: 2");
	        	log.debug("Google's response: {} ", googleResponse.toString());

	        	log.info("IS SUCCESS: " + googleResponse.isSuccess());
	        	log.info("ACTION 1: " + googleResponse.getAction());
	            log.info("ACTION 2: " + action);
	            log.info("STORE 1: " + googleResponse.getScore());
	            log.info("STORE 2: " + captchaSettings.getThreshold());
	            if (!googleResponse.isSuccess() || !googleResponse.getAction().equals(action) || googleResponse.getScore() < captchaSettings.getThreshold()) {
	                if (googleResponse.hasClientError()) {
	                   reCaptchaAttemptService.reCaptchaFailed(getClientIP());
	                }
	                throw new ReCaptchaInvalidException("reCaptcha no se validó con éxito.");
	            }
	        } catch (RestClientException rce) {
	        	//log.info(rce.prin);
	            throw new ReCaptchaUnavailableException("El registro no está disponible en este momento.  Inténtelo de nuevo más tarde.",rce.getMessage());
	        }
	        reCaptchaAttemptService.reCaptchaSucceeded(getClientIP());
	    }
}
