package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionA;

@Mapper
public interface RegistroSeccionAMapper {
	public void registrarDatosGenerales(RegistroSeccionA param);
	public void registrarDatosGeneralesPrimerPaso(RegistroSeccionA param);
	public void registrarDatosGeneralesSegundoPaso(RegistroSeccionA param);
	public void registrarDatosGeneralesTercerPaso(RegistroSeccionA param);
	public void registrarDatosGeneralesCuartoPaso(RegistroSeccionA param);
	
	public void obtenerDatosGeneralesPrimerPaso(RegistroSeccionA param);
	public void obtenerDatosGeneralesSegundoPaso(RegistroSeccionA param);
	public void obtenerDatosGeneralesTercerPaso(RegistroSeccionA param);
	public void obtenerDatosGeneralesCuartoPaso(RegistroSeccionA param);
        
	public void obtenerDatosGenerales(RegistroSeccionA param);
	
    public void listarLVActivos(RegistroSeccionA param);
    
    public void validarSeccionA(RegistroSeccionA param);
    public void actualizarDatosGeneralesPrimerPaso(RegistroSeccionA param);
}
