package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabRegistroSeccionC extends ModeloBase{
	private Integer idRegistroSeccionC;
	private VerificacionLV verificacionLV;
	private String seccion;
	private Integer cantidadAula;
	private Integer cantidadMesa;
	private String observacionRegistro;
	private String usuarioObservacion;
	private String usuarioAprobacion;
	private Date fechaObservacion;
	private Date fechaAprobacion;
	private Integer idEstado;
	private Integer terminado;
	private Integer estadoVerificacion;

	
	private List<CabRegistroSeccionC> seccionC = new ArrayList<>();
}
