package pe.gob.onpe.sisglvbackend.negocio.modelos;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetRegistroSeccionE extends ModeloBase{

	private Long idDetRegistroSeccionE;
	private CabRegistroSeccionE cabRegistroSeccionE;
	private ArchivoLV archivo;
	
	
}
