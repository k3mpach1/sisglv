package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabConfAmbitoVerificacion extends ModeloBase {
	private Integer idTabConfAmbitoVerificacion;
	private TabConfProcesoVerificacion tabConfProcesoVerificacion;
	private MaeUbigeo maeUbigeo;
}
