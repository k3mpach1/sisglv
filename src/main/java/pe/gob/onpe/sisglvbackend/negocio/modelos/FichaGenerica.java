package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

/**
 * @author glennlq
 * @created 9/27/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FichaGenerica extends ModeloBase {
	private MaeProcesoVerificacion maeProcesoVerificacion;
    private MaeProcesoElectoral maeProcesoElectoral;
    private MaeAmbitoElectoral maeAmbitoElectoral;
    private MaeUbigeo maeUbigeo;
}
