package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionC;

import java.util.List;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionE;

@Service
public interface MaeLocalVotacionHistoricoServicio {
    public List<MaeLocalVotacion> listarLocalesVotacionHistorico(MaeLocalVotacion param) throws Exception;
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionA(MaeLocalVotacion param) throws Exception;
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionB(MaeLocalVotacion param) throws Exception;
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionC(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoDetSeccionC(DetFichaSeccionC param) throws Exception;
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionD(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoDetSeccionD(DetFichaSeccionD param) throws Exception;
    public MaeLocalVotacion obtenerLocalVotacionHistoricoSeccionE(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoDetSeccionE(DetFichaSeccionE param) throws Exception;
}
