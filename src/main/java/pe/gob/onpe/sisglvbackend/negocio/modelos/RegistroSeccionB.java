package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistroSeccionB extends ModeloBase{
	
	private Integer idRegistroSeccionB;
	private VerificacionLV verificacionLV;
	private String seccion;
	private Integer tieneCercoPerimetrico;
	private Integer estadoCercoPerimetrico;
	private Integer cantidadSshh;
	private Integer estadoSshh;
	private Integer tienePatio;
	private Integer cantidadPatio;
	private Integer cantidadCirculoSeguridad;
	private Integer cantidadPuertaAcceso;
	private Integer estadoPuestaAcceso;
	private Integer estadoParedAula;
	private Integer estadoTechoAula;
	private Integer estadoPisoAula;
	private Integer estadoPuertaAula;
	private Integer estadoVentanaAula;
	private Integer tieneAgua;
	private String horarioInicioAgua;
	private String HorarioTerminoAgua;
	private Integer tieneLuz;
	private String horarioInicioLuz;
	private String HorarioTerminoLuz;
	private Integer tieneInternet;
	private String proveedorInternet;
	private String observacionRegistro;
	private String usuarioObservacion;
	private Date fechaObservacion;
	private String usuarioAprobacion;
	private Date fechaAprobacion;
	private String suministroLuz;
	private Float areaAproximadaM2;
	private Float areaConstruidaM2;
	private Float areaSinConstruirM2; 
	private Integer terminado;
	private Integer idEstado;
	private Integer estadoVerificacion;
	private List<RegistroSeccionB> registrosSeccionB;

}
