package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.*;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral.ProyeccionParte1InputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral.ProyeccionParte2InputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.GenericoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.ProyeccionParte1OutputDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ProspectivaServicio {

	public void listarPeriodos(Periodo periodo);
	GenericoOutputDto proyeccionParte1(ProyeccionParte1InputDto proyeccionParte1InputDto) throws Exception;
	GenericoOutputDto proyeccionParte2(ProyeccionParte2InputDto proyeccionParte2InputDto) throws Exception;

	ProspectivaEstadisticaResultado obtenerEstadisticaResultado(ProspectivaEstadisticaResultado prospectivaEstadisticaResultado) throws Exception;
	ProspectivaProyeccionResultado obtenerProyeccionResultado(ProspectivaProyeccionResultado prospectivaProyeccionResultado) throws Exception;


	void obtenerExcelEstadisticaResultado(HttpServletRequest request, HttpServletResponse response,
										  ProspectivaEstadisticaResultado prospectivaEstadisticaResultado) throws Exception;
	void obtenerExcelProyeccionResultado(HttpServletRequest request, HttpServletResponse response,
										 ProspectivaProyeccionResultado prospectivaProyeccionResultado) throws Exception;


	public void guardarSeleccion(ProspectivaSeleccion prospectivaSeleccion);
	public void ejecutarProspectiva(ProspectivaEjecucion prospectivaEjecucion);
	public void ejecutarResultadoNacional(ProspectivaResultadoNacional prospectivaResultado);
	public void ejecutarResultadoExtranjero(ProspectivaResultadoExtranjero prospectivaExtranjero);
	
}
