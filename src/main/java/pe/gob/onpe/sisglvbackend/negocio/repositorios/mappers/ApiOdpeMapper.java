package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.beans.ApiOdpe;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Mapper
public interface ApiOdpeMapper {
    void getOdpePorProcesoElectoral(ApiOdpe apiOdpe) throws Exception;

}
