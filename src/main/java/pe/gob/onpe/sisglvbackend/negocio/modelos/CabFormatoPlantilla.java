package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

/**
 * @author glennlq
 * @created 9/22/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CabFormatoPlantilla extends ModeloBase {
    private Integer idFormatoPlantilla;
}
