package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ModeloBase;
import java.util.List;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiOdpe extends ModeloBase {
    Integer idProcesoElectoral;
    String acronimo;
    String nombreOdpe;
    String sede;
    String ubigeo;
    String departamentoContinente;
    String provinciaPais;
    String distritoCiudad;
    String codigoOdpe;
    List<ApiOdpe> lista;
    ApiToken apiToken;
}
