package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.TabConfProcesoVerificacionMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabConfProcesoVerificacionServicio;

@Service
public class TabConfProcesoVerificacionServicioImpl implements TabConfProcesoVerificacionServicio {

	@Autowired
	private TabConfProcesoVerificacionMapper tabConfProcesoVerificacionMapper;
	
	@Override
	public TabConfProcesoVerificacion crearTabConfProcesoVerificacion(
			TabConfProcesoVerificacion tabConfProcesoVerificacion) throws Exception {
		tabConfProcesoVerificacionMapper.crearTabConfProcesoVerificacion(tabConfProcesoVerificacion);
		return tabConfProcesoVerificacion;
	}


}
