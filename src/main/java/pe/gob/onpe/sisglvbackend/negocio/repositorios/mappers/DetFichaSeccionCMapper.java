package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionC;

@Mapper
public interface DetFichaSeccionCMapper {
    
    void obtenerLocalVotacionHistDetSecC(DetFichaSeccionC param) throws Exception;
    
}
