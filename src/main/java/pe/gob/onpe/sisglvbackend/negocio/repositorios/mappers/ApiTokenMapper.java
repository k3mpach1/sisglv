package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;

/**
 * @author glennlq
 * @created 10/25/21
 */
@Mapper
public interface ApiTokenMapper {
    void getToken(ApiToken apiToken) throws Exception;
}
