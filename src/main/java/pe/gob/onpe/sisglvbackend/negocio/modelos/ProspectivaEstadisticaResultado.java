package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProspectivaEstadisticaResultado extends ModeloBase {
    Integer idPadronProyeccion;
    Integer tipoAmbito;

    String ubigeo;
    String departamento;
    String provincia;
    String distrito;
    Double desviacionEstandar;
    Double promedio;
    Double coeficienteVariacion;

    List<ProspectivaEstadisticaResultado> lista;
}
