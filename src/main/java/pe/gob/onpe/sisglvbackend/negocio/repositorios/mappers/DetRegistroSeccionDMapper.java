package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;


import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionD;

@Mapper
public interface DetRegistroSeccionDMapper {
	
	void registrar(DetRegistroSeccionD detFichaSeccionD) throws Exception;
	void listar(DetRegistroSeccionD detFichaSeccionD) throws Exception;
	void actualizar(DetRegistroSeccionD detFichaSeccionD) throws Exception;
	void eliminarArea(DetRegistroSeccionD param)throws Exception;
	
}
