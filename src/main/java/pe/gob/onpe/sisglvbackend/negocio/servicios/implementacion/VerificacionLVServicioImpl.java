package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.EstadoTerminadoNuevoLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.VerificacionLVMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.VerificacionLVServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.EstadoTerminadoNuevoLocalInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.GenericoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.EstadoTerminadoNuevoLocalOutputDto;

import java.util.List;

@Service
public class VerificacionLVServicioImpl implements VerificacionLVServicio {

    @Autowired
    VerificacionLVMapper verificacionLVMapper;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<VerificacionLV> listarVerificacion(VerificacionLV param) throws Exception {
        verificacionLVMapper.listarVerificacionActivos(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "listarVerificacion");
        return param.getVerificaciones().size() == 0 ? null : param.getVerificaciones();
    }

    @Override
    public void enviarAValidacionLV(VerificacionLV param) throws Exception {
        verificacionLVMapper.enviarAValidacionLV(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "enviarAValidacionLV");

    }

    @Override
    public void listarEditarNuevoLocalVotacion(VerificacionLV param) throws Exception {
        verificacionLVMapper.listarEditarNuevoLocalVotacion(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "listarEditarNuevoLocalVotacion");

    }

    @Override
    public void listarValidarNuevoLocalVotacion(VerificacionLV param) throws Exception {
        verificacionLVMapper.listarValidarNuevoLocalVotacion(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "listarValidarNuevoLocalVotacion");

    }

    @Override
    public void validarNuevoRegistroLV(VerificacionLV param) throws Exception {
        verificacionLVMapper.validarNuevoRegistroLV(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "validarNuevoRegistroLV");

    }
    
    @Override
    public void listarLocalVotacionVerificacion(VerificacionLV param) throws Exception {
        verificacionLVMapper.listarLocalVotacionVerificacion(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "listarLocalVotacionVerificacion");
    }

    @Override
    public void totalLocalesVotacion(VerificacionLV param) throws Exception {
        verificacionLVMapper.totalLocalesVotacion(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "totalLocalesVotacion");
    }
    
    @Override
    public void listarLocalVotacionValidacion(VerificacionLV param) throws Exception {
        verificacionLVMapper.listarLocalVotacionValidacion(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "listarLocalVotacionVerificacion");
    }

    @Override
    public void totalLocalesValidacion(VerificacionLV param) throws Exception {
        verificacionLVMapper.totalLocalesValidacion(param);
        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "totalLocalesVotacion");
    }

	@Override
	public void listarEditarVerificacionLocalVotacion(VerificacionLV param) throws Exception {
		 //System.out.println("param:"+param.getMaeProcesoElectoral().getIdProcesoElectoral());
		 //System.out.println("param:"+param.getMaeProcesoVerificacion().getIdProcesoVerificacion());
		 //System.out.println("param:"+param.getMaeUbigeoNivel1().getIdUbigeo());
		 //System.out.println("param:"+param.getMaeUbigeoNivel2().getIdUbigeo());
		 //System.out.println("param:"+param.getRegistroSeccionA().getCentroPoblado().getId());
		 //System.out.println("param:"+param.getMaeAmbitoElectoral().getIdAmbitoElectoral());
		 verificacionLVMapper.listarEditarVerificacionLocalVotacion(param);
	     Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "listarEditarVerificacionLocalVotacion");
		
	}

	@Override
	public void listarValidarLocalVotacionHistorico(VerificacionLV param) throws Exception {
		verificacionLVMapper.listarValidarLocalVotacionHistorico(param);
	     Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "listarValidarLocalVotacionHistorico");
		
		
	}

	@Override
	public void validarVerificacionLV(VerificacionLV param) throws Exception {
		 	verificacionLVMapper.validarVerificacionLV(param);
	        Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "validarVerificacionLV");

		
	}

	@Override
	public void actualizarEstadoDisponiblel(VerificacionLV param) throws Exception {
		verificacionLVMapper.actualizarEstadoDisponible(param);
		Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicioImpl.class, "actualizarEstadoDisponible");
	}

	@Override
	public void cancelarRegistroVerificacion(VerificacionLV param) throws Exception {
		verificacionLVMapper.cancelarRegistroVerificacion(param);
		Funciones.validarOperacionConBaseDatos(param, VerificacionLVServicio.class, "cancelarRegistroVerificacion");
		
	}

    @Override
    public GenericoOutputDto obtenerEstadoTerminadoNuevoLocal(EstadoTerminadoNuevoLocalInputDto param) throws Exception {
        EstadoTerminadoNuevoLocalVotacion votacion = EstadoTerminadoNuevoLocalVotacion.builder().idVerificacionLv(param.getIdValidacionLv()).build();
        verificacionLVMapper.obtenerEstadoTerminadoNuevoLocal(votacion);
        Funciones.validarOperacionConBaseDatos(votacion, VerificacionLVServicioImpl.class, "obtenerEstadoTerminadoNuevoLocal");
        EstadoTerminadoNuevoLocalOutputDto resultado = new EstadoTerminadoNuevoLocalOutputDto();
        if(votacion.getLista().size() > 0){
            resultado = modelMapper.map(votacion.getLista().get(0), EstadoTerminadoNuevoLocalOutputDto.class)  ;
        }
        return GenericoOutputDto.builder().resultado(votacion.getResultado()).mensaje(votacion.getMensaje()).datos(resultado).build();
    }
}
