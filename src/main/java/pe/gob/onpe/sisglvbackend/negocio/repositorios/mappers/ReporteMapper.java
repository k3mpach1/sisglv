package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;


import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.beans.*;
import pe.gob.onpe.sisglvbackend.utils.ReporteUtil;

@Mapper
public interface ReporteMapper {
	
	public void reporteRegistrosOrc(ReporteRegistrosAmbitoElectoral reporte) throws Exception;

	public void reporteRegistrosRegistrador(ReporteRegistrosPorRegistrador reporte) throws Exception;
	public void reporteRegistrosPorSeccion (ReporteSeccionesSisglv reporte) throws Exception;

	void reporteSeccionAB(ReporteSeccionAB reporte) throws Exception;
	void reporteSeccionC(ReporteSeccionC reporte) throws Exception;
	void reporteSeccionD(ReporteSeccionD reporte) throws Exception;
	void reporteSeccionE(ReporteSeccionE reporte) throws Exception;

}
