package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.Catalogo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCatalogoEstructura;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarCatalogoBasicoOuputDto;


public interface CatalogoServicio {
    
    void listarCatalogosActivos(DetCatalogoEstructura param) throws Exception;
    ResponseListarCatalogoBasicoOuputDto listarCatalogosActivosPorMaestroYColumna(String maestro, String columna) throws Exception;

}
