package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionC;

@Mapper
public interface FichaSeccionCMapper {
    
    void listarFichaSeccionC(FichaSeccionC param) throws Exception;
    
}
