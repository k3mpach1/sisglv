package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.ArchivoLV;

@Mapper
public interface ArchivoMapper {

	public ArchivoLV listarCatalogosActivos(ArchivoLV archivoLV) throws Exception;
	
}
