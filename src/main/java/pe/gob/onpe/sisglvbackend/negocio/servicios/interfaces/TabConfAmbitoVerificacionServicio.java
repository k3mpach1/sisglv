package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfAmbitoVerificacion;

public interface TabConfAmbitoVerificacionServicio {
    TabConfAmbitoVerificacion crearTabConfAmbitoVerificacion(TabConfAmbitoVerificacion tabConfAmbitoVerificacion) throws Exception;
}
