package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FichaSeccionA extends ModeloBase {
	//LV = local de votacion
	private Integer idFichaSeccionA;
	private MaeLocalVotacion maeLocalVotacion;
	private Integer idUnidadOrganica;
	private String nombreUnidadOrganica;
	private String seccion;
	private String codigoLV;
	private String codigoMinedu;
	private String nivelMinedu;
	private String dniRegistrador;
	private String nombreApellidoRegistrador;
	private Integer idTipoAmbitoGeografico;
	private String nombreTipoAmbitoGeografico;

	private Integer idTipoAmbitoElectoral;
	private String nombreTipoAmbitoElectoral;
	private Integer idAmbitoElectoral;
	private String nombreAmbitoElectoral;

	private String ubigeo;

	private Integer idUbigeoNivel1;
	private Integer idUbigeoNivel2;
	private Integer idUbigeoNivel3;

	private String codigoUbigeoNivel1;
	private String codigoUbigeoNivel2;
	private String codigoUbigeoNivel3;
	private String nombreUbigeoNivel1;
	private String nombreUbigeoNivel2;
	private String nombreUbigeoNivel3;
	private String nombreCentroPoblado;
	private String nombreLocalVotacion;

	private Integer idTipoLocalVotacion;
	private String nombreTipoLocalVotacion;


	private String numeroLocalVotacion;

	private MaeCentroPoblado maeCentroPoblado;
	private Direccion direccion;
	private String referencia;
	private String latitudDistrito;
	private String longitudDistrito;
	private String latitudPuertaAcceso;
	private String longitudPuertaAcceso;
	private String dniRepresentanteLV;
	private String nombreApeRepresentanteLV;
	private String cargoRepresentanteLV;
	private String telefonoRepresentanteLV;
	private String emailRepresentanteLV;

	private String telefonoRepresentateLV;
	private String dniRepresentanteApafa;
	private String nombreApeRepresentanteApafa;
	private String telefonoRepresentanteApafa;
	//privada publica
	private Integer idTipoInstitucion;
	private String nombreTipoInstitucion;
	private Integer idCategoriaInstitucion;
	private String nombreCategoriaInstitucion;

	private Integer idNivelInstruccion;
	private String nombreNivelInstruccion;
	
	private String usuarioAprobacion;
	private Date fechaAprobacion;


	private Float distanciaCentroDistrito;
	private String tiempoCentroDistrito;

	private Integer cantidadAulaDisponible;
	private Integer cantidadMesaDisponible;

	private Integer idSolucionTecnogica;
	private String nombreSolucionTecnogica;

}
