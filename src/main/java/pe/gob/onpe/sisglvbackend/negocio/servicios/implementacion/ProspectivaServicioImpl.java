package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import com.google.gson.Gson;
import net.sf.jasperreports.engine.JasperPrint;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.*;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.ProspectivaMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProcesoElectoralServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProspectivaServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.utils.ReporteUtil;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral.ProyeccionParte1InputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral.ProyeccionParte2InputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.GenericoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.ProyeccionParte1OutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.ProyeccionParte2OutputDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ProspectivaServicioImpl implements ProspectivaServicio {

	private final String EXCEL_ESTADISTICA_RESULTADO = "/WEB-INF/reportes/Estadistica_resultado.jasper";
	private final String EXCEL_PROYECCION_RESULTADO = "/WEB-INF/reportes/Proyeccion_resultado.jasper";
//	private final String EXCEL_ESTADISTICA_RESULTADO = "reportes/Estadistica_resultado.jasper";
//	private final String EXCEL_PROYECCION_RESULTADO = "reportes/Proyeccion_resultado.jasper";
	private ProspectivaMapper prospectivaMapper;
	private ProcesoElectoralServicio procesoElectoralServicio;
	private ModelMapper modelMapper;
	private ReporteUtil reporteUtil;

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	public void ProspectivaServicioImpl(ProspectivaMapper prospectivaMapper,
			ProcesoElectoralServicio procesoElectoralServicio,
			ModelMapper modelMapper) {
		this.prospectivaMapper = prospectivaMapper;
		this.procesoElectoralServicio = procesoElectoralServicio;
		this.modelMapper = modelMapper;
		this.reporteUtil = new ReporteUtil();
		//this.resourceLoader = resourceLoader;
	}

	@Override
	public void listarPeriodos(Periodo periodo) {
		prospectivaMapper.listarPeriodos(periodo);
	}

	@Override
	public GenericoOutputDto proyeccionParte1(ProyeccionParte1InputDto proyeccionParte1InputDto) throws Exception {
		String usuarioCreacion = "00000";
		MaeProcesoElectoral procesoElectoral = procesoElectoralServicio.getProcesoElectoralById(proyeccionParte1InputDto.getIdProcesoElectoral());
		ProspectivaProyeccionParte1 parte1 = buildProspectivaProyeccionParte1(proyeccionParte1InputDto,procesoElectoral,
				usuarioCreacion);
		prospectivaMapper.proyeccionParte1(parte1);
		Funciones.validarOperacionConBaseDatos(parte1, ProspectivaServicioImpl.class, "proyeccionParte1");
		generarProyeccionParte_1(parte1);
		return buildGenericoOutputDto(parte1.getResultado(), parte1.getMensaje(),
				modelMapper.map(parte1, ProyeccionParte1OutputDto.class) );
	}

	private void generarProyeccionParte_1(ProspectivaProyeccionParte1 prospectiva1) throws Exception {
		System.err.println(new Gson().toJson(prospectiva1));
		GenerarProyeccionParte1 parte1 = GenerarProyeccionParte1.builder()
				.idPadronProyeccion(prospectiva1.getIdPadronProyeccion()).build();
		parte1.setUsuarioCreacion(prospectiva1.getUsuarioCreacion());
		prospectivaMapper.generarProyeccionParte1(parte1);
		System.err.println(new Gson().toJson(parte1));
		Funciones.validarOperacionConBaseDatos(parte1, ProspectivaServicioImpl.class, "generarProyeccionParte_1");
	}

	private Date strToDate(String date) throws Exception {
		String sDate1="31/12/1998";
		return new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
	}


	private ProspectivaProyeccionParte1 buildProspectivaProyeccionParte1(
			ProyeccionParte1InputDto proyeccion, MaeProcesoElectoral procesoElectoral,
			String usuarioCreacion
	) throws Exception {
		ProspectivaProyeccionParte1 parte1 = ProspectivaProyeccionParte1.builder()
				.idProcesoElectoral(procesoElectoral.getIdProcesoElectoral())
				.nombreProyeccion(proyeccion.getNombreProyeccion())
				.procesoElectoral(procesoElectoral)
				.padronInicio(proyeccion.getPadronInicio())
				.padronFin(proyeccion.getPadronFin())
				.padronReferencia(proyeccion.getPadronReferencia())
				.fechaFin(strToDate(proyeccion.getFechaInicio()))
				.fechaFin(strToDate(proyeccion.getFechaFin()))
				.build();
		parte1.setUsuarioCreacion(usuarioCreacion);
		return parte1;
	}

	@Override
	public GenericoOutputDto proyeccionParte2(ProyeccionParte2InputDto proyeccionParte2InputDto) throws Exception {
		String usuarioCreacion = "00000";
		ProspectivaProyeccionParte2 parte2 = modelMapper.map(proyeccionParte2InputDto, ProspectivaProyeccionParte2.class);
		parte2.setUsuarioCreacion(usuarioCreacion);
		prospectivaMapper.proyeccionParte2(parte2);
		Funciones.validarOperacionConBaseDatos(parte2, ProspectivaServicioImpl.class, "proyeccionParte2");
		generarProyeccionParte_2(parte2);
		return buildGenericoOutputDto(parte2.getResultado(), parte2.getMensaje(),
				modelMapper.map(parte2, ProyeccionParte2OutputDto.class) );
	}

	private void generarProyeccionParte_2(ProspectivaProyeccionParte2 prospectiva2) throws Exception {
		GenerarProyeccionParte2 parte2 = GenerarProyeccionParte2.builder()
				.idPadronProyeccion(prospectiva2.getIdPadronProyeccion()).build();
		parte2.setUsuarioCreacion(prospectiva2.getUsuarioCreacion());
		prospectivaMapper.generarProyeccionParte2(parte2);
		Funciones.validarOperacionConBaseDatos(parte2, ProspectivaServicioImpl.class, "generarProyeccionParte_2");
	}

	private GenericoOutputDto buildGenericoOutputDto(Integer resultado, String mensaje, Object datos) throws Exception {
		return GenericoOutputDto.builder().resultado(resultado).mensaje(mensaje).datos(datos).build();
	}

	@Override
	public ProspectivaEstadisticaResultado obtenerEstadisticaResultado(ProspectivaEstadisticaResultado prospectivaEstadisticaResultado) throws Exception {
		prospectivaMapper.obtenerEstadisticaResultado(prospectivaEstadisticaResultado);
		Funciones.validarOperacionConBaseDatos(prospectivaEstadisticaResultado, ProspectivaServicioImpl.class, "obtenerEstadisticaResultado");
		return prospectivaEstadisticaResultado;
	}

	@Override
	public ProspectivaProyeccionResultado obtenerProyeccionResultado(ProspectivaProyeccionResultado prospectivaProyeccionResultado) throws Exception {
		prospectivaMapper.obtenerProyeccionResultado(prospectivaProyeccionResultado);
		Funciones.validarOperacionConBaseDatos(prospectivaProyeccionResultado, ProspectivaServicioImpl.class, "obtenerProyeccionResultado");
		return prospectivaProyeccionResultado;
	}

	@Override
	public void obtenerExcelEstadisticaResultado(HttpServletRequest request, HttpServletResponse response, ProspectivaEstadisticaResultado prospectivaEstadisticaResultado) throws Exception {
		prospectivaEstadisticaResultado = obtenerEstadisticaResultado(prospectivaEstadisticaResultado);
		List<JasperPrint> printList = buildListJasperPrint(request, prospectivaEstadisticaResultado.getLista(), EXCEL_ESTADISTICA_RESULTADO);
		List<String> nombres = new ArrayList<>();
		nombres.add("Estadistica");
		renderFileExcel(response, printList, nombres, "Resultado_estadística.xlsx");
	}

	@Override
	public void obtenerExcelProyeccionResultado(HttpServletRequest request, HttpServletResponse response, ProspectivaProyeccionResultado prospectivaProyeccionResultado) throws Exception {
		prospectivaProyeccionResultado = obtenerProyeccionResultado(prospectivaProyeccionResultado);
		List<JasperPrint> printList = buildListJasperPrint(request, prospectivaProyeccionResultado.getLista(), EXCEL_PROYECCION_RESULTADO);
		List<String> nombres = new ArrayList<>();
		nombres.add("Proyección");
		renderFileExcel(response, printList, nombres, "Resultado_proyección.xlsx");
	}

	private List<JasperPrint> buildListJasperPrint(HttpServletRequest request, List<?> list, String reportName) throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("TOTAL_REGISTROS", list.size());
		List<JasperPrint> jasperPrintList = new ArrayList<>();
		jasperPrintList.add(reporteUtil.fillReportJasperPrint(request, params, list, reportName, this.resourceLoader));
		return jasperPrintList;
 	}

	private void renderFileExcel(HttpServletResponse response, List<JasperPrint> jasperPrintList,
											  List<String> nombresHojaExcel, String fileNameOut) throws Exception {
		InputStream inputStream = reporteUtil.exportReportExcelStream(jasperPrintList, nombresHojaExcel.toArray(new String[0]));
		reporteUtil.renderFileExcel(response, inputStream, fileNameOut);
	}


	@Override
	public void guardarSeleccion(ProspectivaSeleccion prospectivaSeleccion) {
		prospectivaMapper.guardarSeleccion(prospectivaSeleccion);
	}

	@Override
	public void ejecutarProspectiva(ProspectivaEjecucion prospectivaEjecucion) {
		prospectivaMapper.ejecutarProspectiva(prospectivaEjecucion);
	}

	@Override
	public void ejecutarResultadoNacional(ProspectivaResultadoNacional prospectivaResultadoNacional) {
		prospectivaMapper.ejecutarResultadoNacional(prospectivaResultadoNacional);
	}
	
	@Override
	public void ejecutarResultadoExtranjero(ProspectivaResultadoExtranjero prospectivaResultadoExtranjero) {
		prospectivaMapper.ejecutarResultadoExtranjero(prospectivaResultadoExtranjero);
	}

}
