package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeLocalVotacion;

@Mapper
public interface MaeLocalVotacionHistoricoMapper {
    public void listarLocalesVotacionHistorico(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoSeccionA(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoSeccionB(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoCabSeccionC(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoCabSeccionD(MaeLocalVotacion param) throws Exception;
    public void obtenerLocalVotacionHistoricoCabSeccionE(MaeLocalVotacion param) throws Exception;
    
}
