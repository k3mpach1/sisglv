package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCargaArchivo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCargaArchivo;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.DetCargaArchivoMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.DetCargaArchivoServicio;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotFoundRegisterInDb;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

import java.util.List;

@Service
public class DetCargaArchivoServicioImpl implements DetCargaArchivoServicio {

    @Autowired
    private DetCargaArchivoMapper detCargaArchivoMapper;

    @Override
    public DetCargaArchivo crearDetCargaArchivo(DetCargaArchivo detCargaArchivo) throws Exception {
        detCargaArchivoMapper.crearDetCargaArchivo(detCargaArchivo);
        Funciones.validarOperacionConBaseDatos(detCargaArchivo, DetCargaArchivoServicioImpl.class, "crearDetCargaArchivo");
        return detCargaArchivo;
    }

    @Override
    public DetCargaArchivo obtenerDetCargaArchivoPorId(Integer idDetCargaArchivo) throws Exception {
        DetCargaArchivo detCargaArchivo = DetCargaArchivo.builder().idDetCargaArchivo(idDetCargaArchivo).build();
        detCargaArchivoMapper.obtenerDetCargaArchivoPorId(detCargaArchivo);
        Funciones.validarOperacionConBaseDatos(detCargaArchivo, DetCargaArchivoServicioImpl.class, "obtenerDetCargaArchivoPorId");
        if(detCargaArchivo.getLista().size() == 0) {
            throw new NotFoundRegisterInDb("DET_CARGA_ARCHIVO", "N_DET_CARGA_ARCHIVO_PK", idDetCargaArchivo);
        }
        return detCargaArchivo.getLista().get(0);
    }

    @Override
    public List<DetCargaArchivo> listaDetCargaArchivoPorIdCargarArchivo(Integer idCargarArchivo) throws Exception {
        DetCargaArchivo detCargaArchivo = DetCargaArchivo.builder().cabCargaArchivo(
                CabCargaArchivo.builder().idCargarArchivo(idCargarArchivo).build()
                ).build();
        detCargaArchivoMapper.listaDetCargaArchivoPorIdCargarArchivo(detCargaArchivo);
        Funciones.validarOperacionConBaseDatos(detCargaArchivo, DetCargaArchivoServicioImpl.class, "listaDetCargaArchivoPorIdCargarArchivo");
        return detCargaArchivo.getLista();
    }
}
