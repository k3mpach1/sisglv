package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.LocalVotacionGenerico;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeLocalVotacionMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeLocalVotacionServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.ListaLocalVotacionDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.ListaLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.LocalVotacionOutputDto;

import java.util.ArrayList;
import java.util.List;

@Service
public class MaeLocalVotacionServicioImpl implements MaeLocalVotacionServicio {

    @Autowired
    private MaeLocalVotacionMapper maeLocalVotacionMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<LocalVotacionGenerico> getListaLocalVotacionGenericoPorPE(LocalVotacionGenerico localVotacionGenerico) throws Exception {
        maeLocalVotacionMapper.getLocalVotacionGenericoPorPE(localVotacionGenerico);
        Funciones.validarOperacionConBaseDatos(localVotacionGenerico, MaeLocalVotacionServicioImpl.class, "getListaLocalVotacionGenericoPorPE");
        return localVotacionGenerico.getLista();
    }

    @Override
    public ListaLocalVotacionOutputDto getListaLocalVotacionGenerico(ListaLocalVotacionDto listaLocalVotacionDto) throws Exception {
        LocalVotacionGenerico localVotacionGenerico = LocalVotacionGenerico.builder().idProcesoElectoral(listaLocalVotacionDto.getIdProcesoElectoral()).build();
        List<LocalVotacionGenerico> lista = this.getListaLocalVotacionGenericoPorPE(localVotacionGenerico);
        return ListaLocalVotacionOutputDto.builder()
                .lista(parseListaLocalVotacionOutputDto(lista))
                .pagina(0)
                .total(lista.size())
                .build();
    }
    
    @Override
    public ListaLocalVotacionOutputDto getListaLocalVotacionOrcGenerico(ListaLocalVotacionDto listaLocalVotacionDto) throws Exception {
        LocalVotacionGenerico localVotacionGenerico = LocalVotacionGenerico.builder().idProcesoVerificacion(listaLocalVotacionDto.getIdProcesoVerificacion()).build();
        List<LocalVotacionGenerico> lista = this.getLocalVotacionOrcGenericoPorPE(localVotacionGenerico);
        return ListaLocalVotacionOutputDto.builder()
                .lista(parseListaLocalVotacionOutputDto(lista))
                .pagina(0)
                .total(lista.size())
                .build();
    }

    private List<LocalVotacionOutputDto> parseListaLocalVotacionOutputDto(List<LocalVotacionGenerico> lista) throws Exception {
        List<LocalVotacionOutputDto> resultado = new ArrayList<>();
        LocalVotacionOutputDto out = null;
        Long item = 1L;
        for(LocalVotacionGenerico local : lista) {
        	out = modelMapper.map(local, LocalVotacionOutputDto.class);
        	out.setItem(item++);
            resultado.add(out);
        }
        return resultado;
    }

	@Override
	public List<LocalVotacionGenerico> getLocalVotacionOrcGenericoPorPE(LocalVotacionGenerico localVotacionGenerico)
			throws Exception {
		 maeLocalVotacionMapper.getLocalVotacionOrcGenericoPorPE(localVotacionGenerico);
		return localVotacionGenerico.getLista();
	}
}
