package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.LocalVotacionGenerico;

/**
 * @author glennlq
 * @created 9/24/21
 */
@Mapper
public interface MaeLocalVotacionMapper {
    void getLocalVotacionGenericoPorPE(LocalVotacionGenerico localVotacionGenerico) throws Exception;
    void getLocalVotacionOrcGenericoPorPE(LocalVotacionGenerico localVotacionGenerico) throws Exception;
}
