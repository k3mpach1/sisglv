package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mensaje {
	private String mensaje;
	private String mensajeInterno;
	private Integer resultado;
}
