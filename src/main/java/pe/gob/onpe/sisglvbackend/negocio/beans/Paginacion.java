package pe.gob.onpe.sisglvbackend.negocio.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Paginacion extends Mensaje {
	private Integer totalRegistroPorPagina;
	private Integer pagina;
	private Integer totalPaginas;
	private Integer totalRegistros;
}
