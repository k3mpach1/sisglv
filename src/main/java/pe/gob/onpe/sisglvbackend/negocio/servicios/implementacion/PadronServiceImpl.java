package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.gob.onpe.sisglvbackend.negocio.beans.Persona;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.PadronService;
import pe.gob.onpe.sisglvbackend.transversal.properties.PadronProperties;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.general.ConsultaPadronInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ConsultaPadronOutputDto;




@Service
public class PadronServiceImpl implements PadronService{

	@Autowired
	private PadronProperties padronProperties;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public void consultarPadron(Persona persona) {
		ConsultaPadronInputDto peticion = new ConsultaPadronInputDto();
		peticion.setDni(persona.getNumeroDocumento());
		peticion.setCodigo(padronProperties.getCodigo());
		peticion.setClave(padronProperties.getClave());
		
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			HttpEntity<ConsultaPadronInputDto> requestEntity = new HttpEntity<ConsultaPadronInputDto>(peticion,
					headers);

			// ResponseEntity<ConsultaPadronOutputDto> responsePadron =
			// restTemplate.postForEntity(padronProperties.getUrl(), peticion,
			// ConsultaPadronOutputDto.class);

			ResponseEntity<ConsultaPadronOutputDto> responsePadron = restTemplate.exchange(padronProperties.getUrl(),
					HttpMethod.POST, requestEntity, ConsultaPadronOutputDto.class);

			ConsultaPadronOutputDto padronDto = responsePadron.getBody();
			
				
			if(padronDto != null && !StringUtils.isEmpty(padronDto.getNombres())){
			
				String nombres = StringUtils.isEmpty(padronDto.getNombres())?"":padronDto.getNombres();
				String apellidoPaterno = StringUtils.isEmpty(padronDto.getAppat())?"":padronDto.getAppat();
				String apellidoMaterno = StringUtils.isEmpty(padronDto.getApmat())?"":padronDto.getApmat();
				
				persona.setNumeroDocumento(padronDto.getNumele());
				persona.setNombres(nombres +" "+ apellidoPaterno +" "+ apellidoMaterno);
				persona.setApellidoPaterno(padronDto.getAppat());
				persona.setApellidoMaterno(padronDto.getApmat());
				persona.setSoloNombres(padronDto.getNombres());
				
				persona.setMensaje("Se ejecutó la operación correctamente");
				persona.setResultado(1);
			} else {
				String mensaje = "El DNI " + persona.getNumeroDocumento() + " no existe.";
				persona.setMensaje(mensaje);
				persona.setResultado(-2);
			}
			

		} catch (Exception e) {
			String mensaje = e.getMessage();
			if (mensaje != null) {
				if (mensaje.contains("Clave de acceso inválida")) {
					mensaje = "Servicio Padrón no disponible: Acceso Inválido.";
				} else if (mensaje.contains("El sistema no está registrado o no existe")) {
					mensaje = "Servicio Padrón no disponible: Cuenta no registrada.";
				} else if (mensaje.contains("no existe")) {
					mensaje = "El DNI " + persona.getNumeroDocumento() + " no existe.";
				} else {
					mensaje = "Servicio Padrón no disponible";
				}
			} else {
				mensaje = "Ocurrió un error al obtener los datos de la persona.";
			}

			persona.setMensaje(mensaje);
			persona.setResultado(-2);
		
		}

	}

}
