package pe.gob.onpe.sisglvbackend.transversal.excepciones;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FieldErrorModel {
	String fieldName;
	String fieldError;
}
