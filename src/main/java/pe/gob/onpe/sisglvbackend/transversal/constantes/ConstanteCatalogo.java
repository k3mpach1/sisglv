package pe.gob.onpe.sisglvbackend.transversal.constantes;

public class ConstanteCatalogo {
	
	
	//TABLAS
	public static final String MAE_TIPO_AREA = "MAE_TIPO_AREA";
	public static final String MAE_TIPO_ESTADOS = "MAE_TIPO_ESTADOS";
	public static final String MAE_TIPO_INSTITUCION = "MAE_TIPO_INSTITUCION";
	public static final String MAE_CATEGORIA_INSTITUCION = "MAE_CATEGORIA_INSTITUCION";
	public static final String MAE_NIVEL_INSTRUCCION = "MAE_NIVEL_INSTRUCCION";
	public static final String MAE_DIRECCION = "MAE_DIRECCION";
	public static final String MAE_TIPO_LV = "MAE_TIPO_LV";
	public static final String MAE_TIPO_AULA = "MAE_TIPO_AULA";
	public static final String MAE_DEPENDENCIA = "MAE_USO_AULA";
	public static final String MAE_TIPO_ELECCION = "MAE_TIPO_ELECCION";
	
	
	
	//COLUMNAS
	public static final String N_ESTADO_CERCO_PERIMETRICO = "N_ESTADO_CERCO_PERIMETRICO";
	public static final String N_ESTADO_PUERTA_ACCESO = "N_ESTADO_PUERTA_ACCESO";
	public static final String N_ESTADO_SSHH = "N_ESTADO_SSHH";
	public static final String N_ESTADO_TECHO_AULA = "N_ESTADO_TECHO_AULA";
	public static final String N_ESTADO_PARED_AULA = "N_ESTADO_PARED_AULA";
	public static final String N_ESTADO_PISO_AULA = "N_ESTADO_PISO_AULA";
	public static final String N_ESTADO_PUERTA_AULA = "N_ESTADO_PUERTA_AULA";
	public static final String N_ESTADO_VENTANA_AULA = "N_ESTADO_VENTANA_AULA";
	public static final String N_TIPO_AREA = "N_TIPO_AREA";
	public static final String N_TIPO_INSTITUCION = "N_TIPO_INSTITUCION";
	public static final String N_CATEGORIA_INSTITUCION = "N_CATEGORIA_INSTITUCION";
	public static final String N_NIVEL_INSTRUCCION = "N_NIVEL_INSTRUCCION";
	public static final String N_TIPO_VIA = "N_TIPO_VIA";
	public static final String N_TIPO_ZONA = "N_TIPO_ZONA";
	public static final String N_TIPO_LV = "N_TIPO_LV";
	
	public static final String N_USO_AULA = "N_USO_AULA";
	public static final String N_UNIDAD_ORGANICA = "N_UNIDAD_ORGANICA";
	public static final String N_SOLUCION_TECNOLOGICA = "N_SOLUCION_TECNOLOGICA";
	public static final String MAE_TIPO_ARCHIVO = "MAE_TIPO_ARCHIVO";
	public static final String MAE_TIPO_AMBITO_ELECTORAL = "MAE_TIPO_AMBITO_ELECTORAL";
	public static final String MAE_TIPO_AMBITO_GEOGRAFICO = "MAE_TIPO_AMBITO_GEOGRAFICO";
	public static final String MAE_ESTADO_LOCAL_VOTACION = "MAE_ESTADO_LOCAL_VOTACION";
	public static final String MAE_ESTADO_AVANCE_REGISTRO_LV = "MAE_ESTADO_AVANCE_REGISTRO_LV";

	public static final String N_CATEGORIA= "N_CATEGORIA";
	public static final String N_TIPO_AMBITO_ELECTORAL= "N_TIPO_AMBITO_ELECTORAL";
	public static final String N_TIPO_AMBITO_GEOGRAFICO= "N_TIPO_AMBITO_GEOGRAFICO";
	public static final String MAE_TIPO_VIA = "MAE_TIPO_VIA";
	public static final String MAE_TIPO_ZONA = "MAE_TIPO_ZONA";
	public static final String MAE_UNIDAD_ORGANICA = "MAE_UNIDAD_ORGANICA";
	public static final String N_TIPO_AULA = "N_TIPO_AULA";
	public static final String N_ESTADO = "N_ESTADO";
	public static final String N_ESTADO_AVANCE_SECCION = "N_ESTADO_AVANCE_SECCION";
	
	

}
