package pe.gob.onpe.sisglvbackend.transversal.excepciones;

public class FTPErrorMessage {

	private int errorCode;
	private String errorMessage;

	public FTPErrorMessage(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	@Override
	public String toString() {
		return "FTPErrorMessage [errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
	}

}
