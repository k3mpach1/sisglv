package pe.gob.onpe.sisglvbackend.transversal.config;

import com.fasterxml.classmate.TypeResolver;
import io.swagger.annotations.Api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Configuration
@Profile({ "!prod && dev" })
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket docket(TypeResolver typeResolver) {

		  return new Docket(DocumentationType.SWAGGER_2)
				  	.groupName("SGLV")
	                .select()
					  //.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				      .apis(RequestHandlerSelectors.any())
					  .paths(PathSelectors.any())
					  .build()

				  .additionalModels(typeResolver.resolve(ExceptionResponse.class))
				  .pathMapping("/")
				  .genericModelSubstitutes(ResponseEntity.class)
				  .useDefaultResponseMessages(false)
	              .apiInfo(apiInfo())
				  //.globalRequestParameters(getGlobalRequestParameters())
	              .globalResponses(HttpMethod.DELETE, getGlobalResponseMessage())
	              .globalResponses(HttpMethod.PUT, getGlobalResponseMessage())
				  .globalResponses(HttpMethod.GET, getGlobalResponseMessage())
				  .globalResponses(HttpMethod.POST, getGlobalResponseMessage());
	}
	/**
	 * Encapsulating global general parameters
	 */
	private List<RequestParameter> getGlobalRequestParameters() {
		List<RequestParameter> parameters = new ArrayList<>();
		parameters.add(new RequestParameterBuilder()
				.name("uuid")
				.description("equipment uuid")
				.required(true)
				.in(ParameterType.QUERY)
				.query(q -> q.model(m -> m.scalarModel(ScalarType.STRING)))
				.required(false)
				.build());
		return parameters;
	}

	/**
	 * Encapsulating general response information
	 */
	private List<Response> getGlobalResponseMessage() {
		List<Response> responseList = new ArrayList<>();
		responseList.add(
				new ResponseBuilder()
									.code("404")
									.description("Resource not found")
									.representation(MediaType.ALL)
									.apply(r ->
											r.model(m ->
													m.referenceModel(ref ->
															ref.key(k ->
																	k.qualifiedModelName(q ->
																						q.name("ExceptionResponse")
																						.namespace("pe.gob.onpe.sisglvbackend.transversal.excepciones")
																						.build()
																					))))) //<13>

									.build()
		);
		responseList.add(
				new ResponseBuilder()
						.code("401")
						.description("Unauthorized")
						.representation(MediaType.ALL)
						.apply(r ->
								r.model(m ->
										m.referenceModel(ref ->
												ref.key(k ->
														k.qualifiedModelName(q ->
																q.name("ExceptionResponse")
																		.namespace("pe.gob.onpe.sisglvbackend.transversal.excepciones")
																		.build()
														))))) //<13>

						.build()
		);
		responseList.add(
				new ResponseBuilder()
						.code("403")
						.description("Forbidden")
						.representation(MediaType.ALL)
						.apply(r ->
								r.model(m ->
										m.referenceModel(ref ->
												ref.key(k ->
														k.qualifiedModelName(q ->
																q.name("ExceptionResponse")
																		.namespace("pe.gob.onpe.sisglvbackend.transversal.excepciones")
																		.build()
														))))) //<13>

						.build()
		);
		responseList.add(
				new ResponseBuilder()
						.code("403")
						.description("Forbidden")
						.representation(MediaType.ALL)
						.apply(r ->
								r.model(m ->
										m.referenceModel(ref ->
												ref.key(k ->
														k.qualifiedModelName(q ->
																q.name("ExceptionResponse")
																		.namespace("pe.gob.onpe.sisglvbackend.transversal.excepciones")
																		.build()
														))))) //<13>

						.build()
		);
		responseList.add(
				new ResponseBuilder()
						.code("500")
						.description("Internal server error")
						.representation(MediaType.ALL)
						.apply(r ->
								r.model(m ->
										m.referenceModel(ref ->
												ref.key(k ->
														k.qualifiedModelName(q ->
																q.name("ExceptionResponse")
																		.namespace("pe.gob.onpe.sisglvbackend.transversal.excepciones")
																		.build()
														))))) //<13>

						.build()
		);
		return responseList;
	}
	private ApiKey apiKey() {
		return new ApiKey("jwtToken", "Authorization", "header");
	}


	/**
	 * Generate Api Info
	 *
	 * @return Swagger API Info
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("SGLV")
				.description("Descripción del sistema")
				.version("0.1-SNAPSHOT")
				.termsOfServiceUrl("https://es.wikipedia.org/wiki/Sadr")
				.license("Open source licensing")
				.licenseUrl("https://help.github.com/articles/open-source-licensing/")
				.build();
	}

}
