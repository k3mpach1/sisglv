package pe.gob.onpe.sisglvbackend.transversal.properties;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Properties;

@Component
@Log4j2
public class ReporteProperties {

	@Resource(mappedName = "resource/sisglv_properties")
	private Properties properties;

	public String getClave() {
		return this.getProperty("reporte.clave");
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}
