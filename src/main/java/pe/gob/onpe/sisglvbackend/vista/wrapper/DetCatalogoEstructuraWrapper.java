package pe.gob.onpe.sisglvbackend.vista.wrapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCatalogoEstructura;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCatalogoBasicoOutputDto;

public class DetCatalogoEstructuraWrapper {
	
	
	public static final ListarCatalogoBasicoOutputDto obtenerListarCatalogoBasicoOutputDto(DetCatalogoEstructura origen) {
		if(origen==null) {
			return null;
		}
		ListarCatalogoBasicoOutputDto destino = new ListarCatalogoBasicoOutputDto();
		destino.setCodigo(origen.getCodigo());
		destino.setNombre(origen.getNombre());
		destino.setOrden(origen.getOrden());
		destino.setActivo(origen.getActivo());
		
		return destino;
	}

}
