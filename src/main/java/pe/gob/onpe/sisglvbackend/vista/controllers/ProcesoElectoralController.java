package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;

import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeLocalVotacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProcesoElectoralServicio;

import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.*;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.AsignarOdpeAProcesoElectoralOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.AsignarProcesoElectoralOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.EstadoConfigPEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarProcesoElectoralActivosOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarProcesoElectoralActivosOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarProcesoElectoralActivosOuputDto;
import pe.gob.onpe.sisglvbackend.vista.wrapper.MaeProcesoElectoralWrapper;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/proceso-electoral")
@Validated
public class ProcesoElectoralController {
	@Autowired
	ProcesoElectoralServicio procesoElectoralServicio;

	@Autowired
	MaeLocalVotacionServicio maeLocalVotacionServicio;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	JWTTokenProvider jwtTokenProvider;
	
	@ApiOperation(value = "listar procesos electorales activos", 
			notes = "Permite listar los procesos electorales activos"						
			)
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", 
		  					value = "Access Token", 
		  					required = true, 
		  					allowEmptyValue = false, 
		  					paramType = "header", 
		  					dataTypeClass = String.class, 
		  					example = "Bearer access_token")			  
	)
	@GetMapping(value = "/listar-procesos-electorales-activos")
	public ResponseEntity<ResponseListarProcesoElectoralActivosOuputDto> listarProcesosElectoralesActivos() throws Exception {

		MaeProcesoElectoral param = new MaeProcesoElectoral();
				
		List<MaeProcesoElectoral> paramOut = procesoElectoralServicio.listarActivos(param);
		List<ListarProcesoElectoralActivosOuputDto> paramDtoOut = MaeProcesoElectoralWrapper.listarActivosModelToDtoWrapper(paramOut);
	
		DatosListarProcesoElectoralActivosOuputDto objDto = new DatosListarProcesoElectoralActivosOuputDto();
		objDto.setProcesosElectorales(paramDtoOut);
		
		ResponseListarProcesoElectoralActivosOuputDto outDto = new ResponseListarProcesoElectoralActivosOuputDto();
		outDto.setResultado(param.getResultado());
		outDto.setMensaje(param.getMensaje());
		outDto.setDatos(objDto);
		
		return new ResponseEntity<>(outDto, HttpStatus.OK);
	}

	@PostMapping(value = "/crear-proceso-electoral")
	public ResponseEntity<?> crearProcesoElectoral(
			@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			@RequestBody AsignarProcesoElectoralDto asignarProcesoElectoralDto) {
		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		MaeProcesoElectoral procesoElectoral = procesoElectoralServicio.crearProcesoElectoral(modelMapper.map(asignarProcesoElectoralDto, MaeProcesoElectoral.class), numeroDocumento);
		return ResponseEntity.ok(
				modelMapper.map(procesoElectoral, AsignarProcesoElectoralOutputDto.class)
		);
	}

	@PostMapping(value = "/cargar-archivo-odpes")
	public ResponseEntity<?> cargarArchivoOdpes(
			@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			@RequestParam("idProcesoElectoral") Integer idProcesoElectoral, @RequestParam("file") MultipartFile multipartFile) throws Exception {
		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		return ResponseEntity.ok(procesoElectoralServicio.cargarArchivoOdpesAFtp(idProcesoElectoral, multipartFile, numeroDocumento));
	}


//	@PostMapping(value = "/procesar-archivo-odpes")
//	public ResponseEntity<?> procesarArchivoOdpes(@RequestBody ProcesarArchivoOdpesDto procesarArchivoOdpesDto) throws Exception {
//		return ResponseEntity.ok(procesoElectoralServicio.procesarArchivoOdpes(procesarArchivoOdpesDto));
//	}
//
	@PostMapping(value = "/asignar-odpes-a-proceso-electoral")
	public ResponseEntity<?> asignarOdpesAProcesoElectoral(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,@RequestBody AsignarOdpeAProcesoElectoralDto asignarOdpeAProcesoElectoralDto) throws Exception {
		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		AsignarOdpeAProcesoElectoralOutputDto out = procesoElectoralServicio.asignarOdpesAProcesoElectoral(asignarOdpeAProcesoElectoralDto, token, numeroDocumento);
		//Aqui se enviar la informacion generada al SASA
//		procesoElectoralServicio.registrarProcesoAsignacionOdpeEnSasa(asignarOdpeAProcesoElectoralDto, token);
		return ResponseEntity.ok(AsignarOdpeAProcesoElectoralOutputDto.builder()
				.exito(true)
				.mensaje("Procesando en background").build());
	}

	@PostMapping(value = "/estado-config-proceso-electoral")
	public ResponseEntity<?> estadoConfigProcesoElectoral(@RequestBody EstadoConfigPEInputDto estadoConfigPEInputDto) throws Exception {
		MaeProcesoElectoral procesoElectoral = procesoElectoralServicio.getProcesoElectoralById(estadoConfigPEInputDto.getIdProcesoElectoral());
		return ResponseEntity.ok(
				modelMapper.map(procesoElectoral, EstadoConfigPEOutputDto.class)
		);
	}

	@PostMapping(value = "/lista-local-votacion")
	public ResponseEntity<?> listaLocalVotacion(@RequestBody ListaLocalVotacionDto listaLocalVotacionDto) throws Exception {
		return ResponseEntity.ok(maeLocalVotacionServicio.getListaLocalVotacionGenerico(listaLocalVotacionDto));
	}
}
