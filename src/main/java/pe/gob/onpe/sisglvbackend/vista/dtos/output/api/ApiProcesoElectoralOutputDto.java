package pe.gob.onpe.sisglvbackend.vista.dtos.output.api;

import lombok.*;

/**
 * @author glennlq
 * @created 10/26/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiProcesoElectoralOutputDto {
    private String nombre;
    private String acronimo;
}
