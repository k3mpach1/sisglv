package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import lombok.Getter;
import lombok.Setter;

/**
 * @author glennlq
 * @created 9/22/21
 */
@Getter
@Setter
public class ProcesarArchivoOdpesDto {
    private Integer idArchivo;
    private Integer idProcesoElectoral;
}
