package pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProyeccionParte1InputDto {
    String nombreProyeccion;
    Integer idProcesoElectoral;
    Integer padronInicio;
    Integer padronFin;
    Integer padronReferencia;
    String fechaInicio;
    String fechaFin;
}
