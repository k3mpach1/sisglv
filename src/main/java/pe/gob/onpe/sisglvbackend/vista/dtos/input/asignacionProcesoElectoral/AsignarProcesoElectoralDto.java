package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author glennlq
 * @created 9/15/21
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AsignarProcesoElectoralDto {

//    private Integer idProcesoElectoral;

    @ApiModelProperty(notes = "Id del Proceso Electoral Padre.", example = "123456", required = false)
    private Integer idProcesoElectoralPadre;
//    private String idProceso;

    @ApiModelProperty(notes = "Nombre del Proceso Electoral.", example = "Elecciones del Universo.", required = true)
    @NotNull(message = "El nombre es campo requerido")
    private String nombre;

    @ApiModelProperty(notes = "Acrónimo del Proceso Electoral.", example = "Elecciones del Universo.", required = true)
    @NotNull(message = "El acrónimo es campo requerido")
    private String acronimo;

//    private String acronimo;
//    private Date fechaConvocatoria;

    @ApiModelProperty(notes = "Ámbito del Proceso Electoral.", example = "1", required = true)
    @NotNull(message = "Tipo de ámbito es campo requerido")
    private Integer tipoAmbitoElectoral;

    @ApiModelProperty(notes = "Tipo Ámbito Geográfico del Proceso Electoral.", example = "1", required = true)
    @NotNull(message = "Tipo de ámbito es campo requerido")
    private Integer tipoAmbitoGeografico;

}
