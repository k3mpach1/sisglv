package pe.gob.onpe.sisglvbackend.vista.wrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeCentroPoblado;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCentroPobladoBasicoOutputDto;

public class CentroPobladoWrapper {

 
    public static final List<ListarCentroPobladoBasicoOutputDto> ListarCentroPobladoBasicoOutputDtoModelToDtoWrapper(List<MaeCentroPoblado> origen) {
        if (origen == null) {
            return null;
        }
        List<ListarCentroPobladoBasicoOutputDto> destinoList = new ArrayList<>();
        destinoList = origen.stream().map(p -> {
        	ListarCentroPobladoBasicoOutputDto data = new ListarCentroPobladoBasicoOutputDto();
            data.setIdUbigeo(p.getId());
            data.setNombre(p.getNombre());
          
            return data;
        }).collect(Collectors.toList());

        return destinoList;
    }

    

}
