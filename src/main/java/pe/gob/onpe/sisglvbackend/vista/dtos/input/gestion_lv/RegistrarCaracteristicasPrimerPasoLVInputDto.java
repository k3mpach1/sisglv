package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarCaracteristicasPrimerPasoLVInputDto {
	@NotNull(message = "Id Verificación es obligatorio")
	private Integer idVerificacion;
	private Integer tieneCercoPerimetrico;
	private Integer idEstadoCerco;
	@NotNull(message="Cantidad de puertas es obligatorio")
	private Integer cantidaPuertasAcceso;
	private Integer idEstadoPuertasAcceso;
	private Integer cantidaSshh;
	private Integer idEstadoSshh;
	private Integer tieneServicioAgua;
	private String desdeHoraServicioAgua;
	private String hastaHoraServicioAgua;
	
	private Integer tieneEnergiaElectrica;
	private String desdeHoraEnergiaElectrica;
	private String hastaHoraEnergiaElectrica;
	
	private Integer tieneServicioIntenet;
	private String proveedorInternet;
}
