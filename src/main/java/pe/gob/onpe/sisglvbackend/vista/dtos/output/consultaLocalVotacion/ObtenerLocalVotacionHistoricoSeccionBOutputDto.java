package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerLocalVotacionHistoricoSeccionBOutputDto {
    private Integer tieneCercoPerimetrico;
    private Integer idEstadoCercoPerimetrico;
    private Integer cantidadSshh;
    private Integer idEstadoSshh;
    private Integer cantidadPuertaAcceso;
    private Integer idEstadoPuertaAcceso;
    private Integer idEstadoConservacionPuerta;
    private Integer idEstadoConservacionTecho;
    private Integer idEstadoConservacionPared;
    private Integer idEstadoConservacionVentada;
    private Integer idEstadoConservacionPiso;

    private Integer tieneServicioAgua;
    private String servicioAguaHoraDesde;
    private String servicioAguaHoraHasta;

    private Integer tieneServicioLuz;
    private String servicioLuzHoraDesde;
    private String servicioluzHoraHasta;

    private Integer tieneServicioInternet;
    private Integer idProveedorInternet;

    private Integer tienePatio;
    private Integer cantidadPatio;

    private Integer cantidadCirculaSeguridad;
    private Integer numeroSuministroLuz;
    private Float areaAproximada;
    private Float areaConstruida;
    private Float areaSinConstruir;

    private String nombreEstadoCercoPerimetrico;
    private String nombreEstadoSshh;
    private Integer idEstadoPuestaAcceso;
    private String nombreEstadoParedAula;
    private Integer idEstadoPuertaAula;
    private String nombreEstadoPuertaAula;
    private Integer idEstadoTechoAula;
    private String nombreEstadoTechoAula;
    private Integer idEstadoParedAula;
    private Integer IdEstadoVentanaAula;
    private String nombreEstadoVentanaAula;

    private Integer idEstadoPisoAula;
    private String nombreEstadoPisoAula;
    private Integer tieneAgua;
    private String horarioInicioAgua;
    private String horarioTerminoAgua;
    private Integer tieneLuz;
    private String horarioInicioLuz;
    private String horarioTerminoLuz;
    private Integer tieneInternet;
    private String nombreProveedorInternet;

    private Integer cantidadCirculoSeguridad;
    private String suministroLuz;
    private Float areaSinContruir;
}
