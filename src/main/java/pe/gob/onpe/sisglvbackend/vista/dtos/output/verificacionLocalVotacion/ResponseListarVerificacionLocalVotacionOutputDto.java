package pe.gob.onpe.sisglvbackend.vista.dtos.output.verificacionLocalVotacion;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
@Schema(name = "ResponseListarVerificacionLocalVotacionOutputDto", description = "The entity that represents Episode")
public class ResponseListarVerificacionLocalVotacionOutputDto extends BaseOutputDto {
    private DatosListarVerificacionLocalVotacionOutputDto datos;
}
