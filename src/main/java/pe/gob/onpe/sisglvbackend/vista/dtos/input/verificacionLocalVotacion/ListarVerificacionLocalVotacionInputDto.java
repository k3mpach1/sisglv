package pe.gob.onpe.sisglvbackend.vista.dtos.input.verificacionLocalVotacion;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ListarVerificacionLocalVotacionInputDto {
    private Integer idProcesoElectoral;
    private Integer idProcesoVerificacion;
    private String nombreLocalVotacion;
    private Integer idEstadoLV;
    private String codigoUbigeoNivel1;
    private String codigoUbigeoNivel2;
    private String codigoUbigeoNivel3;
    private Integer idEstadoVerificacion;
    private Integer pagina;
    private Integer totalRegistroPorPagina;
}
