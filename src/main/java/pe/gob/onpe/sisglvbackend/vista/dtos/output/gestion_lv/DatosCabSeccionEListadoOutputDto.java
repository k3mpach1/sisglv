package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosCabSeccionEListadoOutputDto {

	private Integer idCabRegistroSeccionE;
	private Integer nVerificacion;
	private String  seccion;
	private String  validado;
	private Integer activo;
	private String  observacionRegistro;
	private String  observacionLv;
	private Integer estadoVerificacion;
	private List<DatosDetSeccionEListadoOutputDto> detalleSeccionE;
	
	
}
