package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarCaracteristicasLVPrimerPasoInputDto {
	private Integer idRegistroSeccionB;
	private Integer tieneCercoPerimetrico;
	private Integer idEstadoCerco;
	private Integer cantidaPuertasAcceso;
	private Integer idEstadoPuertasAcceso;
	private Integer cantidaSshh;
	private Integer idEstadoSshh;
	private Integer tieneServicioAgua;
	private String desdeHoraServicioAgua;
	private String hastaHoraServicioAgua;
	private Integer tieneEnergiaElectrica;
	private String desdeHoraEnergiaElectrica;
	private String hastaHoraEnergiaElectrica;
	private Integer tieneServicioIntenet;
	private String proveedorInternet;
}
