package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto extends BaseOutputDto {
	
	private DatosObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto datos;

}
