package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarCaracteristicaAulaOutputDto {
	
	private Integer idRegistroSeccionC;
	private Date fechaObservacion;
	private String observacionRegistro;
	private String usuarioObservacion;
	private Integer estadoVerificacion;

}
