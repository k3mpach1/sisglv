package pe.gob.onpe.sisglvbackend.vista.wrapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.*;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ListarEditarRegistroNuevoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.verificacionLocalVotacion.ListarVerificacionLocalVotacionInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarEditarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarValidarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.verificacionLocalVotacion.ListarVerificacionLocalVotacionOutputDto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.consultaLocalVotacion.ListarFiltroLocalVotacionVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.DatosLocalesVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerListaFiltroLocalVotacionOutputDto;

public class VerificacionLVWrapper {

    public static final void ListarVerificacionLocalVotacionDtoToModelWrapper(ListarVerificacionLocalVotacionInputDto origen, VerificacionLV destino) {
        if (origen == null) {
            return;
        }
        destino.setMaeProcesoElectoral(new MaeProcesoElectoral());
        destino.getMaeProcesoElectoral().setIdProcesoElectoral(origen.getIdProcesoElectoral());

        destino.setMaeProcesoVerificacion(new MaeProcesoVerificacion());
        destino.getMaeProcesoVerificacion().setIdProcesoVerificacion(origen.getIdProcesoVerificacion());

        destino.setMaeLocalVotacion(new MaeLocalVotacion());
        destino.getMaeLocalVotacion().setNombreLocal(origen.getNombreLocalVotacion());
        destino.getMaeLocalVotacion().setIdEstado(origen.getIdEstadoLV());

        destino.setMaeUbigeoNivel1(new MaeUbigeo());
        destino.setMaeUbigeoNivel2(new MaeUbigeo());
        destino.setMaeUbigeoNivel3(new MaeUbigeo());
        
        destino.getMaeUbigeoNivel1().setUbigeo(origen.getCodigoUbigeoNivel1());
        destino.getMaeUbigeoNivel2().setUbigeo(origen.getCodigoUbigeoNivel2());
        destino.getMaeUbigeoNivel3().setUbigeo(origen.getCodigoUbigeoNivel3());
        
        destino.setEstadoVerificacion(origen.getIdEstadoVerificacion());

        destino.setPagina(origen.getPagina());
        destino.setTotalRegistroPorPagina(origen.getTotalRegistroPorPagina());

    }

    public static final List<ListarVerificacionLocalVotacionOutputDto> ListarVerificacionLocalVotacionModelToDtoWrapper(List<VerificacionLV> origen) {
        if (origen == null) {
            return null;
        }
        List<ListarVerificacionLocalVotacionOutputDto> destinoList = new ArrayList<>();
        destinoList = origen.stream().map(p -> {
            ListarVerificacionLocalVotacionOutputDto data = new ListarVerificacionLocalVotacionOutputDto();
            data.setCodigoUbigeo(p.getMaeUbigeo().getUbigeo());
            data.setNombreUbigeoNivel1(p.getMaeUbigeoNivel1().getNombre());
            data.setNombreUbigeoNivel2(p.getMaeUbigeoNivel2().getNombre());
            data.setNombreUbigeoNivel3(p.getMaeUbigeoNivel3().getNombre());
            data.setNombreCentroPoblado("");
            //data.setNombreCentroPoblado(p.getMaeCentroPoblado().getNombre());
            data.setNombreLocalVotacion(p.getMaeLocalVotacion().getNombreLocal());
            data.setIdTipoVia(p.getMaeLocalVotacion().getDireccion().getIdTipoVia());
            data.setNombreVia(p.getMaeLocalVotacion().getDireccion().getNombreVia());
            data.setDireccionNumero(p.getMaeLocalVotacion().getDireccion().getNumero());
            data.setDireccionKilometro(p.getMaeLocalVotacion().getDireccion().getKilometro());
            data.setDireccionManzana(p.getMaeLocalVotacion().getDireccion().getManzana());
            data.setDireccionDepartamento(p.getMaeLocalVotacion().getDireccion().getDepartamento());
            data.setDireccionLote(p.getMaeLocalVotacion().getDireccion().getLote());
            data.setDireccionPiso(p.getMaeLocalVotacion().getDireccion().getPiso());
            data.setIdTipoZona(p.getMaeLocalVotacion().getDireccion().getIdTipoZona());
            data.setNombreZona(p.getMaeLocalVotacion().getDireccion().getNombreZona());
            data.setFechaModificacion(p.getFechaModificacion());
            data.setIdEstadoVerificacion(p.getEstadoVerificacion());
            return data;
        }).collect(Collectors.toList());

        return destinoList;
    }

    public static final void ListarEditarRegistroNuevoLVDtoToModelWrapper(ListarEditarRegistroNuevoLVInputDto origen, VerificacionLV destino) {
        if (origen == null) {
            return;
        }
        //Proceso Electoral
        if (origen.getTipoProceso() == 1) {
            destino.setMaeProcesoElectoral(new MaeProcesoElectoral());
            destino.getMaeProcesoElectoral().setIdProcesoElectoral(origen.getIdProceso());
        } else {
            destino.setMaeProcesoVerificacion(new MaeProcesoVerificacion());
            destino.getMaeProcesoVerificacion().setIdProcesoVerificacion(origen.getIdProceso());
        }
        
        if(origen.getIdAmbitoElectoral() != null) {
        	destino.setMaeAmbitoElectoral(new MaeAmbitoElectoral());
        	destino.getMaeAmbitoElectoral().setIdAmbitoElectoral(origen.getIdAmbitoElectoral());
        }

        destino.setMaeUbigeoNivel1(new MaeUbigeo());
        destino.getMaeUbigeoNivel1().setIdUbigeo(origen.getIdUbigeoDepartamento());
        destino.setMaeUbigeoNivel2(new MaeUbigeo());
        destino.getMaeUbigeoNivel2().setIdUbigeo(origen.getIdUbigeoProvincia());
        destino.setMaeUbigeoNivel3(new MaeUbigeo());
        destino.getMaeUbigeoNivel3().setIdUbigeo(origen.getIdUbigeoDistrito());

        if (origen.getIdCentroPoblado() != null) {
            RegistroSeccionA seccionA = new RegistroSeccionA();
            seccionA.setCentroPoblado(MaeCentroPoblado.builder().id(origen.getIdCentroPoblado()).build());
            destino.setRegistroSeccionA(seccionA);
        }

        destino.setPagina(origen.getPagina());
        destino.setTotalRegistroPorPagina(origen.getTotalRegistroPorPagina());
      
        if(destino.getRegistroSeccionA() != null) {
        	destino.getRegistroSeccionA().setNombreLocalLV(origen.getNombreLocal());
        } else {
        	destino.setRegistroSeccionA(new RegistroSeccionA());
        	destino.getRegistroSeccionA().setNombreLocalLV(origen.getNombreLocal());
        }
    }

    public static final List<ListarEditarRegistroNuevoLVOutputDto> ListarEditarRegistroNuevoLVModelToDtoWrapper(List<VerificacionLV> origen) {
        if (origen == null) {
            return null;
        }
        List<ListarEditarRegistroNuevoLVOutputDto> destinoList = new ArrayList<>();
        destinoList = origen.stream().map(p -> {
            ListarEditarRegistroNuevoLVOutputDto data = new ListarEditarRegistroNuevoLVOutputDto();
            data.setIdVerificacionLV(p.getIdVerificacionLV());
            data.setIdLocalVotacion(p.getMaeLocalVotacion().getIdLocalVotacion());
            data.setNombreLocalVotacion(p.getMaeLocalVotacion().getNombreLocal());

            if (p.getMaeLocalVotacion().getDireccion() != null) {
                data.setIdTipoVia(p.getMaeLocalVotacion().getDireccion().getIdTipoVia());
                data.setNombreVia(p.getMaeLocalVotacion().getDireccion().getNombreVia());
                data.setDireccionNumero(p.getMaeLocalVotacion().getDireccion().getNumero());
                data.setDireccionKilometro(p.getMaeLocalVotacion().getDireccion().getKilometro());
                data.setDireccionManzana(p.getMaeLocalVotacion().getDireccion().getManzana());
                data.setDireccionDepartamento(p.getMaeLocalVotacion().getDireccion().getDepartamento());
                data.setDireccionLote(p.getMaeLocalVotacion().getDireccion().getLote());
                data.setDireccionPiso(p.getMaeLocalVotacion().getDireccion().getPiso());
                data.setIdTipoZona(p.getMaeLocalVotacion().getDireccion().getIdTipoZona());
                data.setNombreZona(p.getMaeLocalVotacion().getDireccion().getNombreZona());
            }

            data.setFechaModificacion(p.getFechaModificacion());

            // data.setUbigeo(p.getMaeUbigeo().getUbigeo());
            //data.setNombreUbigeoNivel1(p.getMaeUbigeoNivel1().getNombre());
            //data.setNombreUbigeoNivel2(p.getMaeUbigeoNivel2().getNombre());
            //data.setNombreUbigeoNivel3(p.getMaeUbigeoNivel3().getNombre());
            //data.setNombreCentroPoblado( p.getMaeCentroPoblado() == null ? "":  p.getMaeCentroPoblado().getNombre());
            data.setIdRegistroSeccionA(p.getRegistroSeccionA() == null ? null : p.getRegistroSeccionA().getIdRegistroSeccionA());
            data.setIdRegistroSeccionB(p.getRegistroSeccionB() == null ? null : p.getRegistroSeccionB().getIdRegistroSeccionB());
            data.setIdRegistroSeccionC(p.getRegistroSeccionC() == null ? null : p.getRegistroSeccionC().getIdRegistroSeccionC());
            data.setIdRegistroSeccionD(p.getRegistroSeccionD() == null ? null : p.getRegistroSeccionD().getIdRegistroSeccionD());
            data.setIdRegistroSeccionE(p.getRegistroSeccionE() == null ? null : p.getRegistroSeccionE().getIdCabRegistroSeccionE());

            data.setIdEstadoRegistroSeccionA(p.getRegistroSeccionA() == null ? null : p.getRegistroSeccionA().getIdEstado());
            data.setIdEstadoRegistroSeccionB(p.getRegistroSeccionB() == null ? null : p.getRegistroSeccionB().getIdEstado());
            data.setIdEstadoRegistroSeccionC(p.getRegistroSeccionC() == null ? null : p.getRegistroSeccionC().getIdEstado());
            data.setIdEstadoRegistroSeccionD(p.getRegistroSeccionD() == null ? null : p.getRegistroSeccionD().getIdEstado());
            data.setIdEstadoRegistroSeccionE(p.getRegistroSeccionE() == null ? null : p.getRegistroSeccionE().getIdEstado());

            data.setEstadoRegistroSeccionA(p.getRegistroSeccionA() == null ? null : p.getRegistroSeccionA().getEstado());
            data.setEstadoRegistroSeccionB(p.getRegistroSeccionB() == null ? null : p.getRegistroSeccionB().getEstado());
            data.setEstadoRegistroSeccionC(p.getRegistroSeccionC() == null ? null : p.getRegistroSeccionC().getEstado());
            data.setEstadoRegistroSeccionD(p.getRegistroSeccionD() == null ? null : p.getRegistroSeccionD().getEstado());
            data.setEstadoRegistroSeccionE(p.getRegistroSeccionE() == null ? null : p.getRegistroSeccionE().getEstado());

            data.setTerminadodRegistroSeccionA(p.getRegistroSeccionA() == null ? null : p.getRegistroSeccionA().getTerminado());
            data.setTerminadodRegistroSeccionB(p.getRegistroSeccionB() == null ? null : p.getRegistroSeccionB().getTerminado());
            data.setTerminadodRegistroSeccionC(p.getRegistroSeccionC() == null ? null : p.getRegistroSeccionC().getTerminado());
            data.setTerminadodRegistroSeccionD(p.getRegistroSeccionD() == null ? null : p.getRegistroSeccionD().getTerminado());
            data.setTerminadodRegistroSeccionE(p.getRegistroSeccionE() == null ? null : p.getRegistroSeccionE().getTerminado());
            data.setIdEstadoLocalVotacion(p.getEstadoVerificacion());
            data.setEstadoDisponible(p.getEstadoDisponible());
            data.setNombreEstadoDisponible(p.getNombreEstadoDisponible());

            data.setTotalPaginas(p.getTotalPaginas());
            data.setTotalRegistros(p.getTotalRegistros());
            return data;
        }).collect(Collectors.toList());

        return destinoList;
    }

    public static final List<ListarValidarRegistroNuevoLVOutputDto> ListarValidarRegistroNuevoLVModelToDtoWrapper(List<VerificacionLV> origen) {
        if (origen == null) {
            return null;
        }
        List<ListarValidarRegistroNuevoLVOutputDto> destinoList = new ArrayList<>();
        destinoList = origen.stream().map(p -> {
            ListarValidarRegistroNuevoLVOutputDto data = new ListarValidarRegistroNuevoLVOutputDto();
            data.setIdVerificacionLV(p.getIdVerificacionLV());
            data.setIdLocalVotacion(p.getMaeLocalVotacion().getIdLocalVotacion());
            data.setNombreLocalVotacion(p.getMaeLocalVotacion().getNombreLocal());

            if (p.getMaeLocalVotacion().getDireccion() != null) {
                data.setIdTipoVia(p.getMaeLocalVotacion().getDireccion().getIdTipoVia());
                data.setNombreVia(p.getMaeLocalVotacion().getDireccion().getNombreVia());
                data.setDireccionNumero(p.getMaeLocalVotacion().getDireccion().getNumero());
                data.setDireccionKilometro(p.getMaeLocalVotacion().getDireccion().getKilometro());
                data.setDireccionManzana(p.getMaeLocalVotacion().getDireccion().getManzana());
                data.setDireccionDepartamento(p.getMaeLocalVotacion().getDireccion().getDepartamento());
                data.setDireccionLote(p.getMaeLocalVotacion().getDireccion().getLote());
                data.setDireccionPiso(p.getMaeLocalVotacion().getDireccion().getPiso());
                data.setIdTipoZona(p.getMaeLocalVotacion().getDireccion().getIdTipoZona());
                data.setNombreZona(p.getMaeLocalVotacion().getDireccion().getNombreZona());
                data.setFechaModificacion(p.getFechaModificacion());
            }

            /* data.setUbigeo(p.getMaeUbigeo().getUbigeo());
            data.setNombreUbigeoNivel1(p.getMaeUbigeoNivel1().getNombre());
            data.setNombreUbigeoNivel2(p.getMaeUbigeoNivel2().getNombre());
            data.setNombreUbigeoNivel3(p.getMaeUbigeoNivel3().getNombre());
            data.setNombreCentroPoblado( p.getMaeCentroPoblado() == null ? "":  p.getMaeCentroPoblado().getNombre());*/
            data.setIdRegistroSeccionA(p.getRegistroSeccionA() == null ? null : p.getRegistroSeccionA().getIdRegistroSeccionA());
            data.setIdRegistroSeccionB(p.getRegistroSeccionB() == null ? null : p.getRegistroSeccionB().getIdRegistroSeccionB());
            data.setIdRegistroSeccionC(p.getRegistroSeccionC() == null ? null : p.getRegistroSeccionC().getIdRegistroSeccionC());
            data.setIdRegistroSeccionD(p.getRegistroSeccionD() == null ? null : p.getRegistroSeccionD().getIdRegistroSeccionD());
            data.setIdRegistroSeccionE(p.getRegistroSeccionE() == null ? null : p.getRegistroSeccionE().getIdCabRegistroSeccionE());

            data.setIdEstadoRegistroSeccionA(p.getRegistroSeccionA() == null ? null : p.getRegistroSeccionA().getIdEstado());
            data.setIdEstadoRegistroSeccionB(p.getRegistroSeccionB() == null ? null : p.getRegistroSeccionB().getIdEstado());
            data.setIdEstadoRegistroSeccionC(p.getRegistroSeccionC() == null ? null : p.getRegistroSeccionC().getIdEstado());
            data.setIdEstadoRegistroSeccionD(p.getRegistroSeccionD() == null ? null : p.getRegistroSeccionD().getIdEstado());
            data.setIdEstadoRegistroSeccionE(p.getRegistroSeccionE() == null ? null : p.getRegistroSeccionE().getIdEstado());

            data.setTerminadodRegistroSeccionA(p.getRegistroSeccionA() == null ? null : p.getRegistroSeccionA().getTerminado());
            data.setTerminadodRegistroSeccionB(p.getRegistroSeccionB() == null ? null : p.getRegistroSeccionB().getTerminado());
            data.setTerminadodRegistroSeccionC(p.getRegistroSeccionC() == null ? null : p.getRegistroSeccionC().getTerminado());
            data.setTerminadodRegistroSeccionD(p.getRegistroSeccionD() == null ? null : p.getRegistroSeccionD().getTerminado());
            data.setTerminadodRegistroSeccionE(p.getRegistroSeccionE() == null ? null : p.getRegistroSeccionE().getTerminado());

            data.setIdEstadoLocalVotacion(p.getEstadoVerificacion());
            data.setEstadoDisponible(p.getEstadoDisponible());
            data.setNombreEstadoDisponible(p.getNombreEstadoDisponible());

            data.setTotalPaginas(p.getTotalPaginas());
            data.setTotalRegistros(p.getTotalRegistros());
            return data;
        }).collect(Collectors.toList());

        return destinoList;
    }

    public static final void ListarLocalVotacionVerificacionDtoToModelWrapper(ListarFiltroLocalVotacionVInputDto origen, VerificacionLV destino) {
        if (origen == null) {
            return;
        }
        
        destino.setRegistroSeccionA(new RegistroSeccionA());
        destino.getRegistroSeccionA().setNombreLocalLV(origen.getNombreLocal());

        destino.setMaeAmbitoElectoral(new MaeAmbitoElectoral());
        destino.getMaeAmbitoElectoral().setIdAmbitoElectoral(origen.getIdAmbitoElectoral());

        if(origen.getTipoProceso() == 1) {
        	 destino.setMaeProcesoElectoral(new MaeProcesoElectoral());
             destino.getMaeProcesoElectoral().setIdProcesoElectoral(origen.getIdProceso());

        } else {
        	 destino.setMaeProcesoVerificacion(new MaeProcesoVerificacion());
             destino.getMaeProcesoVerificacion().setIdProcesoVerificacion(origen.getIdProceso());

        }
       
        destino.setMaeLocalVotacion(new MaeLocalVotacion());
        destino.getMaeLocalVotacion().setNombreLocal(origen.getNombreLocal());
        destino.setEstadoVerificacion(origen.getEstadoVerificacion());
        destino.setEstadoDisponible(origen.getEstadoDisponible());

        destino.setMaeUbigeoNivel1(new MaeUbigeo());
        destino.getMaeUbigeoNivel1().setIdUbigeo(origen.getIdUbigeoDepartamento());
        destino.setMaeUbigeoNivel2(new MaeUbigeo());
        destino.getMaeUbigeoNivel2().setIdUbigeo(origen.getIdUbigeoProvincia());
        destino.setMaeUbigeoNivel3(new MaeUbigeo());
        destino.getMaeUbigeoNivel3().setIdUbigeo(origen.getIdUbigeoDistrito());
        destino.setPagina(origen.getPagina());
        destino.setTotalRegistroPorPagina(origen.getTotalRegistrosPorPagina());
    }

    public static final ObtenerListaFiltroLocalVotacionOutputDto ObtenerListaFiltroLocalVotacionVerificadoModelToDtoWrapper(List<VerificacionLV> origenes, VerificacionLV origen1) {
        if (origen1 == null || origenes == null) {
            return null;
        }

        ObtenerListaFiltroLocalVotacionOutputDto destino = new ObtenerListaFiltroLocalVotacionOutputDto();
        destino.setLocalesRegistrando(origen1.getVerificaciones().get(0).getLocalesRegistrando());
        destino.setLocalesPorValidar(origen1.getVerificaciones().get(0).getLocalesPorValidar());
        destino.setLocalesValidados(origen1.getVerificaciones().get(0).getLocalesValidados());
        destino.setLocalesObservados(origen1.getVerificaciones().get(0).getLocalesObsevados());
        destino.setTotalDeLocales(origen1.getVerificaciones().get(0).getTotalLocales());

        List<DatosLocalesVotacionOutputDto> lstDatos = new ArrayList<>();
        DatosLocalesVotacionOutputDto datos;
        for (VerificacionLV origen : origenes) {
        	datos = new DatosLocalesVotacionOutputDto();
//            datos.setProcesoVerificacion(origen.getMaeProcesoVerificacion().getIdProcesoVerificacion());
            datos.setUbigeo(origen.getMaeUbigeo().getIdUbigeo());
            if (origen.getMaeUbigeoNivel1() != null) {
                datos.setUbigeoNivel1(origen.getMaeUbigeoNivel1().getIdUbigeo());
                datos.setNombreUbigeoNivel1(origen.getMaeUbigeoNivel1().getNombre());
            }
            if (origen.getMaeUbigeoNivel2() != null) {
                datos.setUbigeoNivel2(origen.getMaeUbigeoNivel2().getIdUbigeo());
                datos.setNombreUbigeoNivel2(origen.getMaeUbigeoNivel2().getNombre());
            }
            if (origen.getMaeUbigeoNivel3() != null) {
                datos.setUbigeoNivel3(origen.getMaeUbigeoNivel3().getIdUbigeo());
                datos.setNombreUbigeoNivel3(origen.getMaeUbigeoNivel3().getNombre());
            }
            if (origen.getMaeCentroPoblado() != null) {
                datos.setCentroPobladoN(origen.getMaeCentroPoblado().getId());
                datos.setCentroPobladoC(origen.getMaeCentroPoblado().getNombre());
            }
            if (origen.getMaeLocalVotacion() != null) {
                datos.setNombreLV(origen.getMaeLocalVotacion().getNombreLocal());
                datos.setNumeroLV(origen.getMaeLocalVotacion().getNumeroLocal());
                if (origen.getMaeLocalVotacion().getDireccion() != null) {
                    datos.setIdTipoVia(origen.getMaeLocalVotacion().getDireccion().getIdTipoVia());
                    datos.setNombretipoVia(origen.getMaeLocalVotacion().getDireccion().getNombreTipoVia());
                    datos.setNombreVia(origen.getMaeLocalVotacion().getDireccion().getNombreVia());
                    datos.setDireccionNumero(origen.getMaeLocalVotacion().getDireccion().getNumero());
                    datos.setDireccionKilometro(origen.getMaeLocalVotacion().getDireccion().getKilometro());
                    datos.setDireccionManzana(origen.getMaeLocalVotacion().getDireccion().getManzana());
                    datos.setDireccionDepartamento(origen.getMaeLocalVotacion().getDireccion().getDepartamento());
                    datos.setDireccionLote(origen.getMaeLocalVotacion().getDireccion().getLote());
                    datos.setDireccionPiso(origen.getMaeLocalVotacion().getDireccion().getPiso());
                    datos.setIdTipoZona(origen.getMaeLocalVotacion().getDireccion().getIdTipoZona());
                    datos.setNombreTipoZona(origen.getMaeLocalVotacion().getDireccion().getNombreTipoZona());
                    datos.setNombreZona(origen.getMaeLocalVotacion().getDireccion().getNombreZona());
                }
            }
            if(origen.getRegistroSeccionA() != null && origen.getRegistroSeccionA().getMaeLocalVotacion() != null
            && origen.getRegistroSeccionA().getMaeLocalVotacion().getDireccion() != null ){
                Direccion tmp = origen.getRegistroSeccionA().getMaeLocalVotacion().getDireccion();
                datos.setIdTipoVia(tmp.getIdTipoVia());
                datos.setNombretipoVia(tmp.getNombreTipoVia());
                datos.setNombreVia(tmp.getNombreVia());
                datos.setDireccionNumero(tmp.getNumero());
                datos.setDireccionKilometro(tmp.getKilometro());
                datos.setDireccionManzana(tmp.getManzana());
                datos.setDireccionDepartamento(tmp.getDepartamento());
                datos.setDireccionLote(tmp.getLote());
                datos.setDireccionPiso(tmp.getPiso());
                datos.setIdTipoZona(tmp.getIdTipoZona());
                datos.setNombreTipoZona(tmp.getNombreTipoZona());
                datos.setNombreZona(tmp.getNombreZona());
            }
            datos.setTipoLV(origen.getRegistroSeccionA().getIdTipoLocalLV());
            datos.setNombreTipoLV(origen.getRegistroSeccionA().getNombreTipoLocalLV());
            datos.setEstadoLVN(origen.getEstadoDisponible());
            datos.setEstadoLVC(origen.getNombreEstadoDisponible());
            datos.setEstadoVerificacion(origen.getEstadoVerificacion());
            datos.setEstadoVerificacionLV(origen.getNombreEstadoLV());
            datos.setFechaCreacion(origen.getFechaCreacion());
            datos.setFechaModificacion(origen.getFechaModificacion());

            lstDatos.add(datos);
        }
        destino.setDatosLocalesVotacionOutputDto(lstDatos);

        return destino;
    }

}
