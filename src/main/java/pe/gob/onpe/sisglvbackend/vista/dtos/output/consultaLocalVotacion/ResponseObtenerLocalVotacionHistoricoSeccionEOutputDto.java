package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseObtenerLocalVotacionHistoricoSeccionEOutputDto  extends BaseOutputDto{
    
    private ObtenerLocalVotacionHistoricoSeccionEOutputDto datos;
    
    
}
