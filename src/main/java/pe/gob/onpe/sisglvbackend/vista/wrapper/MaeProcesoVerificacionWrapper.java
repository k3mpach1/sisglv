package pe.gob.onpe.sisglvbackend.vista.wrapper;

import java.util.ArrayList;
import java.util.List;


import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarProcesoVerificacionActivosOuputDto;


public class MaeProcesoVerificacionWrapper {
	public static final List<ListarProcesoVerificacionActivosOuputDto> listarActivosModelToDtoWrapper(List<MaeProcesoVerificacion> origen) {
		if(origen==null) {
			return null;
		}
		List<ListarProcesoVerificacionActivosOuputDto> resultList = new ArrayList<>();
		ListarProcesoVerificacionActivosOuputDto dtoOut = null;
		for (MaeProcesoVerificacion out : origen) {
			dtoOut = new ListarProcesoVerificacionActivosOuputDto();
			dtoOut.setIdProceso(out.getIdProcesoVerificacion());
			dtoOut.setNombreProceso(out.getNombre());
			dtoOut.setAcronimo(out.getAcronimo());
			resultList.add(dtoOut);
		}
		return resultList;
	}
}
