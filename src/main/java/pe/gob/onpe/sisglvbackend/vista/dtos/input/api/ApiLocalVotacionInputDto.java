package pe.gob.onpe.sisglvbackend.vista.dtos.input.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Getter
@Setter
public class ApiLocalVotacionInputDto {

    @ApiModelProperty(notes = "Acrónimo del Proceso Electoral a consultar.", example = "EG2021", required = true)
    String acronimo;
}
