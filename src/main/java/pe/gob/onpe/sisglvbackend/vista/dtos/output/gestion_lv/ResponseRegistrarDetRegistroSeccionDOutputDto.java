package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;
import lombok.Getter;

@Getter
@Setter
public class ResponseRegistrarDetRegistroSeccionDOutputDto extends BaseOutputDto {
	
	private DatosRegistrarDetRegistroSeccionDOutputDto datos;

}
