package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrarProcesoElectoralInputDto {

	@NotBlank(message = "El nombre no puede ser nulo ni estar en blanco")
	@NotEmpty(message = "El nombre no puede ser nulo ni estar en blanco")
	@NotNull(message = "El nombre no puede ser nulo")
	private String nombre;
	
	@NotBlank(message = "El acronimo no puede ser nulo ni estar en blanco")
	@NotEmpty(message = "El acronimo no puede ser nulo ni estar en blanco")
	@NotNull(message = "El acronimo no puede ser nulo")
	private String acronimo;
	
	@NotNull(message = "Indicar si es un proceso base, no puede ser nulo")
	private Integer procesoBase;
	
	private String idProcesoBase;
	
}

