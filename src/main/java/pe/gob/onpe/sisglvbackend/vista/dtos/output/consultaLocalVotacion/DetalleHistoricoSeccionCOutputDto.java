package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetalleHistoricoSeccionCOutputDto {

    private Integer idDetFichaSeccionC;

    private String aula;
    private String pabellon;
    private Integer piso;
    private Integer requiereToldo;
    private Integer usoAula;
    private Integer cantidadMesa;

    private Integer tipoAula;
    private String descripcionTipoAula;

    private Integer tieneLuminaria;
    private Integer cantidadLuminariaBuenEstado;
    private Integer cantidadLuminariaMalEstado;

    private Integer tieneTomaCorriente;
    private Integer cantidadTomacorrienteBuenEstado;
    private Integer cantidadTomacorrienteMalEstado;

    private Integer tieneInterruptor;
    private Integer cantidadInterruptorBuenEstado;
    private Integer cantidadInterruptorMalEstado;

    private Integer tienePuerta;
    private Integer cantidadPuertaBuenEstado;
    private Integer cantidadPuertaMalEstado;

    private Integer tieneVentana;
    private Integer cantidadVentanaBuenEstado;
    private Integer cantidadVentanaMalEstado;

    private Integer tienePuntoInternet;
    private Integer tieneAccesoWifi;

}
