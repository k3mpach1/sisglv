package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocalVotacionOutputDto {
    private String odpe;
    private Long   item;
    private String orc;
    private String departamento;
    private String provincia;
    private String distrito;
    private String localVotacion;
}
