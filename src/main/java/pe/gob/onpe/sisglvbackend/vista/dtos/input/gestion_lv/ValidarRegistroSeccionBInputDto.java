package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidarRegistroSeccionBInputDto {

	@NotNull(message = "ID del registro de la seccion B")
	private Integer idRegistroSeccionB;
	private Integer idVerificacionLv;
	private String observacionRegistro;
	

	
}
