package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto {
	private String documentoIdentidadRepresentante;
	private String nombreCompletoRepresentante;
	private String cargoRepresentante;
	private String telefonoRepresentante;
	private String correoRepresentante;
	private String documentoIdentidadRepresentanteApafa;
	private String nombreCompletoRepresentanteApafa;
	private String telefonoRepresentanteApafa;
	private Integer cantidadAula;
	private Integer cantidadMesa;
	private String codigoMinedu;
	private Integer idTipoTecnologia;
	private String observacionRegistro;
}
