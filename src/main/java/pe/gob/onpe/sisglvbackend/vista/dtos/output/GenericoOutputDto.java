package pe.gob.onpe.sisglvbackend.vista.dtos.output;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenericoOutputDto {
	private Integer resultado;
	private String mensaje;
	private Object datos;
}
