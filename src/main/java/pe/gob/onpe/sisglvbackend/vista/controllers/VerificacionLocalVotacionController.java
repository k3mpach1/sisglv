package pe.gob.onpe.sisglvbackend.vista.controllers;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionCServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.VerificacionLVServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ListarEditarRegistroNuevoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.verificacionLocalVotacion.ListarVerificacionLocalVotacionInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosListarValidarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarValidarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarValidarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.verificacionLocalVotacion.DatosListarVerificacionLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.verificacionLocalVotacion.ListarVerificacionLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.verificacionLocalVotacion.ResponseListarVerificacionLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.wrapper.VerificacionLVWrapper;

import javax.validation.Valid;
import java.util.List;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/verificacion-local-votacion")
@Validated
public class VerificacionLocalVotacionController {
	
	@Autowired
	VerificacionLVServicio verificacionLVServicio;
	
	@Autowired
	RegistroSeccionCServicio registroSeccionCServicio;

	
	@ApiOperation(value = "listar verificaciones de locales de votación", notes = "Permite listar las verificaciones de los locales de votación")
	@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
	/*
	 * @ApiResponses({
	 * 
	 * @ApiResponse(code = 200, message = "ok", response =
	 * ResponseListarVerificacionLocalVotacionOutputDto.class),
	 * 
	 * @ApiResponse(code = 401, message = "Unauthorized", response =
	 * ExceptionResponse.class),
	 * 
	 * @ApiResponse(code = 403, message = "Forbidden", response =
	 * ExceptionResponse.class),
	 * 
	 * @ApiResponse(code = 404, message = "Not found", response =
	 * ExceptionResponse.class),
	 * 
	 * @ApiResponse(code = 500, message = "Internal server error", response =
	 * ExceptionResponse.class) })
	 */
	@PostMapping(value = "/listar-verificacion-local-votacion")
	public ResponseEntity<ResponseListarVerificacionLocalVotacionOutputDto> listarVerificacionLocalVotacion(
			@RequestBody @Valid final ListarVerificacionLocalVotacionInputDto paramDto) throws Exception {

		VerificacionLV param = new VerificacionLV();
		VerificacionLVWrapper.ListarVerificacionLocalVotacionDtoToModelWrapper(paramDto, param);
		List<VerificacionLV> paramOut = verificacionLVServicio.listarVerificacion(param);
		List<ListarVerificacionLocalVotacionOutputDto> paramDtoOut = VerificacionLVWrapper
				.ListarVerificacionLocalVotacionModelToDtoWrapper(paramOut);

		DatosListarVerificacionLocalVotacionOutputDto objDto = new DatosListarVerificacionLocalVotacionOutputDto();
		objDto.setVerificacionesLocalVotacion(paramDtoOut);

		ResponseListarVerificacionLocalVotacionOutputDto outDto = new ResponseListarVerificacionLocalVotacionOutputDto();
		outDto.setResultado(param.getResultado());
		outDto.setMensaje(param.getMensaje());
		outDto.setDatos(objDto);

		return new ResponseEntity<>(outDto, HttpStatus.OK);
	}

	@ApiOperation(value = "listar registros de locales nuevos para validar", notes = "listar registros de locales nuevos para validar")
	@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
	@PostMapping("/listar-validar-verficacion-lv")
	public ResponseEntity<?> listarValidarVerificacionLV(
			@RequestBody @Valid ListarEditarRegistroNuevoLVInputDto paramInputDto) throws Exception {

		VerificacionLV param = new VerificacionLV();
		VerificacionLVWrapper.ListarEditarRegistroNuevoLVDtoToModelWrapper(paramInputDto, param);

		verificacionLVServicio.listarValidarLocalVotacionHistorico(param);

		List<ListarValidarRegistroNuevoLVOutputDto> outputDto = VerificacionLVWrapper
				.ListarValidarRegistroNuevoLVModelToDtoWrapper(param.getVerificaciones());

		DatosListarValidarRegistroNuevoLVOutputDto datos = new DatosListarValidarRegistroNuevoLVOutputDto();
		datos.setLocales(outputDto);
		datos.setTotalRegistros(
				param.getVerificaciones().size() == 0 ? 0 : param.getVerificaciones().get(0).getTotalRegistros());

		ResponseListarValidarRegistroNuevoLVOutputDto response = new ResponseListarValidarRegistroNuevoLVOutputDto();
		response.setDatos(datos);
		response.setMensaje(param.getMensaje());
		response.setResultado(param.getResultado());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
}
