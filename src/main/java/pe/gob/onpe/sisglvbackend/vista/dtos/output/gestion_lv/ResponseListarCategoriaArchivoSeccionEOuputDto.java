package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
@Schema(name = "ResponseListarCategoriaArchivoSeccionEOuputDto", description = "The entity that represents Episode")
public class ResponseListarCategoriaArchivoSeccionEOuputDto extends BaseOutputDto{
	private DatosListarCategoriaArchivoSeccionEOuputDto datos;	
}
