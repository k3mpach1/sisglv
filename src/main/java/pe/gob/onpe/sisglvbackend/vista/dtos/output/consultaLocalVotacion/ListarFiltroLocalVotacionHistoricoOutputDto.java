package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class ListarFiltroLocalVotacionHistoricoOutputDto {
    private Integer idFichaSeccionA;
    private Integer idFichaSeccionB;
    private Integer idFichaSeccionC;
    private Integer idFichaSeccionD;
    private Integer idFichaSeccionE;
    private String nombreUbigeoNivel1;
    private String nombreUbigeoNivel2;
    private String nombreUbigeoNivel3;
    private String nombreCentroPoblado;
    private String nombreLocalVotacion;
    private Integer idTipoVia;
    private String nombreVia;
    private Integer direccionNumero;
    private Float direccionKilometro;
    private String direccionManzana;
    private String direccionInterior;
    private String direccionDepartamento;
    private String direccionLote;
    private Integer direccionPiso;
    private Integer direccionIdTipoZona;
    private String direccionNombreZona;
    private String idEstado;
    private Date fechaModificacion;
}
