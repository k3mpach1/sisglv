package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.Getter;
import lombok.Setter;

/**
 * @author glennlq
 * @created 1/6/22
 */
@Getter
@Setter
public class ReporteSeccionEOutputDto {
	private String codigoHistoricoLV;
	private String nombreLv;
	private String categoria;
	private String observacionLv;
//	private Integer archivo;

//	private String detalle;
}
