package pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProyeccionParte1OutputDto {
    Integer idProcesoElectoral;
    Integer idPadronProyeccion;
    Integer idPadronReferencia;
}
