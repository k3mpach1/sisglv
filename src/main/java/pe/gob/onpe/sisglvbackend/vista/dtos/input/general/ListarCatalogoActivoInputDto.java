package pe.gob.onpe.sisglvbackend.vista.dtos.input.general;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListarCatalogoActivoInputDto {
	
	@NotNull(message = "Codigo es campo requerido")
	private Integer codigo;
	
}
