package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosArchivoLvSubidoOutputDto {

	private Integer idArchivo;
	private String  guid;
	private String 	filename;
	private String  formato;
	private String  filenameOriginal;
	private String  peso;
	private Integer idCabRegistroSeccionE;
	
}
