package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EstadoConfigPEOutputDto {
    private Integer idProcesoElectoral;
    private Integer estadoConfig;
}
