package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.*;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

/**
 * @author glennlq
 * @created 1/7/22
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidarClaveReporteOutputDto extends BaseOutputDto {
    boolean claveValidado;
    String mensajeValidacion;
}
