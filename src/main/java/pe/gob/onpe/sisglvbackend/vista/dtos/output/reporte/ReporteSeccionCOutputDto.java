package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.Getter;
import lombok.Setter;

/**
 * @author glennlq
 * @created 1/6/22
 */
@Getter
@Setter
public class ReporteSeccionCOutputDto {
	private String codigoHistoricoLV;
	private String nombreLv;
	private Integer cantidadMesa;
	private Integer cantidadAula;
	private String tipoAula;
	private String requiereToldo;
	private String aula;
	private String pabellon;
	private Integer piso;
	private Integer cantidadMesadaDetalle;
	private String tieneLuminaria;
	private Integer cantidadLuminariaBe;
	private Integer cantidadLuminariaMe;
	private String tieneTomaCorriente;
	private Integer cantidadTomaCorrienteBe;
	private Integer cantidadTomaCorrienteMe;
	private String tieneInterruptor;
	private Integer cantidadInterruptorBe;
	private Integer cantidadInterruptorMe;
	private String tienePuerta;
	private Integer cantiadPuertaBe;
	private Integer cantidadPuertaMe;
	private String tieneVentana;
	private Integer cantidadaVentanaBe;
	private Integer cantidadVentanaMe;
	private String tienePuntoInternet;
	private String tieneWifi;
	private String usoSeleccion;
}
