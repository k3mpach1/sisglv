package pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosDescargaPlantillaPeriodoTrimestralOutputDto {

	private String extension;
	private String nombrePlantilla;
	private String fileBase64;
	
}
