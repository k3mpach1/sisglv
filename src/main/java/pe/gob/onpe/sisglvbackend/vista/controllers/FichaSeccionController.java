package pe.gob.onpe.sisglvbackend.vista.controllers;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.FichaSeccionSevicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/ficha-seccion")
@Validated
public class FichaSeccionController {
    
    @Autowired
    FichaSeccionSevicio fichaSeccionSevicio;
    
    @ApiOperation(value = "registro de datos generales local votación", notes = "Permite registrar los datos generales del local de votación")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = FichaSeccionC.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
        @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
        @ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)})
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/lista-ficha-seccion-c/{idLocalVotacion}")
    public ResponseEntity<?> listarFichaSeccionC(@PathVariable(required = true) Integer idLocalVotacion) throws Exception {
        
        MaeLocalVotacion maelocal = new MaeLocalVotacion();
        maelocal.setIdLocalVotacion(idLocalVotacion);
        
        FichaSeccionC param = new FichaSeccionC();
        param.setMaeLocalVotacion(maelocal);
        
        fichaSeccionSevicio.listarFichaSeccionC(param);
        
        List<FichaSeccionC> outputDto = Funciones.mapAll(param.getLista(),
				FichaSeccionC.class);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());
        resultado.put("lista", outputDto);

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }
    
    @ApiOperation(value = "registro de datos generales local votación", notes = "Permite registrar los datos generales del local de votación")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = FichaSeccionD.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
        @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
        @ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)})
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/lista-ficha-seccion-d/{idLocalVotacion}")
    public ResponseEntity<?> listarFichaSeccionD(@PathVariable(required = true) Integer idLocalVotacion) throws Exception {
        
        MaeLocalVotacion maelocal = new MaeLocalVotacion();
        maelocal.setIdLocalVotacion(idLocalVotacion);
        
        FichaSeccionD param = new FichaSeccionD();
        param.setMaeLocalVotacion(maelocal);
        
        fichaSeccionSevicio.listarFichaSeccionD(param);
        
        List<FichaSeccionC> outputDto = Funciones.mapAll(param.getLista(),
				FichaSeccionC.class);

        HashMap<String, Object> resultado = new HashMap<String, Object>();
        resultado.put("resultado", param.getResultado());
        resultado.put("mensaje", param.getMensaje());
        resultado.put("lista", outputDto);

        return new ResponseEntity<>(resultado, HttpStatus.OK);
    }

}
