package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteComun;

@Getter
@Setter
public class RegistrarDatosGeneralesLocalVotacionInputDto {
	
	
	@NotBlank(message = "Documento Identidad es obligatorio")
	private String documentoIdentidad;
	@NotBlank(message = "Nombre Completo es obligatorio")
	private String nombreCompleto;
	@NotNull(message = "Dependencia es obligatorio")
	private Integer idDependencia;
	@NotBlank(message = "Departamento es obligatorio")
	private String codigoDepartamento;
	private String nombreDepartamento;
	@NotBlank(message = "Provincia es obligatorio")
	private String codigoProvincia;
	private String nombreProvincia;
	@NotBlank(message = "Distrito es obligatorio")
	private String codigoDistrito;

	private String nombreDistrito;
	@NotBlank(message = "Centro Poblado es obligatorio")
	private String codigoCentroPoblado;
	
	@NotBlank(message = "Latitud del Distrito es obligatorio")
	private String latitudDistrito;
	@NotBlank(message = "Longitud del Distrito es obligatorio")
	private String longitudDistrito;
	
	@NotNull(message = "Tipo de Local es obligatorio")
	private Integer idTipoLocal;
	
	@NotBlank(message = "Nombre de Local es obligatorio")
	private String nombreLocal;
	@NotBlank(message = "Número de Local es obligatorio")
	private String numeroLocal;
	@NotBlank(message = "Código de Local es obligatorio")
	private String codigoLocal;
	
	@NotNull(message = "Tipo de Vía es obligatorio")
	private Integer idTipoVia;
	private String nombreVia;
	private String numeroVia;
	private String kilometro;
	
	
	private String manzana;
	private String interior;
	private String numeroDepartamento;
	private String lote;
	private String piso;
	private Integer idTipoZona;
	private String nombreZona;
	private String referencia;
	private String latitudPuertaAcceso;
	private String longitudPuertaAcceso;
	private String distanciaAproximadaCentroDistrito;
	private String tiempoAproximadaCentroDistrito;
	
	@NotNull(message = "Tipo de Institución es obligatorio")
	private Integer idTipoInstituacion;
	
	@NotNull(message = "Categoría de la Institución es obligatorio")
	private Integer idCategoriaInstituacion;
	private String otraCategoria;
	
	@NotNull(message = "Nivel es obligatorio")
	private Integer idTipoNivel;
	
	
	private String documentoIdentidadRepresentanteLocalVotacion;
	private String nombreCompletoRepresentanteLocalVotacion;
	private String cargoRepresentanteLocalVotacion;
	private String telefonoRepresentanteLocalVotacion;
	
	@Email(message = ConstanteComun.MENSAJE_CORREO_FORMATO_INCORRECTO)
	private String correoRepresentanteLocalVotacion;
	
	private String documentoIdentidadRepresentanteApafa;
	private String nombreCompletoRepresentanteApafa;
	private String telefonoRepresentanteApafa;
	
	private String cantidadAulasDisponibles;
	private String cantidadMesasDisponibles;
	
	private String codigoLocalSegunMinedu;
	
	private String idTipoTecnologia;
}
