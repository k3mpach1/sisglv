package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * @author glennlq
 * @created 12/14/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstadoTerminadoNuevoLocalInputDto {

    @NotNull(message = "idValidacionLv es un campo requerido")
    private Integer idValidacionLv;

}
