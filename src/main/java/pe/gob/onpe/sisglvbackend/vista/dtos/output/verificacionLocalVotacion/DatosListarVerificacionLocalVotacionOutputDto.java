package pe.gob.onpe.sisglvbackend.vista.dtos.output.verificacionLocalVotacion;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DatosListarVerificacionLocalVotacionOutputDto {
    private List<ListarVerificacionLocalVotacionOutputDto> verificacionesLocalVotacion;
}
