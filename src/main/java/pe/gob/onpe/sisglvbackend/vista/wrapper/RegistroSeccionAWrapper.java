package pe.gob.onpe.sisglvbackend.vista.wrapper;

import java.util.ArrayList;
import java.util.List;

import pe.gob.onpe.sisglvbackend.negocio.modelos.Direccion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeCentroPoblado;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionA;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ActualizarDatosGeneralesPrimerPasoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarDatosGeneralesCuartoPasoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarDatosGeneralesLocalVotacionInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarDatosGeneralesPrimerPasoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarDatosGeneralesSegundoPasoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarDatosGeneralesTercerPasoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCentroPobladoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarUbigeoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.LocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto;

public  class RegistroSeccionAWrapper {
	public static final void RegistroDatosGeneralesDtoToModelWrapper(RegistrarDatosGeneralesLocalVotacionInputDto origen,RegistroSeccionA destino) {
		if(origen==null) {
			return;
		}
		destino.setDniRegistrador(origen.getDocumentoIdentidad());
		destino.setNombreApellidoRegistrador(origen.getNombreCompleto());
		//destino.setIdUbigeo(origen.getIdUb());
		
		//destino.setCentroPoblado(origen.getCodigoCentroPoblado()); //=>considerar cambios se va a registrar el código y el nombre del centro poblado
		
		destino.setLatitudDistrito(origen.getLatitudDistrito());
		destino.setLongitudDistrito(origen.getLongitudDistrito());
		
		//destino.setIdTipoInstitucion(origen.getIdTipoLocal());
		
		//destino.setIdCategoriaInstitucion(origen.getIdCategoriaInstituacion());
		//Preguntar por el campo otra categoría
		//Preguntar por el campo nivel
		//Preguntar por el campo tipo via
		//Preguntar por el campo nombre via
		//Preguntar por el campo numero via
		//Preguntar por el campo kilometro
		//Preguntar por el campo mazana
		//Preguntar por el campo interior
		//Preguntar por el campo departamento
		//Preguntar por el campo lote
		//Preguntar por el campo piso
		
		//Preguntar por el campo tipo zona
		//Preguntar por el campo nombre zona
		//destino.setReferencia(origen.getReferencia());
		//Preguntar por el campo latitud puerta de acceso
		//Preguntar por el campo longitud puerta de acceso
		//Preguntar por el campo distancia al centro del distrito
		//Preguntar por el campo tiempo al centro del distrito
		
		
		destino.setDniRepresentanteApafa(origen.getDocumentoIdentidad());
		destino.setNombreApeRepresentanteLV(origen.getNombreCompleto());
		
	}
	public static final void RegistroDatosGeneralesPrimerPasoDtoToModelWrapper(RegistrarDatosGeneralesPrimerPasoLVInputDto origen,RegistroSeccionA destino) {
		if(origen==null) {
			return;
		}


		//destino.setIdTipoAmbitoGeografico(origen.getIdAmbitoElectoral());

		destino.setUbigeo(MaeUbigeo.builder().idUbigeo(origen.getIdUbigeo()).build());
		
		destino.setVerificacionLV(new VerificacionLV());
		destino.getVerificacionLV().setIdTipoProceso(origen.getIdTipoProceso());
		destino.getVerificacionLV().setMaeProcesoElectoral(new MaeProcesoElectoral());
		destino.getVerificacionLV().getMaeProcesoElectoral().setIdProcesoElectoral(origen.getIdProcesoElectoral());
		destino.getVerificacionLV().setMaeProcesoVerificacion(new MaeProcesoVerificacion());
		destino.getVerificacionLV().getMaeProcesoVerificacion().setIdProcesoVerificacion(origen.getIdProcesoVerificacion());
		destino.setIdAmbitoElectoral(origen.getIdAmbitoElectoral());
		destino.setIdTipoAmbitoElectoral(origen.getIdTipoAmbitoElectoral());
		destino.setIdTipoLocalLV(origen.getIdTipoLocal());
		destino.setNombreLocalLV(origen.getNombreLocal());
		destino.setNumeroLocaLV(origen.getNumeroLocal());
		
		if(origen.getIdCentroPoblado() != null) {
			destino.setCentroPoblado(MaeCentroPoblado.builder().id(origen.getIdCentroPoblado()).build());
		}
		
		
	}
	public static final void ActualizarDatosGeneralesPrimerPasoDtoToModelWrapper(ActualizarDatosGeneralesPrimerPasoLVInputDto origen,RegistroSeccionA destino) {
		if(origen==null) {
			return;
		}


		destino.setVerificacionLV(new VerificacionLV());
		destino.getVerificacionLV().setIdVerificacionLV(origen.getIdVerificacion());
		
		destino.setIdRegistroSeccionA(origen.getIdRegistroSeccionA());
		destino.setUbigeo(MaeUbigeo.builder().idUbigeo(origen.getIdUbigeo()).build());
		
		if(origen.getIdCentroPoblado() != null) {
			destino.setCentroPoblado(MaeCentroPoblado.builder().id(origen.getIdCentroPoblado()).build());
		}
		
		destino.setIdTipoLocalLV(origen.getIdTipoLocal());
		destino.setNombreLocalLV(origen.getNombreLocal());
		destino.setNumeroLocaLV(origen.getNumeroLocal());

	
	}
	
	public static final void RegistroDatosGeneralesSegundoPasoDtoToModelWrapper(RegistrarDatosGeneralesSegundoPasoLVInputDto origen,RegistroSeccionA destino) {
		if(origen==null) {
			return;
		}
		destino.setIdRegistroSeccionA(origen.getIdRegistroSeccionA());
		destino.setVerificacionLV(new VerificacionLV());
		destino.getVerificacionLV().setIdVerificacionLV(origen.getIdVerificacion());
		destino.setDniRegistrador(origen.getDniRegistrador());
		destino.setNombreApellidoRegistrador(origen.getNombreApellidoRegistrador());
		
		destino.setIdUnidadOrganica(origen.getIdDependencia());
		destino.setNombreUnidadOrganica(origen.getNombreDependencia());
		
		destino.setIdTipoAmbitoElectoral(origen.getIdTipoAmbitoElectoral());
		destino.setIdAmbitoElectoral(origen.getIdAmbitoElectoral());
		

		if(origen.getIdCentroPoblado() != null) {
			destino.setCentroPoblado(MaeCentroPoblado.builder().id(origen.getIdCentroPoblado()).build());
		}
		
		
		
		
		destino.setLatitudDistrito(origen.getLatitudDistrito());
		destino.setLongitudDistrito(origen.getLongitudDistrito());
		
	}
	public static final void RegistroDatosGeneralesTercerPasoDtoToModelWrapper(RegistrarDatosGeneralesTercerPasoLVInputDto origen,RegistroSeccionA destino) {
		if(origen==null) {
			return;
		}
		destino.setIdRegistroSeccionA(origen.getIdRegistroSeccionA());
			
		
		destino.setDireccion(new Direccion());
		destino.getDireccion().setIdTipoVia(origen.getIdTipoVia());
		destino.getDireccion().setNombreVia(origen.getNombreVia());
			   
		destino.getDireccion().setNumero(origen.getNumero());
		destino.getDireccion().setKilometro(origen.getKilometro());
		destino.getDireccion().setInterior(origen.getInterior());
		destino.getDireccion().setManzana(origen.getManzana());
		destino.getDireccion().setDepartamento(origen.getDepartamento());
		destino.getDireccion().setLote(origen.getLote());
		destino.getDireccion().setPiso(origen.getPiso());
			   
		destino.getDireccion().setIdTipoZona(origen.getIdTipoZona());
		destino.getDireccion().setNombreZona(origen.getNombreZona());
		destino.getDireccion().setReferencia(origen.getReferencia());
		
		
		destino.setLatitudPuertaAcceso(origen.getLatitudPuertaAcceso());
		destino.setLongitudPuertaAcceso(origen.getLongitudPuertaAcceso());
		destino.setDistanciaCentroDistrito(origen.getDistanciaCentroDistrito());
		destino.setTiempoCentroDistrito(origen.getTiempoCentroDistrito());
		
		destino.setIdTipoInstitucion(origen.getIdTipoInstitucion());
		destino.setIdCategoriaInstitucion(origen.getIdCategoriaInstitucion());
		destino.setOtraCategoria(origen.getOtraCategoria());
		destino.setIdNivelInstruccion(origen.getIdNivel());
		
	
		
	}
	public static final void RegistroDatosGeneralesCuartoPasoDtoToModelWrapper(RegistrarDatosGeneralesCuartoPasoLVInputDto origen,RegistroSeccionA destino) {
		if(origen==null) {
			return;
		}
		destino.setIdRegistroSeccionA(origen.getIdRegistroSeccionA());
		destino.setVerificacionLV(new VerificacionLV());
		destino.getVerificacionLV().setIdVerificacionLV(origen.getIdVerificacion());
		destino.setDniRepresentanteLV(origen.getDocumentoIdentidadRepresentante());
		destino.setNombreApeRepresentanteLV(origen.getNombreCompletoRepresentante());
		destino.setCargoRepresentanteLV(origen.getCargoRepresentante());
		destino.setTelefonoRepresentateLV(origen.getTelefonoRepresentante());
		destino.setCorreoRepresentateLV(origen.getCorreoRepresentante());
		
		destino.setDniRepresentanteApafa(origen.getDocumentoIdentidadRepresentanteApafa());
		destino.setNombreApeRepresentanteApafa(origen.getNombreCompletoRepresentanteApafa());
		
		destino.setTelefonoRepresentanteApafa(origen.getTelefonoRepresentanteApafa());
		
		destino.setCantidadAula(origen.getCantidadAula());
		destino.setCantidadMesa(origen.getCantidadMesa());
		destino.setCodigoMinedu(origen.getCodigoMinedu());
		destino.setIdTipoTecnologia(origen.getIdTipoTecnologia());
		
	}
	public static final ObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto ObtenerDatosGeneralesPrimerPasoModelToDtoWrapper(RegistroSeccionA origen) {
		if(origen==null) {			
			return null;
		}
		ObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto destino = new ObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto();
		
		ListarUbigeoBasicoOutputDto departamento = new ListarUbigeoBasicoOutputDto();
		departamento.setIdUbigeo(origen.getUbigeoDepartamento().getIdUbigeo());
		departamento.setUbigeo(origen.getUbigeoDepartamento().getUbigeo());
		departamento.setNombre(origen.getUbigeoDepartamento().getNombre());
		departamento.setActivo("1");
		destino.setDepartamento(departamento);
		
		ListarUbigeoBasicoOutputDto provincia = new ListarUbigeoBasicoOutputDto();
		provincia.setIdUbigeo(origen.getUbigeoProvincia().getIdUbigeo());
		provincia.setUbigeo(origen.getUbigeoProvincia().getUbigeo());
		provincia.setNombre(origen.getUbigeoProvincia().getNombre());
		provincia.setActivo("1");
		destino.setProvincia(provincia);
		
		ListarUbigeoBasicoOutputDto distrito = new ListarUbigeoBasicoOutputDto();
		distrito.setIdUbigeo(origen.getUbigeo().getIdUbigeo());
		distrito.setUbigeo(origen.getUbigeo().getUbigeo());
		distrito.setNombre(origen.getUbigeo().getNombre());
		distrito.setActivo("1");
		destino.setDistrito(distrito);
	
		if(origen.getCentroPoblado() != null) {
			ListarCentroPobladoBasicoOutputDto centroPoblado = new ListarCentroPobladoBasicoOutputDto();
			centroPoblado.setIdUbigeo(origen.getCentroPoblado().getId());
			centroPoblado.setNombre(origen.getCentroPoblado().getNombre());
			destino.setCentroPoblado(centroPoblado);
		}
		
		
		destino.setIdTipoLocal(origen.getIdTipoLocalLV());		
		destino.setNombreLocal(origen.getNombreLocalLV());
		destino.setNumeroLocal(origen.getNumeroLocaLV());
		destino.setIdTipoAmbitoElectoral(origen.getIdTipoAmbitoElectoral());
		destino.setIdAmbitoElectoral(origen.getIdAmbitoElectoral());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		destino.setEstadoVerificacion(origen.getEstadoVerificacion());
		return destino;
	}
	public static final ObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto ObtenerDatosGeneralesSegundoPasoModelToDtoWrapper(RegistroSeccionA origen) {
		if(origen==null) {			
			return null;
		}
		
		ObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto destino = new ObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto();
		destino.setDniRegistrador(origen.getDniRegistrador());
		destino.setNombreApellidoRegistrador(origen.getNombreApellidoRegistrador());
		
		destino.setIdDependencia(origen.getIdUnidadOrganica());
		
		ListarUbigeoBasicoOutputDto departamento = new ListarUbigeoBasicoOutputDto();
		departamento.setIdUbigeo(origen.getUbigeoDepartamento().getIdUbigeo());
		departamento.setUbigeo(origen.getUbigeoDepartamento().getUbigeo());
		departamento.setNombre(origen.getUbigeoDepartamento().getNombre());
		departamento.setActivo("1");
		destino.setDepartamento(departamento);
		
		ListarUbigeoBasicoOutputDto provincia = new ListarUbigeoBasicoOutputDto();
		provincia.setIdUbigeo(origen.getUbigeoProvincia().getIdUbigeo());
		provincia.setUbigeo(origen.getUbigeoProvincia().getUbigeo());
		provincia.setNombre(origen.getUbigeoProvincia().getNombre());
		provincia.setActivo("1");
		destino.setProvincia(provincia);
		
		ListarUbigeoBasicoOutputDto distrito = new ListarUbigeoBasicoOutputDto();
		distrito.setIdUbigeo(origen.getUbigeo().getIdUbigeo());
		distrito.setUbigeo(origen.getUbigeo().getUbigeo());
		distrito.setNombre(origen.getUbigeo().getNombre());	
		distrito.setActivo("1");
		destino.setDistrito(distrito);
		
		if(origen.getCentroPoblado() != null) {
			ListarCentroPobladoBasicoOutputDto centroPoblado = new ListarCentroPobladoBasicoOutputDto();
			centroPoblado.setIdUbigeo(origen.getCentroPoblado().getId());
			centroPoblado.setNombre(origen.getCentroPoblado().getNombre());
			destino.setCentroPoblado(centroPoblado);
		}
		
		
		destino.setLatitudDistrito(origen.getLatitudDistrito());
		destino.setLongitudDistrito(origen.getLongitudDistrito());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		destino.setIdTipoAmbitoElectoral(origen.getIdTipoAmbitoElectoral());
		destino.setIdAmbitoElectoral(origen.getIdAmbitoElectoral());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		
		return destino;
	}
	public static final ObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto ObtenerDatosGeneralesTerceroPasoModelToDtoWrapper(RegistroSeccionA origen) {
		if(origen==null) {			
			return null;
		}	
		
		ObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto destino = new ObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto();
		 
		if(origen.getDireccion() != null) {
			destino.setIdTipoVia(origen.getDireccion().getIdTipoVia());
			destino.setNombreVia(origen.getDireccion().getNombreVia());
			destino.setNumero(origen.getDireccion().getNumero());
			destino.setKilometro(origen.getDireccion().getKilometro());
			destino.setInterior(origen.getDireccion().getInterior());
			destino.setManzana(origen.getDireccion().getManzana());
			destino.setDepartamento(origen.getDireccion().getDepartamento());
			destino.setLote(origen.getDireccion().getLote());
			destino.setPiso(origen.getDireccion().getPiso());			
			destino.setIdTipoZona(origen.getDireccion().getIdTipoZona());
			destino.setNombreZona(origen.getDireccion().getNombreZona());
			destino.setReferencia(origen.getDireccion().getReferencia());
		}
		
		destino.setLatitudPuertaAcceso(origen.getLatitudPuertaAcceso());
		destino.setLongitudPuertaAcceso(origen.getLongitudPuertaAcceso());
		destino.setDistanciaCentroDistrito(origen.getDistanciaCentroDistrito());
		destino.setTiempoCentroDistrito(origen.getTiempoCentroDistrito());
		destino.setIdTipoInstitucion(origen.getIdTipoInstitucion());
		destino.setIdCategoriaInstitucion(origen.getIdCategoriaInstitucion());
		destino.setOtraCategoria(origen.getOtraCategoria());
		destino.setIdNivel(origen.getIdNivelInstruccion());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		
		return destino;
	}
	public static final ObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto ObtenerDatosGeneralesCuartoPasoModelToDtoWrapper(RegistroSeccionA origen) {
		if(origen==null) {
			return null;
		}
		
		ObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto destino = new ObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto();
		
		
		
		destino.setDocumentoIdentidadRepresentante(origen.getDniRepresentanteLV());
		destino.setNombreCompletoRepresentante(origen.getNombreApeRepresentanteLV());
		destino.setCargoRepresentante(origen.getCargoRepresentanteLV());
		
		destino.setTelefonoRepresentante(origen.getTelefonoRepresentateLV());
		destino.setCorreoRepresentante(origen.getCorreoRepresentateLV());
		
		destino.setDocumentoIdentidadRepresentanteApafa(origen.getDniRepresentanteApafa());		
		destino.setNombreCompletoRepresentanteApafa(origen.getNombreApeRepresentanteApafa());
		destino.setTelefonoRepresentanteApafa(origen.getTelefonoRepresentanteApafa());
		
		destino.setCantidadAula(origen.getCantidadAula());
		destino.setCantidadMesa(origen.getCantidadMesa());
		destino.setCodigoMinedu(origen.getCodigoMinedu());
		destino.setIdTipoTecnologia(origen.getIdTipoTecnologia());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		return destino;
	}
	
	
	public static final ObtenerDatosGeneralesLocalVotacionOutputDto ObtenerDatosGeneralesModelToDtoWrapper(RegistroSeccionA origen) {
		if(origen==null) {
			return null;
		}
		
		ObtenerDatosGeneralesLocalVotacionOutputDto destino = new ObtenerDatosGeneralesLocalVotacionOutputDto();
		/*destino.setCodigoUbigeoNivel1(origen.getCodigoUbigeoNivel1());
		destino.setCodigoUbigeoNivel2(origen.getCodigoUbigeoNivel2());
		destino.setCodigoUbigeoNivel3(origen.getCodigoUbigeoNivel3());
		destino.setNombreDepartamento(origen.getNombreUbigeoNivel1());
		destino.setNombreProvincia(origen.getNombreUbigeoNivel2());
		destino.setNombreDistrito(origen.getNombreUbigeoNivel3());*/
		destino.setIdTipoLocal(origen.getMaeLocalVotacion().getIdTipoLocal());		
		destino.setNombreLocal(origen.getMaeLocalVotacion().getNombreLocal());
		destino.setNumeroLocal(origen.getMaeLocalVotacion().getNumeroLocal());
		destino.setIdCentroPoblado(origen.getCentroPoblado() != null ? origen.getCentroPoblado().getId():null);
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		destino.setDniRegistrador(origen.getDniRegistrador());
		destino.setNombreApellidoRegistrador(origen.getNombreApellidoRegistrador());
		destino.setIdDependencia(origen.getIdUnidadOrganica());
		
		destino.setNombreCentroPoblado(origen.getCentroPoblado() != null ? origen.getCentroPoblado().getNombre():"");
		destino.setLatitudDistrito(origen.getLatitudDistrito());
		destino.setLongitudDistrito(origen.getLongitudDistrito());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		if(origen.getDireccion() != null) {
			destino.setIdTipoVia(origen.getDireccion().getIdTipoVia());
			destino.setNombreVia(origen.getDireccion().getNombreVia());
			destino.setNumero(origen.getDireccion().getNumero());
			destino.setKilometro(origen.getDireccion().getKilometro());
			destino.setInterior(origen.getDireccion().getInterior());
			destino.setManzana(origen.getDireccion().getManzana());
			destino.setDepartamento(origen.getDireccion().getDepartamento());
			destino.setLote(origen.getDireccion().getLote());
			destino.setPiso(origen.getDireccion().getPiso());
			destino.setIdTipoZona(origen.getDireccion().getIdTipoZona());
			destino.setNombreZona(origen.getDireccion().getNombreZona());
			destino.setReferencia(origen.getDireccion().getReferencia());
		}
					
		destino.setLatitudPuertaAcceso(origen.getLatitudPuertaAcceso());
		destino.setLongitudPuertaAcceso(origen.getLongitudPuertaAcceso());
		destino.setDistanciaCentroDistrito(origen.getDistanciaCentroDistrito());
		destino.setTiempoCentroDistrito(origen.getTiempoCentroDistrito());
		destino.setIdTipoInstitucion(origen.getIdTipoInstitucion());
		destino.setIdCategoriaInstitucion(origen.getIdCategoriaInstitucion());
		destino.setOtraCategoria(origen.getOtraCategoria());
	
		destino.setIdNivel(origen.getMaeLocalVotacion().getIdNivelInstitucion());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		destino.setDocumentoIdentidadRepresentante(origen.getDniRepresentanteLV());
		destino.setNombreCompletoRepresentante(origen.getNombreApeRepresentanteLV());
		destino.setCargoRepresentante(origen.getCargoRepresentanteLV());
		destino.setTelefonoRepresentante(origen.getTelefonoRepresentateLV());
		destino.setCorreoRepresentante(origen.getCorreoRepresentateLV());
		destino.setDocumentoIdentidadRepresentanteApafa(origen.getDniRepresentanteApafa());		
		destino.setNombreCompletoRepresentanteApafa(origen.getNombreApeRepresentanteApafa());
		destino.setTelefonoRepresentanteApafa(origen.getTelefonoRepresentanteApafa());
		destino.setCantidadAula(origen.getCantidadAula());
		destino.setCantidadMesa(origen.getCantidadMesa());
		destino.setCodigoMinedu(origen.getCodigoMinedu());
		destino.setIdTipoTecnologia(origen.getIdTipoTecnologia());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		return destino;
	}
	
	
	public static final List<LocalVotacionOutputDto> listarLvActivosModelToDtoWrapper(List<RegistroSeccionA> origen) {
		if(origen==null) {
			return null;
		}
		List<LocalVotacionOutputDto> resultList = new ArrayList<>();
		LocalVotacionOutputDto dtoOut = null;
		for (RegistroSeccionA param : origen) {
			dtoOut = new LocalVotacionOutputDto();
			dtoOut.setNombreLV(param.getNombreLocalLV());
			dtoOut.setNumeroLV(param.getNumeroLocaLV());
			if(param.getDireccion() != null) {
				dtoOut.setNombreVia(param.getDireccion().getNombreVia());
			}
			dtoOut.setLongitud(param.getLongitudDistrito());
			dtoOut.setLatitud(param.getLatitudDistrito());
			dtoOut.setLatitudPuertaAcceso(param.getLatitudPuertaAcceso());
			dtoOut.setLongitudPuertaAcceso(param.getLongitudPuertaAcceso());
			dtoOut.setTotalRegistros(param.getTotalRegistros());
			dtoOut.setTotalPaginas(param.getTotalPaginas());
			resultList.add(dtoOut);
		}
		return resultList;
	}


	
}
