package pe.gob.onpe.sisglvbackend.vista.dtos.input;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActualizarAplicacionInputDto {
	@NotNull(message = "El campo id de aplicacion no puede ser nulo")
	private Integer idAplicacion;
	
	private String codigo;
	
	@NotBlank(message = "El nombre no puede ser nulo ni estar en blanco")
	@NotNull(message = "El nombre no puede ser nulo")
	private String nombre;

	private String descripcion;
	
	@NotBlank(message = "El estado no puede ser nulo ni estar en blanco")
	@NotNull(message = "El estado no puede ser nulo")
	private String estado;
	
	private String url;
	@NotNull(message = "El idUnidadOrganica no puede ser nulo")
	private Integer idUnidadOrganica;
}
