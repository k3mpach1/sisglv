package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LocalVotacionInputDto {
    
  
    @NotNull(message = "Id ubigeo distrito es obligatorio")
    private Integer idUbigeoDistrito;
    
    private String nombre;
    private String numero;
    	
   
    
}
