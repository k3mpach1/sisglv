package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetalleHistoricoSeccionDOutputDto {

    private Integer idDetFichaSeccionD;
    private Integer tipoArea;
    private String descripcionTipoArea;
    private Integer largo;
    private Integer ancho;
    private String referencia;
    private Integer tieneTecho;

    private Integer tienePuerta;
    private Integer cantidadPuerta;
    private Integer tieneAccesoLuz;

}
