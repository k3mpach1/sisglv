package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto {
	private Integer idTipoInstitucion;
	private Integer idCategoriaInstitucion;
	private String otraCategoria;
	private Integer idNivel;
	private Integer idTipoVia;
	private String nombreVia;
	private Integer numero;
	private Float kilometro;
	private String manzana;
	private String interior;
	private String departamento;
	
	private String lote;
	private Integer piso;
	private Integer idTipoZona;
	private String nombreZona;
	private String referencia;
	private String latitudPuertaAcceso;
	private String longitudPuertaAcceso;
	private Float distanciaCentroDistrito;
	private String tiempoCentroDistrito;
	private String observacionRegistro;
}
