package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;

import pe.gob.onpe.sisglvbackend.negocio.modelos.Aplicacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.Usuario;
import pe.gob.onpe.sisglvbackend.negocio.modelos.UsuarioSesion;
import pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion.ApiTokenProviderServicioImpl;
import pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion.captcha.CaptchaServicioV3;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CaptchaServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.UsuarioServicio;
import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteJwt;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotAuthorizedException;
import pe.gob.onpe.sisglvbackend.transversal.properties.ValidarCustomProperties;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.ActualizarNuevaClaveInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.AsignarPersonaUsuarioGeneradoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.CargarAccesosInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.LoginInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.CargarAccesoDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.RefrescarTokenOutputDto;


//@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Validated
@RequestMapping(ConstanteApi.VERSION_API + "/api/seguridad")
public class SeguridadController {

	private UsuarioServicio usuarioServicio;

	@Autowired
	private JWTTokenProvider jwtTokenProvider;
	
	@Autowired
	public SeguridadController(UsuarioServicio usuarioServicio) {
		this.usuarioServicio = usuarioServicio;
		//this.jwtTokenProvider = jwtTokenProvider;
	}

	@GetMapping("/refreshtoken")
	public ResponseEntity<?> refreshtoken(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token)
			throws Exception {
		/*HttpHeaders responseHeaders = new HttpHeaders();
		
		RefrescarTokenOutputDto dto = usuarioServicio.refrescarToken(token);

		HashMap<String, Object> response = new HashMap<String, Object>();

		response.put("resultado", dto.getResultado());
		response.put("mensaje", dto.getMensaje());
		response.put("token", dto.getToken());*/
		HttpHeaders responseHeaders = new HttpHeaders();
		token = jwtTokenProvider.obtenerSoloToken(token);
		Claims claims = Jwts.parser().setSigningKey(ConstanteJwt.SECRET).parseClaimsJws(token).getBody();

		Map<String, Object> expectedMap = Funciones.getMapFromIoJsonwebtokenClaims((DefaultClaims) claims);

		String tokenRefresh = jwtTokenProvider.generarRefreshToken(expectedMap,
				expectedMap.get("numeroDocumento").toString());

		HashMap<String, Object> response = new HashMap<String, Object>();

		response.put("resultado", 1);
		response.put("mensaje", "todo correcto");
		response.put("token", tokenRefresh);

		return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);

		
	}

}
