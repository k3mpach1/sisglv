package pe.gob.onpe.sisglvbackend.vista.dtos.input.reporte;

import lombok.*;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

/**
 * @author glennlq
 * @created 1/7/22
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidarClaveReporteInputDto {
    String clave;
}
