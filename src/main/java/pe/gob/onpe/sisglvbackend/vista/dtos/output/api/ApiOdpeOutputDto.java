package pe.gob.onpe.sisglvbackend.vista.dtos.output.api;

import lombok.*;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiOdpeOutputDto {
    String nombreOdpe;
    String sede;
    String ubigeo;
    String departamentoContinente;
    String provinciaPais;
    String distritoCiudad;
    String codigoOdpe;
}
