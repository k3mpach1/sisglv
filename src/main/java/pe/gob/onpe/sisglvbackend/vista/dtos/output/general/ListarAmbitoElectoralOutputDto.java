package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarAmbitoElectoralOutputDto {
	private Integer idAmbitoElectoral;
	private Integer idAmbitoElectoralPadre;
	private String nombre;
	private String abreviatura;
	private Integer tipoAmbitoElectoral;
	
}
