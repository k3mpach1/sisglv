package pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EjecutarResultadoProspectivaExtranjeroDto {

	private String ubigeo;
	private String departamento;
	private String provincia;
	private String distrito;
	private Double desvEst;
	private Double promedio;
	private Double cv;
}
