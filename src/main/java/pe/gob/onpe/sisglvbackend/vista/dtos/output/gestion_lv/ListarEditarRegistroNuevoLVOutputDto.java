package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarEditarRegistroNuevoLVOutputDto {
	
	private Integer idVerificacionLV;
	
	private Integer idLocalVotacion;
	private String nombreLocalVotacion;
	
	private Integer terminadodRegistroSeccionA;
	private Integer idEstadoRegistroSeccionA;
	private String  estadoRegistroSeccionA;
	private Integer idRegistroSeccionA;
	
	private Integer terminadodRegistroSeccionB;
	private Integer idEstadoRegistroSeccionB;
	private String  estadoRegistroSeccionB;
	private Integer idRegistroSeccionB;
	
	private Integer terminadodRegistroSeccionC;
	private Integer idEstadoRegistroSeccionC;
	private String  estadoRegistroSeccionC;
	private Integer idRegistroSeccionC;
	
	private Integer terminadodRegistroSeccionD;
	private Integer idEstadoRegistroSeccionD;
	private String  estadoRegistroSeccionD;
	private Integer idRegistroSeccionD;
	
	
	private Integer terminadodRegistroSeccionE;
	private Integer idEstadoRegistroSeccionE;
	private String  estadoRegistroSeccionE;
	private Integer idRegistroSeccionE;
	
	private String estadoLocalVotacion;
	private Integer idEstadoLocalVotacion;
	
	private String ubigeo;
    private String nombreUbigeoNivel1;
    private String nombreUbigeoNivel2;
    private String nombreUbigeoNivel3;
    private String nombreCentroPoblado;
    private Integer idTipoVia;
    private String nombreVia;
    private Integer direccionNumero;
    private Float direccionKilometro;
    private String direccionManzana;
    private String direccionDepartamento;
    private String direccionLote;
    private Integer direccionPiso;
    private Integer idTipoZona;
    private String nombreZona;
    private Date fechaModificacion;
    
    private Integer totalPaginas;
    private Integer totalRegistros;
    
    private Integer estadoDisponible;
    private String nombreEstadoDisponible;

}
