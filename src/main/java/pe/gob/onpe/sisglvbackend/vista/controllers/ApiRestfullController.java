package pe.gob.onpe.sisglvbackend.vista.controllers;

import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion.ApiTokenProviderServicioImpl;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiLocalVotacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiOdpeServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiTokenServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProcesoElectoralServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.TokenApiException;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.api.ApiLocalVotacionInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.api.ApiOdpeInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiBasicOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiProcesoElectoralOutputDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author glennlq
 * @created 10/21/21
 */
//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/restfull")
@Validated
public class ApiRestfullController {

    @Autowired
    ApiOdpeServicio apiOdpeServicio;

    @Autowired
    ApiTokenProviderServicioImpl jwtTokenProvider;

    @Autowired
    ApiTokenServicio apiTokenServicio;

    @Autowired
    ApiLocalVotacionServicio apiLocalVotacionServicio;

    @Autowired
    ProcesoElectoralServicio procesoElectoralServicio;

    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation(value = "Lista de Procesos Electorales activos.")
    @PostMapping(value = "/lista-proceso-electoral")
    public ResponseEntity<?> listaProcesoElectoral(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            ApiToken apiToken = apiTokenServicio.getApiTokenByToken(jwtTokenProvider.resolveToken(request));
            MaeProcesoElectoral param = new MaeProcesoElectoral();
            List<MaeProcesoElectoral> paramOut = procesoElectoralServicio.listarActivos(param);
            List<ApiProcesoElectoralOutputDto> lista = new ArrayList<>();
            for (MaeProcesoElectoral maeProcesoElectoral : paramOut) {
                lista.add(modelMapper.map(maeProcesoElectoral, ApiProcesoElectoralOutputDto.class));
            }
            ApiBasicOutputDto apiBasicOutputDto = ApiBasicOutputDto.builder()
                    .estado(true)
                    .codigoError(0)
                    .mensaje("")
                    .cantidad(lista.size())
                    .resultado(lista)
                    .build();

            return ResponseEntity.ok(apiBasicOutputDto);
        } catch (TokenApiException e) {
            response.sendError(e.getHttpStatus().value(), e.getMessage());
        }
        return null;
    }

    @ApiOperation(value = "Lista de ODPEs del Proceso Electoral a consular.")
    @PostMapping(value = "/lista-odpe")
    public ResponseEntity<?> listaOdpe(HttpServletRequest request, HttpServletResponse response, @RequestBody ApiOdpeInputDto apiOdpeInputDto) throws Exception {
        try {
            ApiToken apiToken = apiTokenServicio.getApiTokenByToken(jwtTokenProvider.resolveToken(request));
            return ResponseEntity.ok(apiOdpeServicio.getOdpePorProcesoElectoral(apiToken, apiOdpeInputDto));
        } catch (TokenApiException e) {
            e.printStackTrace();
            response.sendError(e.getHttpStatus().value(), e.getMessage());
        }
        return null;
    }


    @ApiOperation(value = "Lista de Locales de Votación del Proceso Electoral a consular.")
    @PostMapping(value = "/lista-local-votacion")
    public ResponseEntity<?> listaLocalVotacion(HttpServletRequest request, HttpServletResponse response, @RequestBody ApiLocalVotacionInputDto apiLocalVotacionInputDto) throws Exception {
        try {
            ApiToken apiToken = apiTokenServicio.getApiTokenByToken(jwtTokenProvider.resolveToken(request));
            return ResponseEntity.ok(apiLocalVotacionServicio.getLocalVotacionPorProcesoElectoral(apiToken, apiLocalVotacionInputDto));
        } catch (TokenApiException e) {
            response.sendError(e.getHttpStatus().value(), e.getMessage());
        }
        return null;
    }
}
