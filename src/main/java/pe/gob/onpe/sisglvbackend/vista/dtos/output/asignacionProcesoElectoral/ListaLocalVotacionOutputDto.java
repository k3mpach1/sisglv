package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.*;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListaLocalVotacionOutputDto {
    private Integer pagina;
    private Integer total;
    private List<LocalVotacionOutputDto> lista;
}
