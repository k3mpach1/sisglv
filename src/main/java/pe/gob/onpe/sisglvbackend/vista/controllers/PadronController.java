package pe.gob.onpe.sisglvbackend.vista.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nonapi.io.github.classgraph.utils.StringUtils;
import pe.gob.onpe.sisglvbackend.negocio.beans.Persona;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCatalogo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCatalogoEstructura;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CatalogoServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.PadronService;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteCatalogo;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ConsultaPadronNombreOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosConsultaPadronOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarCatalogoBasicoOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCatalogoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseConsultaPadronOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarCatalogoBasicoOuputDto;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/padron")
@Validated
public class PadronController {
    
    @Autowired
    PadronService padronServicio;

    @ApiOperation(value = "Obtener nombre de persona por dni", notes = "Obtener nombre de persona por dni")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
   
    @GetMapping("/obtener-persona/{numeroDocumento}")
    public ResponseEntity<?> obtenerPersona(@PathVariable("numeroDocumento") String numeroDocumento) throws Exception {

    	Persona persona = new Persona();
    	persona.setNumeroDocumento(numeroDocumento);
        padronServicio.consultarPadron(persona);

        ConsultaPadronNombreOutputDto  output = null;
        if(persona.getNombres() != null) {
        	output = new ConsultaPadronNombreOutputDto();
        	output.setNombres(persona.getNombres());
        	output.setNumeroDocumento(persona.getNumeroDocumento());
        	output.setSoloNombre(persona.getSoloNombres());
        	output.setApellidoPaterno(persona.getApellidoPaterno());
        	output.setApellidoMaterno(persona.getApellidoMaterno());
        }
        
        DatosConsultaPadronOutputDto  datos = new DatosConsultaPadronOutputDto();
        datos.setPersona(output);

       
        
        
        ResponseConsultaPadronOutputDto response = new ResponseConsultaPadronOutputDto();
        response.setResultado(persona.getResultado());
        response.setMensaje( persona.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    
    
    
}
