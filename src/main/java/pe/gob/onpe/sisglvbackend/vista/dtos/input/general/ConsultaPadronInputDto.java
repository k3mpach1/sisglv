package pe.gob.onpe.sisglvbackend.vista.dtos.input.general;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaPadronInputDto {
	
	
	private String dni;
	private String clave;
	private String codigo;

}
