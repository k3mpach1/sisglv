package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto {
	private Integer tieneCercoPerimetrico;
	private Integer idEstadoCerco;
	private Integer cantidaPuertasAcceso;
	private Integer idEstadoPuertasAcceso;
	private Integer cantidaSshh;
	private Integer idEstadoSshh;
	
	private Integer tieneServicioAgua;
	private String desdeHoraServicioAgua;
	private String hastaHoraServicioAgua;
	
	private Integer tieneEnergiaElectrica;
	private String desdeHoraEnergiaElectrica;
	private String hastaHoraEnergiaElectrica;
	private String observacionRegistro;
	private Integer tieneServicioIntenet;
	private String proveedorInternet;
	
}
