package pe.gob.onpe.sisglvbackend.vista.dtos.output.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiBasicOutputDto {

    @ApiModelProperty(notes = "Estado de la operación, true = exito, false = error", example = "true")
    boolean estado;
    @ApiModelProperty(notes = "Código de error, en caso existiera.", example = "100")
    Integer codigoError;
    @ApiModelProperty(notes = "Mensaje de error en caso existiera.", example = "Proceso electoral no existe")
    String mensaje;
    @ApiModelProperty(notes = "Cantidad de registros del resultado.", example = "100")
    Integer cantidad;
    @ApiModelProperty(notes = "Lista de resultados.", example = "[]")
    Object resultado;
}
