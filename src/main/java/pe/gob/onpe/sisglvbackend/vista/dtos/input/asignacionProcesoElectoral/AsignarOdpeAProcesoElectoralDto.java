package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AsignarOdpeAProcesoElectoralDto {
    private Integer idCargarArchivo;
}
