package pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosPeriodoOutputDto {

	private Long   id;
	private String periodo;
	private Long   orden;
	private String importado;
	
}
