package pe.gob.onpe.sisglvbackend.vista.dtos.input.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.BasePaginacionInputDto;

@Getter
@Setter
public class ListarFiltroLocalLocalVotacionHistoricoInputDto extends BasePaginacionInputDto {
    private String nombreLocal;
    private Integer idEstado;
    private String codigoUbigeoNivel1;
    private String codigoUbigeoNivel2;
    private String codigoUbigeoNivel3;
}
