package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosDetSeccionEListadoOutputDto {

	private Long idDetRegistroSeccionE;
	private DatosArchivoLvListadoOutputDto archivo;
	
}
