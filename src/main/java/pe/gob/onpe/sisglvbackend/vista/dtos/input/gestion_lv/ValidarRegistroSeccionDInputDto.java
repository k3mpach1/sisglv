package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidarRegistroSeccionDInputDto {

	@NotNull(message = "ID del registro de la seccion D")
	private Integer idCabRegistroSeccionD;
	private Integer idVerificacionLv;
	private String observacionRegistro;
	

	
}
