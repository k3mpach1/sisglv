package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ObtenerLocalVotacionHistoricoSeccionAOutputDto {
    private String documentoIdentidadRegistrador;
    private String nombreCompletoRegistrador;
    private Integer idDependencia;
    private Integer idOdpeOrc;
    private Integer idUbigeoNivel1;
    private Integer idUbigeoNivel2;
    private Integer idUbigeoNivel3;

    private String nombreUbigeoNivel1;
    private String nombreUbigeoNivel2;
    private String nombreUbigeoNivel3;

    private Integer idCentroPoblado;
    private String nombreCentroPoblado;

    private String latitudDistrito;
    private String longitudDistrito;
    private Integer idTipoLocalVotacion;
    private String nombreTipoLocalVotacion;
    private String nombreLocalVotacion;
    private String numeroLocalVotacion;
    private Integer idTipoInstitucion;
    private String nombreTipoInstitucion;
    private Integer idCategoriaInstitucion;
    private String nombreCategoriaInstitucion;
    private String otraCategoria;
    private Integer idNivel;
    //datos de la direccion
    private Integer idTipoVia;
    private String nombreVia;
    private Integer direccionNumero;
    private Float direccionKilometro;
    private String direccionManzana;
    private String direccionDepartamento;
    private String direccionLote;
    private Integer direccionPiso;
    private Integer idTipoZona;
    private String nombreZona;
    private String referencia;
    private String latitudPuertaAcceso;
    private String longitudPuertaAcceso;
    private Float distanciaCentroDistrito;
    private String tiempoCentroDistrito;

    private Integer idUnidadOrganica;
    private String nombreUnidadOrganica;

    private Integer idTipoAmbitoGeografico;
    private String nombreTipoAmbitoGeografico;

    private Integer idTipoAmbitoElectoral;
    private String nombreTipoAmbitoElectoral;


    private Integer idAmbitoElectoral;
    private String nombreAmbitoElectoral;

    private Integer idNivelInstruccion;
    private String nombreNivelInstruccion;

    private Integer direccionIdTipoVia;
    private String direccionNombreTipoVia;
    private String direccionNombreVia;
    private Integer direccionIdTipoZona;
    private String direccionNombreTipoZona;
    private String direccionNombreZona;
    private String direccionReferencia;
    private String dniRepresentanteApafa;
    private String nombreApeRepresentanteApafa;
    private String telefonoApeRepresentanteApafa;
    private Integer cantidadAulaDisponible;
    private Integer cantidadMesaDisponible;
    private String codigoMinedu;
    private Integer idSolucionTecnologica;
}
