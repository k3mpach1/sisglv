package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrarProcesoElectoralInicioOutput extends BaseOutputDto{
	
	private Integer idProceso;

}
