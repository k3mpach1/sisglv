package pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProyeccionParte2InputDto {
    Integer idPadronProyeccion;
    Double toleranciaNacional;
    Double toleranciaExtranjero;
    Double toleranciaLimaCallao;
    Double toleranciaProvinciaDis;
    Double toleranciaExtranjeroDis;
}
