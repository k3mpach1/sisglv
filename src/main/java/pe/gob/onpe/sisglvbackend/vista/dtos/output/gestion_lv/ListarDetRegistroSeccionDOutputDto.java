package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarDetRegistroSeccionDOutputDto {
	
	private Integer idDetRegistroSeccionD;
	private Integer  tipoArea;
	private String  descripcionTipoArea;
	private BigDecimal largo;
	private BigDecimal ancho;
	private String referencia;
	private Integer tieneTecho;
	private Integer tienePuerta;
	private Integer cantidadPuerta;
	private Integer tieneAccesoLuz;
	private String observacion;
	private Integer activo;
	
}
