package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter 
@Setter
public class RegistrarGeneracionUsuarioPorProcesoInputDto {
	
	@NotNull(message = "El idProcesoElectoral no puede ser nulo")
	private Integer idProcesoElectoral;
	
	@NotNull(message = "El idUnidadOrganica no puede ser nulo")
	private Integer idUnidadOrganica;
	
	@NotNull(message = "El idTipoAmbito no puede ser nulo")
	private Integer idTipoAmbito;//1=odpe 2=orc 3 = cantidad
	
	@NotNull(message = "El campo perfil no puede ser nulo")
	private List<PerfilInputDto> perfiles;
	

	
}
