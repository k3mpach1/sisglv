package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class DatosListarFiltroLocalVotacionHistoricoOutputDto {
   List<ListarFiltroLocalVotacionHistoricoOutputDto> localesVotacionHistorico;
   ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDto totales;
}
