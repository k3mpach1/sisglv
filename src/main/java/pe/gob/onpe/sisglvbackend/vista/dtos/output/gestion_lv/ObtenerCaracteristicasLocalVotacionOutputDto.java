package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerCaracteristicasLocalVotacionOutputDto {
	private Integer tieneCercoPerimetrico;
	private Integer idEstadoCercoPerimetrico;
	private Integer cantidadSshh;
	private Integer idEstadoSshh;
	
	private Integer cantidadPuertaAcceso;
	private Integer idEstadoPuertaAcceso;
	
	private Integer idEstadoConservacionPuertasAulas;
	private Integer idEstadoConservacionTechosAulas;
	private Integer idEstadoConservacionParedesAulas;
	private Integer idEstadoConservacionVentanasAulas;
	private Integer idEstadoConservacionPisosAulas;
	
	private Integer tieneServicioAgua;
	private String desdeServicioAgua;
	private String hastaServicioAgua;
	
	private Integer tieneServicioEnergiaElectrica;
	private String desdeServicioEnergiaElectrica;
	private String hastaServicioEnergiaElectrica;
	
	private Integer tieneServicioInternet;
	private String proveedorInternet;
	
	private Integer tienePatio;
	private Integer cantidadPatio;
	private Integer cantidadCirculoSeguridad;
	private String suministroEnergiaElectrica;
	
	
	private Float areaAproximadaM2;
	private Float areaConstruidaM2;
	private Float areaSinConstruirM2;
	
	private String observacionRegistro;	
	private Integer estadoVerificacion;
}
