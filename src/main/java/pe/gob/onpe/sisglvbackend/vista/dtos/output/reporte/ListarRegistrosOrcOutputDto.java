package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarRegistrosOrcOutputDto {
	
	private String ubigeo;
	private String orc;
	private String departamento;
	private String provincia;
	private String distrito;
	private String tipoRegistro;
	private Integer totalRegistros;
	private Integer esNuevo;

	
}
