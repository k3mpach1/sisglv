package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarDatosGeneralesCuartoPasoLVInputDto {
	@NotNull(message = "Id Registro Seccion A es obligatorio")
	private Integer idRegistroSeccionA;
	@NotNull(message = "Id Verificacion es obligatorio")
	private Integer idVerificacion;
	
	private String documentoIdentidadRepresentante;
	private String nombreCompletoRepresentante;
	private String cargoRepresentante;
	private String telefonoRepresentante;
	private String correoRepresentante;
	private String documentoIdentidadRepresentanteApafa;
	private String nombreCompletoRepresentanteApafa;
	private String telefonoRepresentanteApafa;
	private Integer cantidadAula;
	private Integer cantidadMesa;
	private String codigoMinedu;
	private Integer idTipoTecnologia;
}
