package pe.gob.onpe.sisglvbackend.vista.dtos.input.habilitarProcesoVerificacion;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarProcesoVerificacionInputDto {
	
	@ApiModelProperty(notes = "Nombre de proceso de verificacion.", example = "Proceso de verificacion Eg2021", required = true)
	@NotBlank(message = "nombre de proceso es obligatorio")
	private String nombre;
	
	@ApiModelProperty(notes = "tipo de ambito electoral.", example = "1", required = true)
	@NotNull(message = "tipo ambito electoral es obligatorio")
	private Integer tipoAmbitoElectoral;
	
	@ApiModelProperty(notes = "acronimo", required = false)
	private String acronimo;
	
	

}
