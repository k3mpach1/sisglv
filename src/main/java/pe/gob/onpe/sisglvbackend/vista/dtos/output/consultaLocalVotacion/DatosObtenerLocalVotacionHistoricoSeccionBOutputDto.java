package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosObtenerLocalVotacionHistoricoSeccionBOutputDto {
    private ObtenerLocalVotacionHistoricoSeccionBOutputDto localVotacionHistoricoSeccionB;
}
