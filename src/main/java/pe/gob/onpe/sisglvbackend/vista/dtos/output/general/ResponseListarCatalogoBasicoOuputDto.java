package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
@Schema(name = "ResponseListarCatalogoBasicoOuputDto", description = "The entity that represents Episode")
public class ResponseListarCatalogoBasicoOuputDto extends BaseOutputDto{
	private DatosListarCatalogoBasicoOuputDto datos;	
}
