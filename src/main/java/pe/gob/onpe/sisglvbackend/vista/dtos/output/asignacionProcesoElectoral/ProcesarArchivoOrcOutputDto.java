package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabFormatoPlantilla;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabArchivo;

/**
 * @author glennlq
 * @created 9/22/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcesarArchivoOrcOutputDto {
    private Integer idCargarArchivo; // id de CabCargaArchivo
    private MaeProcesoVerificacion maeProcesoVerificacion;
    private Integer tipoProceso;	// 1 = proceso electoral,   x = proceso verificacion
    private CabFormatoPlantilla cabFormatoPlantilla;
    private TabArchivo tabArchivo;
}
