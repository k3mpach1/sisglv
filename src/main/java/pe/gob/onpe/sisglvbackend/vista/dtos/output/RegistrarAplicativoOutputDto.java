package pe.gob.onpe.sisglvbackend.vista.dtos.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarAplicativoOutputDto {
	private String descripcion;
	private String nombre;
}
