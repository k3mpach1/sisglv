package pe.gob.onpe.sisglvbackend.vista.wrapper;

import java.util.ArrayList;

import java.util.List;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarProcesoElectoralActivosOuputDto;

public class MaeProcesoElectoralWrapper {
	public static final List<ListarProcesoElectoralActivosOuputDto> listarActivosModelToDtoWrapper(List<MaeProcesoElectoral> origen) {
		if(origen==null) {
			return null;
		}
		List<ListarProcesoElectoralActivosOuputDto> resultList = new ArrayList<>();
		ListarProcesoElectoralActivosOuputDto dtoOut = null;
		for (MaeProcesoElectoral maeProcesoElectoral : origen) {
			dtoOut = new ListarProcesoElectoralActivosOuputDto();
			dtoOut.setIdProceso(maeProcesoElectoral.getIdProcesoElectoral());
			dtoOut.setNombreProceso(maeProcesoElectoral.getNombre());
			dtoOut.setAcronimo(maeProcesoElectoral.getAcronimo());
			resultList.add(dtoOut);
		}
		return resultList;
	}
}
