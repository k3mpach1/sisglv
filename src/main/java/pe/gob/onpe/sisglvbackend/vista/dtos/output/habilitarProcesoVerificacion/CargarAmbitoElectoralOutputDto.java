package pe.gob.onpe.sisglvbackend.vista.dtos.output.habilitarProcesoVerificacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargarAmbitoElectoralOutputDto {

	private Integer idAmbitoElectoral;
	private String nombreAmbitoElectoral;
	private Integer idUbigeo;
	private String departamento;
	private String provincia;
	private String distrito;
	private Integer idLocal;
	private String nombreLocal;
	private Integer totalPaginas;
	private Integer totalRegistros; 
}
