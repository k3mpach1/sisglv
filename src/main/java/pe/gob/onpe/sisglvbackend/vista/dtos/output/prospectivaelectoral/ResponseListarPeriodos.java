package pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseListarPeriodos extends BaseOutputDto {

	private List<DatosPeriodoOutputDto> datos;
}
