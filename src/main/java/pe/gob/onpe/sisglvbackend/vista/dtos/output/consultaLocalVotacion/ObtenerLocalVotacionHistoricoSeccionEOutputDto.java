
package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerLocalVotacionHistoricoSeccionEOutputDto {
    
    	private Integer idFichaSeccionE;
	private String seccion;
	private String observacionLV;
        private String observacionRegistro;
        
        private List<DetalleHistoricoSeccionEOutputDto> lista = new ArrayList<>();
    
}
