package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarRegistrosRegistradorOutputDto {
	
	private String dniRegistrador;
	private String nombresRegistrador;
	private String tipoRegistro;
	private Integer total;
	private Integer esNuevo;

	
}
