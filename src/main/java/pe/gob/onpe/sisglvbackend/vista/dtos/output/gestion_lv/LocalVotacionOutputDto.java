package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.negocio.modelos.Direccion;

@Getter
@Setter
public class LocalVotacionOutputDto {
    
    private String nombreLV;
    private String numeroLV;
	private Integer idTipoVia;
	private String nombreTipoVia;
	private String nombreVia;
	private Integer numero;
	private Float kilometro;
	private String interior;
	private String departamento;
	private String manzana;
	private String lote;
	private Integer piso;
	
	private Integer idTipoZona;
	private String nombreTipoZona;
	private String nombreZona;
	private String referencia;
    private String latitud;
    private String longitud;
    private String latitudPuertaAcceso;
    private String longitudPuertaAcceso;
    
    private Integer totalRegistros;
    private Integer totalPaginas;
    
}
