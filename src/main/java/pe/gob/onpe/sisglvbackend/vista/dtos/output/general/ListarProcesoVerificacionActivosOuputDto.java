package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ListarProcesoVerificacionActivosOuputDto {
	private Integer idProceso;
	private String nombreProceso;
	private String acronimo;
}
