package pe.gob.onpe.sisglvbackend.vista.wrapper;

import java.util.ArrayList;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionA;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeLocalVotacion;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.consultaLocalVotacion.ListarFiltroLocalLocalVotacionHistoricoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ListarFiltroLocalVotacionHistoricoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerLocalVotacionHistoricoSeccionAOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerLocalVotacionHistoricoSeccionBOutputDto;

import java.util.List;
import java.util.stream.Collectors;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionE;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.DetalleHistoricoSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.DetalleHistoricoSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.DetalleHistoricoSeccionEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerLocalVotacionHistoricoSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerLocalVotacionHistoricoSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerLocalVotacionHistoricoSeccionEOutputDto;

public class MaeLocalVotacionHistoricoWrapper {
    public static void ListarFiltroLocalLocalVotacionHistoricoDtoToModelWrapper(ListarFiltroLocalLocalVotacionHistoricoInputDto origen,MaeLocalVotacion destino){
        if (origen==null){
            return;
        }
        destino.setFichaSeccionA(new FichaSeccionA());
        destino.getFichaSeccionA().setNombreLocalVotacion(origen.getNombreLocal());
        destino.getFichaSeccionA().setCodigoUbigeoNivel1(origen.getCodigoUbigeoNivel1());
        destino.getFichaSeccionA().setCodigoUbigeoNivel2(origen.getCodigoUbigeoNivel2());
        destino.getFichaSeccionA().setCodigoUbigeoNivel3(origen.getCodigoUbigeoNivel3());
        destino.setIdEstado(origen.getIdEstado());
        destino.setPagina(origen.getPagina());
        destino.setTotalRegistroPorPagina(origen.getTotalRegistroPorPagina());
    }
    public static final List<ListarFiltroLocalVotacionHistoricoOutputDto> ListarFiltroLocalLocalVotacionHistoricoModelToDtoWrapper(List<MaeLocalVotacion> origen) {
        if (origen==null){
            return null;
        }
        List<ListarFiltroLocalVotacionHistoricoOutputDto> destino;
        destino = origen.stream().map(lv->{
            ListarFiltroLocalVotacionHistoricoOutputDto data = new ListarFiltroLocalVotacionHistoricoOutputDto();
            data.setIdFichaSeccionA(lv.getFichaSeccionA().getIdFichaSeccionA());
            data.setIdFichaSeccionB(lv.getFichaSeccionB().getIdFichaSeccionB());

            data.setIdFichaSeccionC(lv.getCabFichaSeccionC().getIdFichaSeccionC());
            data.setIdFichaSeccionD(lv.getCabFichaSeccionD().getIdFichaSeccionD());
            data.setIdFichaSeccionE(lv.getCabFichaSeccionE().getIdFichaSeccionE());

            data.setNombreUbigeoNivel1(lv.getFichaSeccionA().getNombreUbigeoNivel1());
            data.setNombreUbigeoNivel2(lv.getFichaSeccionA().getNombreUbigeoNivel2());
            data.setNombreUbigeoNivel3(lv.getFichaSeccionA().getNombreUbigeoNivel3());
            data.setNombreCentroPoblado(lv.getFichaSeccionA().getNombreCentroPoblado());
            data.setNombreLocalVotacion(lv.getFichaSeccionA().getNombreLocalVotacion());
            data.setNombreVia(lv.getDireccion().getNombreVia());
            data.setDireccionNumero(lv.getDireccion().getNumero());
            data.setDireccionKilometro(lv.getDireccion().getKilometro());
            data.setDireccionManzana(lv.getDireccion().getManzana());

            data.setDireccionInterior(lv.getDireccion().getInterior());
            data.setDireccionDepartamento(lv.getDireccion().getDepartamento());
            data.setDireccionLote(lv.getDireccion().getLote());
            data.setDireccionPiso(lv.getDireccion().getPiso());
            data.setDireccionIdTipoZona(lv.getDireccion().getIdTipoZona());
            data.setDireccionNombreZona(lv.getDireccion().getNombreZona());

            data.setFechaModificacion(lv.getFechaModificacion());

            return data;
        }).collect(Collectors.toList());
        return destino;
    }
    public static final ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDto ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDtoModelToDtoWrapper(List<MaeLocalVotacion> origen) {
        if (origen==null){
            return null;
        }
        if(origen.size()==0){
            return null;
        }
        ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDto destino = new ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDto();
        destino.setTotalPaginas(origen.get(0).getTotalPaginas());
        destino.setTotalRegistros(origen.get(0).getTotalRegistros());
        return destino;
    }

    public static final ObtenerLocalVotacionHistoricoSeccionAOutputDto ObtenerLocalLocalVotacionHistoricoSeccionAModelToDtoWrapper(MaeLocalVotacion origen) {
        if (origen==null){
            return null;
        }
        ObtenerLocalVotacionHistoricoSeccionAOutputDto destino = new ObtenerLocalVotacionHistoricoSeccionAOutputDto();
        destino.setDocumentoIdentidadRegistrador(origen.getFichaSeccionA().getDniRegistrador());
        destino.setNombreCompletoRegistrador(origen.getFichaSeccionA().getNombreApellidoRegistrador());
        destino.setIdUnidadOrganica(origen.getFichaSeccionA().getIdUnidadOrganica());
        destino.setNombreUnidadOrganica(origen.getFichaSeccionA().getNombreUnidadOrganica());


        destino.setIdTipoAmbitoElectoral(origen.getFichaSeccionA().getIdTipoAmbitoElectoral());
        destino.setNombreTipoAmbitoElectoral(origen.getFichaSeccionA().getNombreTipoAmbitoElectoral());

        destino.setIdAmbitoElectoral(origen.getFichaSeccionA().getIdAmbitoElectoral());
        destino.setNombreAmbitoElectoral(origen.getFichaSeccionA().getNombreAmbitoElectoral());
        destino.setIdTipoAmbitoGeografico(origen.getFichaSeccionA().getIdTipoAmbitoGeografico());
        destino.setNombreTipoAmbitoGeografico(origen.getFichaSeccionA().getNombreTipoAmbitoGeografico());
        destino.setIdUbigeoNivel1(origen.getFichaSeccionA().getIdUbigeoNivel1());
        destino.setIdUbigeoNivel2(origen.getFichaSeccionA().getIdUbigeoNivel2());
        destino.setIdUbigeoNivel3(origen.getFichaSeccionA().getIdUbigeoNivel3());
        destino.setNombreUbigeoNivel1(origen.getFichaSeccionA().getNombreUbigeoNivel1());
        destino.setNombreUbigeoNivel2(origen.getFichaSeccionA().getNombreUbigeoNivel2());
        destino.setNombreUbigeoNivel3(origen.getFichaSeccionA().getNombreUbigeoNivel3());

        if (origen.getFichaSeccionA().getMaeCentroPoblado()!=null){
            destino.setIdCentroPoblado(origen.getFichaSeccionA().getMaeCentroPoblado().getId());
            destino.setNombreCentroPoblado(origen.getFichaSeccionA().getMaeCentroPoblado().getNombre());
        }

        destino.setLatitudDistrito(origen.getFichaSeccionA().getLatitudDistrito());
        destino.setLongitudDistrito(origen.getFichaSeccionA().getLongitudDistrito());

        destino.setIdTipoLocalVotacion(origen.getFichaSeccionA().getIdTipoLocalVotacion());
        destino.setNombreTipoLocalVotacion(origen.getFichaSeccionA().getNombreTipoLocalVotacion());

        destino.setIdTipoInstitucion(origen.getFichaSeccionA().getIdTipoInstitucion());
        destino.setNombreTipoInstitucion(origen.getFichaSeccionA().getNombreTipoInstitucion());

        destino.setIdCategoriaInstitucion(origen.getFichaSeccionA().getIdCategoriaInstitucion());
        destino.setNombreCategoriaInstitucion(origen.getFichaSeccionA().getNombreCategoriaInstitucion());

        destino.setIdNivelInstruccion(origen.getFichaSeccionA().getIdNivelInstruccion());
        destino.setNombreNivelInstruccion(origen.getFichaSeccionA().getNombreUnidadOrganica());

        destino.setDireccionIdTipoVia(origen.getFichaSeccionA().getDireccion().getIdTipoVia());
        destino.setDireccionNombreTipoVia(origen.getFichaSeccionA().getDireccion().getNombreTipoVia());
        destino.setDireccionNombreVia(origen.getFichaSeccionA().getDireccion().getNombreVia());
        destino.setDireccionNumero(origen.getFichaSeccionA().getDireccion().getNumero());
        destino.setDireccionKilometro(origen.getFichaSeccionA().getDireccion().getKilometro());
        destino.setDireccionManzana(origen.getFichaSeccionA().getDireccion().getManzana());
        destino.setDireccionDepartamento(origen.getFichaSeccionA().getDireccion().getDepartamento());
        destino.setDireccionLote(origen.getFichaSeccionA().getDireccion().getLote());
        destino.setDireccionPiso(origen.getFichaSeccionA().getDireccion().getPiso());
        destino.setDireccionIdTipoZona(origen.getFichaSeccionA().getDireccion().getIdTipoZona());
        destino.setDireccionNombreTipoZona(origen.getFichaSeccionA().getDireccion().getNombreTipoZona());

        destino.setDireccionNombreZona(origen.getFichaSeccionA().getDireccion().getNombreZona());
        destino.setDireccionReferencia(origen.getFichaSeccionA().getDireccion().getReferencia());

        destino.setLongitudPuertaAcceso(origen.getFichaSeccionA().getLongitudPuertaAcceso());
        destino.setDistanciaCentroDistrito(origen.getFichaSeccionA().getDistanciaCentroDistrito());
        destino.setTiempoCentroDistrito(origen.getFichaSeccionA().getTiempoCentroDistrito());

        destino.setDniRepresentanteApafa(origen.getFichaSeccionA().getDniRepresentanteApafa());
        destino.setNombreApeRepresentanteApafa(origen.getFichaSeccionA().getNombreApeRepresentanteApafa());
        destino.setTelefonoApeRepresentanteApafa(origen.getFichaSeccionA().getTelefonoRepresentanteApafa());

        destino.setCantidadAulaDisponible(origen.getFichaSeccionA().getCantidadAulaDisponible());
        destino.setCantidadMesaDisponible(origen.getFichaSeccionA().getCantidadMesaDisponible());
        destino.setCodigoMinedu(origen.getFichaSeccionA().getCodigoMinedu());
        destino.setIdSolucionTecnologica(origen.getFichaSeccionA().getIdSolucionTecnogica());
        destino.setNombreLocalVotacion(origen.getFichaSeccionA().getNombreSolucionTecnogica());
        return destino;
    }
    public static final ObtenerLocalVotacionHistoricoSeccionBOutputDto ObtenerLocalLocalVotacionHistoricoSeccionBModelToDtoWrapper(MaeLocalVotacion origen) {
        if (origen==null){
            return null;
        }
        ObtenerLocalVotacionHistoricoSeccionBOutputDto destino = new ObtenerLocalVotacionHistoricoSeccionBOutputDto();
        destino.setTieneCercoPerimetrico(origen.getFichaSeccionB().getTieneCercoPerimetrico());
        destino.setIdEstadoCercoPerimetrico(origen.getFichaSeccionB().getIdEstadoCercoPerimetrico());
        destino.setNombreEstadoCercoPerimetrico(origen.getFichaSeccionB().getNombreEstadoCercoPerimetrico());
        destino.setCantidadSshh(origen.getFichaSeccionB().getCantidadSshh());
        destino.setIdEstadoSshh(origen.getFichaSeccionB().getIdEstadoSshh());
        destino.setNombreEstadoSshh(origen.getFichaSeccionB().getNombreEstadoSshh());
        destino.setCantidadPuertaAcceso(origen.getFichaSeccionB().getCantidadPuertaAcceso());
        destino.setIdEstadoPuestaAcceso(origen.getFichaSeccionB().getIdEstadoPuestaAcceso());
        destino.setNombreEstadoParedAula(origen.getFichaSeccionB().getNombreEstadoParedAula());
        destino.setIdEstadoPuertaAula(origen.getFichaSeccionB().getIdEstadoPuertaAula());
        destino.setNombreEstadoPuertaAula(origen.getFichaSeccionB().getNombreEstadoPuertaAula());
        destino.setIdEstadoTechoAula(origen.getFichaSeccionB().getIdEstadoTechoAula());
        destino.setNombreEstadoTechoAula(origen.getFichaSeccionB().getNombreEstadoTechoAula());
        destino.setIdEstadoParedAula(origen.getFichaSeccionB().getIdEstadoParedAula());
        destino.setNombreEstadoParedAula(origen.getFichaSeccionB().getNombreEstadoParedAula());
        destino.setIdEstadoVentanaAula(origen.getFichaSeccionB().getIdEstadoVentanaAula());
        destino.setNombreEstadoVentanaAula(origen.getFichaSeccionB().getNombreEstadoVentanaAula());

        destino.setIdEstadoPisoAula(origen.getFichaSeccionB().getIdEstadoPisoAula());
        destino.setNombreEstadoPisoAula(origen.getFichaSeccionB().getNombreEstadoPisoAula());
        destino.setTieneAgua(origen.getFichaSeccionB().getTieneAgua());
        destino.setHorarioInicioAgua(origen.getFichaSeccionB().getHorarioInicioAgua());
        destino.setHorarioTerminoAgua(origen.getFichaSeccionB().getHorarioTerminoAgua());
        destino.setTieneLuz(origen.getFichaSeccionB().getTieneLuz());
        destino.setHorarioInicioLuz(origen.getFichaSeccionB().getHorarioInicioLuz());
        destino.setHorarioTerminoLuz(origen.getFichaSeccionB().getHorarioTerminoLuz());
        destino.setTieneInternet(origen.getFichaSeccionB().getTieneInternet());
        destino.setNombreProveedorInternet(origen.getFichaSeccionB().getNombreProveedorInternet());

        destino.setTienePatio(origen.getFichaSeccionB().getTienePatio());
        destino.setCantidadPatio(origen.getFichaSeccionB().getCantidadPatio());
        destino.setCantidadCirculoSeguridad(origen.getFichaSeccionB().getCantidadCirculoSeguridad());
        destino.setSuministroLuz(origen.getFichaSeccionB().getSuministroLuz());
        destino.setAreaAproximada(origen.getFichaSeccionB().getAreaAproximada());
        destino.setAreaConstruida(origen.getFichaSeccionB().getAreaConstruida());
        destino.setAreaSinContruir(origen.getFichaSeccionB().getAreaSinContruir());



        return destino;
    }
    
    public static final ObtenerLocalVotacionHistoricoSeccionCOutputDto ObtenerLocalLocalVotacionHistoricoSeccionCModelToDtoWrapper(MaeLocalVotacion origen, List<DetFichaSeccionC> detalles) {
        if (origen == null) {
            return null;
        }

        ObtenerLocalVotacionHistoricoSeccionCOutputDto destino = new ObtenerLocalVotacionHistoricoSeccionCOutputDto();
        destino.setIdFichaSeccionC(origen.getCabFichaSeccionC().getIdFichaSeccionC());
        destino.setSeccion(origen.getCabFichaSeccionC().getSeccion());
        destino.setCantidadAula(origen.getCabFichaSeccionC().getCantidadAula());
        destino.setCantidadMesa(origen.getCabFichaSeccionC().getCantidadMesa());
        destino.setObservacionRegistro(origen.getCabFichaSeccionC().getObservacionRegistro());
        
        List<DetalleHistoricoSeccionCOutputDto> data = new ArrayList<>();
        for (DetFichaSeccionC detalle : detalles) {
            DetalleHistoricoSeccionCOutputDto ds = new DetalleHistoricoSeccionCOutputDto();
            ds.setIdDetFichaSeccionC(detalle.getIdDetFichaSeccionC());
            ds.setAula(detalle.getAula());
            ds.setPabellon(detalle.getPabellon());
            ds.setPiso(detalle.getPiso());
            ds.setRequiereToldo(detalle.getRequiereToldo());
            ds.setUsoAula(detalle.getUsoAula());
            ds.setCantidadMesa(detalle.getCantidadMesa());
            ds.setTipoAula(detalle.getTipoAula());
            ds.setDescripcionTipoAula(detalle.getDescripcionTipoAula());
            ds.setTieneLuminaria(detalle.getTieneLuminaria());
            ds.setCantidadLuminariaBuenEstado(detalle.getCantidadLuminariaBuenEstado());
            ds.setCantidadLuminariaMalEstado(detalle.getCantidadLuminariaMalEstado());
            ds.setTieneTomaCorriente(detalle.getTieneTomaCorriente());
            ds.setCantidadTomacorrienteBuenEstado(detalle.getCantidadTomacorrienteBuenEstado());
            ds.setCantidadTomacorrienteMalEstado(detalle.getCantidadTomacorrienteMalEstado());
            ds.setTieneInterruptor(detalle.getTieneInterruptor());
            ds.setCantidadInterruptorBuenEstado(detalle.getCantidadInterruptorBuenEstado());
            ds.setCantidadInterruptorMalEstado(detalle.getCantidadInterruptorMalEstado());
            ds.setTienePuerta(detalle.getTienePuerta());
            ds.setCantidadPuertaBuenEstado(detalle.getCantidadPuertaBuenEstado());
            ds.setCantidadPuertaMalEstado(detalle.getCantidadPuertaMalEstado());
            ds.setTieneVentana(detalle.getTieneVentana());
            ds.setCantidadVentanaBuenEstado(detalle.getCantidadVentanaBuenEstado());
            ds.setCantidadVentanaMalEstado(detalle.getCantidadVentanaMalEstado());
            ds.setTienePuntoInternet(detalle.getTienePuntoInternet());
            ds.setTieneAccesoWifi(detalle.getTieneAccesoWifi());
            data.add(ds);
        }
        
        destino.setLista(data);
        
        return destino;
    }
    
    
    public static final ObtenerLocalVotacionHistoricoSeccionDOutputDto ObtenerLocalLocalVotacionHistoricoSeccionDModelToDtoWrapper(MaeLocalVotacion origen, List<DetFichaSeccionD> detalles) {
        if (origen == null) {
            return null;
        }

        ObtenerLocalVotacionHistoricoSeccionDOutputDto destino = new ObtenerLocalVotacionHistoricoSeccionDOutputDto();
        destino.setIdFichaSeccionD(origen.getCabFichaSeccionD().getIdFichaSeccionD());
        destino.setSeccion(origen.getCabFichaSeccionD().getSeccion());
        destino.setObservacionRegistro(origen.getCabFichaSeccionD().getObservacionRegistro());
        destino.setUsuarioObservacion(origen.getCabFichaSeccionD().getUsuarioObservacion());
        destino.setUsuarioAprobacion(origen.getCabFichaSeccionD().getUsuarioAprobacion());
        destino.setFechaObservacion(origen.getCabFichaSeccionD().getFechaObservacion());
        destino.setFechaAprobacion(origen.getCabFichaSeccionD().getFechaAprobacion());

        List<DetalleHistoricoSeccionDOutputDto> data = new ArrayList<>();
        for (DetFichaSeccionD detalle : detalles) {
            DetalleHistoricoSeccionDOutputDto ds = new DetalleHistoricoSeccionDOutputDto();
            ds.setIdDetFichaSeccionD(detalle.getIdDetFichaSeccionD());
            ds.setTipoArea(detalle.getTipoArea());
            ds.setDescripcionTipoArea(detalle.getDescripcionTipoArea());
            ds.setLargo(detalle.getLargo());
            ds.setAncho(detalle.getAncho());
            ds.setReferencia(detalle.getReferencia());
            ds.setTieneTecho(detalle.getTieneTecho());
            ds.setTienePuerta(detalle.getTienePuerta());
            ds.setCantidadPuerta(detalle.getCantidadPuerta());
            ds.setTieneAccesoLuz(detalle.getTieneAccesoLuz());
            data.add(ds);
        }
        destino.setLista(data);

        return destino;

    }
    
    public static final ObtenerLocalVotacionHistoricoSeccionEOutputDto ObtenerLocalLocalVotacionHistoricoSeccionEModelToDtoWrapper(MaeLocalVotacion origen, List<DetFichaSeccionE> detalles) {
        if (origen == null) {
            return null;
        }
        
        ObtenerLocalVotacionHistoricoSeccionEOutputDto destino = new ObtenerLocalVotacionHistoricoSeccionEOutputDto();
        destino.setIdFichaSeccionE(origen.getCabFichaSeccionE().getIdFichaSeccionE());
        destino.setSeccion(origen.getCabFichaSeccionE().getSeccion());
        destino.setObservacionLV(origen.getCabFichaSeccionE().getObservacionLV());
        destino.setObservacionRegistro(origen.getCabFichaSeccionE().getObservacionRegistro());
        
        List<DetalleHistoricoSeccionEOutputDto> data = new ArrayList<>();
        for (DetFichaSeccionE detalle : detalles) {
            DetalleHistoricoSeccionEOutputDto ds = new DetalleHistoricoSeccionEOutputDto();
            ds.setIdDetFichaSeccionE(detalle.getIdDetFichaSeccionE());
            ds.setArchivo(detalle.getArchivo());
            ds.setCategoria(detalle.getCategoria());
            ds.setDetalle(detalle.getDetalle());
            data.add(ds);
        }
        destino.setLista(data);
        
        return destino;
    }
}
