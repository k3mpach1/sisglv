package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import pe.gob.onpe.sisglvbackend.negocio.beans.ReporteRegistrosAmbitoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.beans.ReporteRegistrosPorRegistrador;
import pe.gob.onpe.sisglvbackend.negocio.beans.ReporteSeccionesSisglv;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ReporteService;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;

import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

import pe.gob.onpe.sisglvbackend.vista.dtos.input.reporte.ValidarClaveReporteInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte.*;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/reporte")
public class ReporteController {
    
    @Autowired
    private ReporteService reporteService;

    @ApiOperation(value = "reporte registros orc", notes = "reporte registros orc")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
   
    @GetMapping("/listar-reporte-registros-orc")
    public ResponseEntity<ResponseListarRegistrosOrcOuputDto> listarReporteRegistrosOrc() throws Exception {

    	ReporteRegistrosAmbitoElectoral reporte = new ReporteRegistrosAmbitoElectoral();
    	
    	reporteService.reporteRegistrosOrc(reporte);

        List<ListarRegistrosOrcOutputDto> outputDto = Funciones.mapAll(reporte.getReporte(), ListarRegistrosOrcOutputDto.class);

        DatosListarRegistrosOrcOuputDto datos = new DatosListarRegistrosOrcOuputDto();
        datos.setRegistros(outputDto);
        
        
        ResponseListarRegistrosOrcOuputDto response = new ResponseListarRegistrosOrcOuputDto();
        response.setResultado(reporte.getResultado());
        response.setMensaje( reporte.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "reporte registros por registrador", notes = "reporte registros por registrador")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
   
    @GetMapping("/listar-reporte-registros-registrador")
    public ResponseEntity<ResponseListarRegistrosRegistradorOuputDto> listarReporteRegistrosRegistrador() throws Exception {

    	ReporteRegistrosPorRegistrador reporte = new ReporteRegistrosPorRegistrador();
    	
    	reporteService.reporteRegistrosRegistrador(reporte);

        List<ListarRegistrosRegistradorOutputDto> outputDto = Funciones.mapAll(reporte.getReporte(), ListarRegistrosRegistradorOutputDto.class);

        DatosListarRegistrosRegistradorOuputDto datos = new DatosListarRegistrosRegistradorOuputDto();
        datos.setRegistros(outputDto);
        
        
        ResponseListarRegistrosRegistradorOuputDto response = new ResponseListarRegistrosRegistradorOuputDto();
        response.setResultado(reporte.getResultado());
        response.setMensaje( reporte.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "reporte secciones de sisglv", notes = "reporte secciones de sisglv")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
   
    @GetMapping("/listar-reporte-secciones-sisglv")
    public ResponseEntity<ResponseListarSeccionesSisglvOuputDto> listarReporteSeccionesSisglv() throws Exception {

    	ReporteSeccionesSisglv reporte = new ReporteSeccionesSisglv();
    	
    	reporteService.reporteRegistrosPorSeccion(reporte);

        List<ListarRegistrosSeccionesSisglvOutputDto> outputDto = Funciones.mapAll(reporte.getReporte(), ListarRegistrosSeccionesSisglvOutputDto.class);

        DatosListarSeccionesSislgvOuputDto datos = new DatosListarSeccionesSislgvOuputDto();
        datos.setRegistros(outputDto);
        
        
        ResponseListarSeccionesSisglvOuputDto response = new ResponseListarSeccionesSisglvOuputDto();
        response.setResultado(reporte.getResultado());
        response.setMensaje( reporte.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/listar-reporte-secciones-abcde")
    public ResponseEntity<?> listarReporteSeccionesAbcde() throws Exception {
        ListaReporteSeccionABCDEOutputDto lista = reporteService.reporteSeccionesABCDE();
        lista.setResultado(1);
        lista.setMensaje("");
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PostMapping("/validar-clave-reporte")
    public ResponseEntity<?> validarClaveReporte(@RequestBody ValidarClaveReporteInputDto validarClaveReporteInputDto) throws Exception {
        return new ResponseEntity<>(reporteService.validarClaveReporte(validarClaveReporteInputDto), HttpStatus.OK);
    }


}
