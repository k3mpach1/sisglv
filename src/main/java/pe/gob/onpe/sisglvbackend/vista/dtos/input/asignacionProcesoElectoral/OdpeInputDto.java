package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class OdpeInputDto {
	
	@NotNull(message = "El idOdpe no puede ser nulo")
	private Integer idOdpe;
	
	@NotNull(message = "El nombre no puede ser nulo")
	private String nombre;
	
	@NotNull(message = "El abreviatura no puede ser nulo")
	private String abreviatura;
	

}
