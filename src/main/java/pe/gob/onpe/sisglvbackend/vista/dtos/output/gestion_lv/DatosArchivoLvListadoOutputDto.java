package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosArchivoLvListadoOutputDto {

	private String  guid;
	private String  ruta;
	private String  nombre;
	private String  nombreOriginal;
	private String  formato;
	private String  peso;
	private Integer idCategoria;
	private String  categoria;
	
}
