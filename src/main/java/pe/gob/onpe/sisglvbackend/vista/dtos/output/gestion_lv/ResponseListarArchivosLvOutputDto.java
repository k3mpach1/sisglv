package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseListarArchivosLvOutputDto extends BaseOutputDto {

	private List<DatosArchivoLvObtenidoOutputDto> datos;
	
}
