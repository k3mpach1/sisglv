package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;


import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCentroPobladoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarUbigeoBasicoOutputDto;

@Getter
@Setter
public class ObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto {
	
	
	
	private String dniRegistrador;
	private String nombreApellidoRegistrador;
	
	private Integer idDependencia;
	private Integer idAmbitoElectoral;
	private Integer idTipoAmbitoElectoral;
	
	private ListarUbigeoBasicoOutputDto departamento;
	private ListarUbigeoBasicoOutputDto provincia;
	private ListarUbigeoBasicoOutputDto distrito;
	private ListarCentroPobladoBasicoOutputDto centroPoblado;
	
	private String nombreCentroPoblado;
	
	
	private String latitudDistrito;
	
	private String longitudDistrito;
	
	private String observacionRegistro;
}
