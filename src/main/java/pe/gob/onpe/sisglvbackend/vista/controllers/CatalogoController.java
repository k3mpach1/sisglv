package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCatalogo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCatalogoEstructura;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CatalogoServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteCatalogo;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarCatalogoBasicoOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCatalogoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarCatalogoBasicoOuputDto;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/catalogo")
public class CatalogoController {
    
    @Autowired
    CatalogoServicio catalogoServicio;

    @ApiOperation(value = "listar tipo área", notes = "lista los tipo de area")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
   
    @GetMapping("/listar-tipo-area")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarTipoArea() throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_AREA);
        param.setCabCatalogo(catalogo);
        param.setColumna(ConstanteCatalogo.N_TIPO_AREA);
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    
    @ApiOperation(value = "listar estados caracteristicas local", notes = "listar estados de caracateristicas local, los valores de codigoEstado son : "
    		+ "\n1 = ESTADO CERCO PERIMETRICO"
    		+ "\n2 = ESTADO PUERTA ACCESO"
    		+ "\n3 = ESTADO SSHH"
    		+ "\n4 = ESTADO PUERTA AULA"
    		+ "\n5 = ESTADO TECHO AULA"
    		+ "\n6 = ESTADO PARED AULA"
    		+"\n7 = ESTADO VENTANA AULA"
    		+ "\n8 = ESTADO PISO AULA")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-estado-caracateristica-local/{codigoEstado}")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarEstadoCaracteristicaLocal(@PathVariable Integer codigoEstado ) throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_ESTADOS);
        
        param.setCabCatalogo(catalogo);
        
        switch (codigoEstado) {
		case 1:
			param.setColumna(ConstanteCatalogo.N_ESTADO_CERCO_PERIMETRICO);
			break;
		case 2:
			param.setColumna(ConstanteCatalogo.N_ESTADO_PUERTA_ACCESO);
			break;
		case 3:
			param.setColumna(ConstanteCatalogo.N_ESTADO_SSHH);
			break;
		case 4:
			param.setColumna(ConstanteCatalogo.N_ESTADO_PUERTA_AULA);
			break;
		case 5:
			param.setColumna(ConstanteCatalogo.N_ESTADO_TECHO_AULA);
			break;
		case 6:
			param.setColumna(ConstanteCatalogo.N_ESTADO_PARED_AULA);
			break;
		case 7:
			param.setColumna(ConstanteCatalogo.N_ESTADO_VENTANA_AULA);
			break;
		case 8:
			param.setColumna(ConstanteCatalogo.N_ESTADO_PISO_AULA);
			break;
        
		default:
			break;
		}
        
        
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "listar tipo institución", notes = "listar tipo de institución")  
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )

    @GetMapping("/listar-tipo-institucion")
    //public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarTipoInstitucion() throws Exception {
    public ResponseEntity<?> listarTipoInstitucion() throws Exception {
        try {
            DetCatalogoEstructura param = new DetCatalogoEstructura();
            CabCatalogo catalogo = new CabCatalogo();
            catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_INSTITUCION);
            param.setCabCatalogo(catalogo);
            param.setColumna(ConstanteCatalogo.N_TIPO_INSTITUCION);
            catalogoServicio.listarCatalogosActivos(param);

            List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

            DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
            datos.setCatalogos(outputDto);


            ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
            response.setResultado(param.getResultado());
            response.setMensaje(param.getMensaje());
            response.setDatos(datos);

            return new ResponseEntity<ResponseListarCatalogoBasicoOuputDto>(response, HttpStatus.OK);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    
    @ApiOperation(value = "listar categoría de  institución", notes = "listar categoría de  institución") 
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-categoria-institucion")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarCategoriaInstitucion() throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_CATEGORIA_INSTITUCION);
        param.setCabCatalogo(catalogo);
        param.setColumna(ConstanteCatalogo.N_CATEGORIA_INSTITUCION);
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<ResponseListarCatalogoBasicoOuputDto>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "listar nivel de instrucción", notes = "listar nivel de  instrucción")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )    
    @GetMapping("/listar-nivel-instruccion")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarNivelInstrucción() throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_NIVEL_INSTRUCCION);
        param.setCabCatalogo(catalogo);
        param.setColumna(ConstanteCatalogo.N_NIVEL_INSTRUCCION);
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "listar código de dirección ", notes = "permite listar codigo de dirección codigoDireccion :"
    		+ "\n 1 = LISTA TIPO DE VIA"
    		+ "\n 2 = LISTA TIPO DE ZONA  ")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )    
    @GetMapping("/listar-codigos-direccion/{codigoDireccion}")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarCodigoDireccion(@PathVariable Integer codigoDireccion ) throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
       
        
        switch (codigoDireccion) {
		case 1:
			catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_VIA);
		    param.setCabCatalogo(catalogo);
			param.setColumna(ConstanteCatalogo.N_TIPO_VIA);
			break;
		case 2:
			catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_ZONA);
		    param.setCabCatalogo(catalogo);
			param.setColumna(ConstanteCatalogo.N_TIPO_ZONA);
			break;
		default:
			break;
		}
        
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "listar tipo de  local de votación", notes = "listar tipo de local de votación")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-tipo-lv")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarTipoLocalVotacion() throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_LV);
        param.setCabCatalogo(catalogo);
        param.setColumna(ConstanteCatalogo.N_TIPO_LV);
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "listar uso de aula", notes = "listar tipos  de uso de aula") 
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-uso-aula")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarUsoAula() throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_AULA);
        param.setCabCatalogo(catalogo);
        param.setColumna(ConstanteCatalogo.N_TIPO_AULA);
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "listar dependencias", notes = "listar dependencias")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-dependencia")
    public ResponseEntity<?> listarDependencia() throws Exception {
        try {
            DetCatalogoEstructura param = new DetCatalogoEstructura();
            CabCatalogo catalogo = new CabCatalogo();
            catalogo.setMaestro(ConstanteCatalogo.MAE_UNIDAD_ORGANICA);
            param.setCabCatalogo(catalogo);
            param.setColumna(ConstanteCatalogo.N_UNIDAD_ORGANICA);
            catalogoServicio.listarCatalogosActivos(param);

            List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

            DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
            datos.setCatalogos(outputDto);


            ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
            response.setResultado(param.getResultado());
            response.setMensaje(param.getMensaje());
            response.setDatos(datos);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    
    @ApiOperation(value = "listar tipo de  eleccion", notes = "listar tipo de elección, solución tecnologica a utilizar")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarCatalogoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-tipo-eleccion")
    public ResponseEntity<ResponseListarCatalogoBasicoOuputDto> listarTipoEleccion() throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_ELECCION);
        param.setCabCatalogo(catalogo);
        param.setColumna(ConstanteCatalogo.N_SOLUCION_TECNOLOGICA);
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @ApiOperation(value = "listar categoria tipo archivo", notes = "listar categoria tipo archivo")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarCatalogoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-categoria-tipo-archivo")
    public ResponseEntity<?> listarCategoriaTipoArchivo() throws Exception {

        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(ConstanteCatalogo.MAE_TIPO_ARCHIVO);
        param.setCabCatalogo(catalogo);
        param.setColumna(ConstanteCatalogo.N_CATEGORIA);
        catalogoServicio.listarCatalogosActivos(param);

        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);

        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        
        
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @ApiOperation(value = "Listar tipo Ámbito Electoral", notes = "Listar tipo Ámbito Electoral")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = ResponseListarCatalogoBasicoOuputDto.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
    })
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-tipo-ambito-electoral")
    public ResponseEntity<?> listarTipoAmbitoElectoral() throws Exception {
        ResponseListarCatalogoBasicoOuputDto response = catalogoServicio.listarCatalogosActivosPorMaestroYColumna(ConstanteCatalogo.MAE_TIPO_AMBITO_ELECTORAL,
                ConstanteCatalogo.N_TIPO_AMBITO_ELECTORAL);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }



    @ApiOperation(value = "Listar tipo Ámbito Geográfico", notes = "Listar tipo Ámbito Geográfico")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = ResponseListarCatalogoBasicoOuputDto.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
    })
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-tipo-ambito-geografico")
    public ResponseEntity<?> listarTipoAmbitoGeografico() throws Exception {
        ResponseListarCatalogoBasicoOuputDto response = catalogoServicio.listarCatalogosActivosPorMaestroYColumna(ConstanteCatalogo.MAE_TIPO_AMBITO_GEOGRAFICO,
                ConstanteCatalogo.N_TIPO_AMBITO_GEOGRAFICO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @GetMapping("/listar-estado-avance-seccion")
    public ResponseEntity<?> listarEstadoAvanceSeccion() throws Exception {
        ResponseListarCatalogoBasicoOuputDto response = catalogoServicio.listarCatalogosActivosPorMaestroYColumna(
        		ConstanteCatalogo.MAE_ESTADO_AVANCE_REGISTRO_LV,
                ConstanteCatalogo.N_ESTADO_AVANCE_SECCION);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @GetMapping("/listar-estado-verificacion")
    public ResponseEntity<?> listarEstado() throws Exception {
        ResponseListarCatalogoBasicoOuputDto response = catalogoServicio.listarCatalogosActivosPorMaestroYColumna(
        		ConstanteCatalogo.MAE_ESTADO_LOCAL_VOTACION,
                ConstanteCatalogo.N_ESTADO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @GetMapping("/listar-estado-lv")
    public ResponseEntity<?> listarEstadoLv() throws Exception {
        ResponseListarCatalogoBasicoOuputDto response = catalogoServicio.listarCatalogosActivosPorMaestroYColumna(
        		ConstanteCatalogo.MAE_ESTADO_LOCAL_VOTACION,
                ConstanteCatalogo.N_ESTADO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
