package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarCatalogoBasicoOutputDto {
	
	private Integer codigo;
	private String  nombre;
	private Integer  activo;
	private Integer orden;
	
	
}
