package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarCaracteristicasSegundoPasoLVInputDto {
	/*
	@NotNull(message = "Id Verificación es obligatorio")
	private Integer idVerificacion;
	*/
	@NotNull(message = "Id Registro Sección B es obligatorio")
	private Integer idRegistroSeccionB;
	private Integer tienePatio;
	private Integer cantidadPatios;
	private Integer idEstadoConservacionPuertasAulas;
	private Integer idEstadoConservacionTechosAulas;
	private Integer idEstadoConservacionParedesAulas;
	private Integer idEstadoConservacionVentanasAulas;
	private Integer idEstadoConservacionPisosAulas;
	private String suministroEnergiaElectrica;
	private Integer cantidadCirculoSeguridad;
	private Float areaAproximadaM2;
	private Float areaConstruidaM2;
	private Float areaSinConstruirM2;
}
