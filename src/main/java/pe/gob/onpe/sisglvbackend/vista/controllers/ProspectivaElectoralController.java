package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.onpe.sisglvbackend.negocio.modelos.*;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProspectivaServicio;
import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.EstadoConfigPEInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral.ProyeccionParte1InputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.prospectivaelectoral.ProyeccionParte2InputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.EstadoConfigPEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.DatosPeriodoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.EjecutarResultadoProspectivaDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.EjecutarResultadoProspectivaExtranjeroDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.EjecutarResultadoProspectivaNacionalDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.ResponseEjecutarResultadoProspectiva;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral.ResponseListarPeriodos;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@CrossOrigin(origins = "*")
@RestController
//@Controller
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/prospectiva-electoral")
@Validated
public class ProspectivaElectoralController {

	@Autowired
	JWTTokenProvider jwtTokenProvider;
	
	@Autowired
	private ProspectivaServicio prospectivaServicio;
	
	@ApiOperation(value = "listar periodos", notes = "Permite listar periodos")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarPeriodos.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-periodos")
    public ResponseEntity<?> listarPeriodos(
            //@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token
            ) throws Exception {

		ResponseListarPeriodos response = new ResponseListarPeriodos();
		List<DatosPeriodoOutputDto> datos = new ArrayList<DatosPeriodoOutputDto>();
		DatosPeriodoOutputDto data = null;
		
		Periodo periodo = new Periodo();
		prospectivaServicio.listarPeriodos(periodo);
		
		if(periodo.getPeriodos()!=null) {
			for(Periodo _periodo:periodo.getPeriodos()) {
				data = new DatosPeriodoOutputDto();
				data.setId(_periodo.getId());
				data.setPeriodo(_periodo.getEtiqueta());
				data.setOrden(_periodo.getOrden());
				datos.add(data);
			} // end-for
		}
		response.setDatos(datos);
		response.setMensaje(periodo.getMensaje());
		response.setResultado(periodo.getResultado());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

	@PostMapping(value = "/proyeccion-parte-1")
	public ResponseEntity<?> proyeccionParte1(@RequestBody ProyeccionParte1InputDto proyeccionParte1InputDto) throws Exception {
		try {
			return ResponseEntity.ok(prospectivaServicio.proyeccionParte1(proyeccionParte1InputDto));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@PostMapping(value = "/proyeccion-parte-2")
	public ResponseEntity<?> proyeccionParte2(@RequestBody ProyeccionParte2InputDto proyeccionParte2InputDto) throws Exception {
		try {
			System.err.println(new Gson().toJson(proyeccionParte2InputDto));
			return ResponseEntity.ok(prospectivaServicio.proyeccionParte2(proyeccionParte2InputDto));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/obtener-estadistica-resultado")
	public ResponseEntity<?> obtenerEstadisticaResultado(@RequestBody ProspectivaEstadisticaResultado prospectivaEstadisticaResultado) throws Exception{
		try {
			return ResponseEntity.ok(prospectivaServicio.obtenerEstadisticaResultado(prospectivaEstadisticaResultado));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/obtener-proyeccion-resultado")
	public ResponseEntity<?> obtenerProyeccionResultado(@RequestBody ProspectivaProyeccionResultado prospectivaProyeccionResultado) throws Exception {
		try {
			System.err.println(new Gson().toJson(prospectivaProyeccionResultado));
			return ResponseEntity.ok(prospectivaServicio.obtenerProyeccionResultado(prospectivaProyeccionResultado));
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}






	
	@ApiOperation(value = "Ejecutar resultado de la prospectiva", notes = "Permite ejecutar resultado de la prospectiva")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarPeriodos.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/ejecutar-resultado-prospectiva/{id}")
    public ResponseEntity<?> ejecutarResultadoProspectiva(
            //@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token
    		@PathVariable(name = "id") Long id) throws Exception {

		ResponseEjecutarResultadoProspectiva response = new ResponseEjecutarResultadoProspectiva();
		EjecutarResultadoProspectivaDto data = new EjecutarResultadoProspectivaDto();
		
		EjecutarResultadoProspectivaNacionalDto dataNacional = null;
		List<EjecutarResultadoProspectivaNacionalDto> datasetNacional = new ArrayList<EjecutarResultadoProspectivaNacionalDto>();
		ProspectivaResultadoNacional prospectivaNacional = new ProspectivaResultadoNacional();
		prospectivaNacional.setIdProyeccion(id);
		prospectivaServicio.ejecutarResultadoNacional(prospectivaNacional);
		if(prospectivaNacional.getListadoResultadoNacional()!=null) {
			for(ProspectivaResultadoNacional _pn:prospectivaNacional.getListadoResultadoNacional()) {
				dataNacional = new EjecutarResultadoProspectivaNacionalDto();
				dataNacional.setUbigeo(_pn.getUbigeo());
				dataNacional.setDepartamento(_pn.getDepartamento());
				dataNacional.setProvincia(_pn.getProvincia());
				dataNacional.setDistrito(_pn.getDistrito());
				dataNacional.setCv(_pn.getCv());
				dataNacional.setDesvEst(_pn.getDesvEst());
				dataNacional.setPromedio(_pn.getPromedio());
				datasetNacional.add(dataNacional);
			} // end-for
		} // end-if
		
		
		EjecutarResultadoProspectivaExtranjeroDto dataExtranjero = null;
		List<EjecutarResultadoProspectivaExtranjeroDto> datasetExtranjero = new ArrayList<EjecutarResultadoProspectivaExtranjeroDto>();
		ProspectivaResultadoExtranjero prospectivaExtanjero = new ProspectivaResultadoExtranjero();
		prospectivaExtanjero.setIdProyeccion(id);
		prospectivaServicio.ejecutarResultadoExtranjero(prospectivaExtanjero);
		if(prospectivaExtanjero.getListadoResultadoExtranjero()!=null) {
			for(ProspectivaResultadoExtranjero _pe:prospectivaExtanjero.getListadoResultadoExtranjero()) {
				dataExtranjero = new EjecutarResultadoProspectivaExtranjeroDto();
				dataExtranjero.setUbigeo(_pe.getUbigeo());
				dataExtranjero.setDepartamento(_pe.getDepartamento());
				dataExtranjero.setProvincia(_pe.getProvincia());
				dataExtranjero.setDistrito(_pe.getDistrito());
				dataExtranjero.setCv(_pe.getCv());
				dataExtranjero.setDesvEst(_pe.getDesvEst());
				dataExtranjero.setPromedio(_pe.getPromedio());
				datasetExtranjero.add(dataExtranjero);
			} // end-for
		} // end-if
		
		data.setIdProyeccion(id);
		data.setProspectivaExtranjero(datasetExtranjero);
		data.setProspectivaNacional(datasetNacional);
		response.setDatos(data);
		response.setMensaje(prospectivaExtanjero.getMensaje());
		response.setResultado(prospectivaExtanjero.getResultado());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	
}
