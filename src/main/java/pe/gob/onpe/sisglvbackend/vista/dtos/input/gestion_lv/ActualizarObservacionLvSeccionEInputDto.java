package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarObservacionLvSeccionEInputDto {

	@NotNull(message = "El número de verificacion es obligatorio")
	private Integer nverificacion;
	
	@NotNull(message = "La sección es obligatorio")
	private String seccion;

	private String observacionLv;
}
