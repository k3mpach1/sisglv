package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerLocalVotacionHistoricoSeccionCOutputDto {

    private Integer idFichaSeccionC;
    private String seccion;
    private Integer cantidadAula;
    private Integer cantidadMesa;
    private String observacionRegistro;
    private List<DetalleHistoricoSeccionCOutputDto> lista = new ArrayList<>();

}
