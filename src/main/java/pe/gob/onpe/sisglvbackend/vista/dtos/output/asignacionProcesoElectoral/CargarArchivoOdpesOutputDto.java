package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.*;
import pe.gob.onpe.sisglvbackend.negocio.beans.OdpeOcrExcel;

import java.util.List;

/**
 * @author glennlq
 * @created 9/15/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CargarArchivoOdpesOutputDto {
    private Integer idArchivo;
    private String guidArchivo;
    private String nombreArchivo;
    private String formatoArchivo;
    private String pesoArchivo;
    private String nombreOriginalArchivo;
}
