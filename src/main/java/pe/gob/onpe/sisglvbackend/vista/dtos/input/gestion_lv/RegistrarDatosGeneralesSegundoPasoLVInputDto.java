package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarDatosGeneralesSegundoPasoLVInputDto {
	
	@NotNull(message = "Id Registro Seccion A es obligatorio")
	private Integer idRegistroSeccionA;
	
	@NotNull(message = "Id Verificación es obligatorio")
	private Integer idVerificacion;
	
	@NotBlank(message = "D.N.I del Registrador es obligatorio")
	private String dniRegistrador;
	@NotBlank(message = "Nombre y Apellido Registrados es obligatorio")
	private String nombreApellidoRegistrador;
	
	
	@NotNull(message = "IdDependencia es obligatorio")
	private Integer idDependencia;
	
	private Integer idTipoAmbitoElectoral;
	private Integer idAmbitoElectoral;
	
	private String nombreDependencia;
		
	private Integer idOdpe;
	private String nombreOdpe;
	
	private Integer idOrc;
	private String nombreOrc;
	

	private String codigoUbigeo;
	
	@NotNull(message = "Id Ubigeo es obligatorio")	
	private Integer idUbigeo;
	
	
	private String nombreDepartamento;
	private String nombreProvincia;
	private String nombreDistrito;
	
	
	private Integer idCentroPoblado;
	
	
	private String nombreCentroPoblado;
	

	private String latitudDistrito;

	private String longitudDistrito;
	
}
