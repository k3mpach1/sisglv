package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;




import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RegistrarDetRegistroSeccionDInputDto {
	@NotBlank(message = "Sección es obligatorio")
	private String seccion;
	@NotNull(message = "ID verificación es obligatorio")
	private Integer idVerificacionLV;
	
	@NotNull(message = "Tipo de área es obligatorio")
	private Integer tipoArea;
	
	@NotNull(message = "Largo es obligatorio")
	private BigDecimal largo;
	
	@NotNull(message = "Largo es obligatorio")
	private BigDecimal ancho;
	
	@Size(max = 250)
	@NotBlank(message = "Referencia de la ubicación es obligatorio")
	private String referencia;
	
	@NotNull(message = "Tiene techo es obligatorio")
	private Integer tieneTecho;
	
	@NotNull(message = "Tiene puerta es obligatorio")
	private Integer tienePuerta;
	
	private Integer cantidadPuerta;
	
	@NotNull(message = "Tiene acceso a energía eléctrica es obligatorio")
	private Integer tieneAccesoLuz;
	
	@Size(max = 2500)
	private String observacion;
	
	
	
}
