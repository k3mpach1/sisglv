package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerLocalVotacionHistoricoSeccionDOutputDto {

    private Integer idFichaSeccionD;
    private String seccion;
    private String observacionRegistro;
    private String usuarioObservacion;
    private String usuarioAprobacion;
    private Date fechaObservacion;
    private Date fechaAprobacion;
    
    private List<DetalleHistoricoSeccionDOutputDto> lista = new ArrayList<>();

}
