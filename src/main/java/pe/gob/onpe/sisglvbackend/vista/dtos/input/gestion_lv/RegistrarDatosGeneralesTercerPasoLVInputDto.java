package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarDatosGeneralesTercerPasoLVInputDto {
	@NotNull(message = "Id Registro Seccion A es obligatorio")
	private Integer idRegistroSeccionA;
	
	private Integer idTipoInstitucion;
	private Integer idCategoriaInstitucion;
	private String otraCategoria;
	private Integer idNivel;
	private Integer idTipoVia;
	private String nombreVia;
	private Integer numero;
	private Float kilometro;
	private String manzana;
	private String interior;
	private String departamento;
	
	private String lote;
	private Integer piso;
	private Integer idTipoZona;
	private String nombreZona;
	private String referencia;
	private String latitudPuertaAcceso;
	private String longitudPuertaAcceso;
	private Float distanciaCentroDistrito;
	
//	@Pattern(regexp = "[0-9]{2}:[0-9]{2}$",message = "Formato Incorrecto (HH:mm)")
	private String tiempoCentroDistrito;
}
