package pe.gob.onpe.sisglvbackend.vista.dtos.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseOutputDto {
	private String mensaje;
	private Integer resultado;
}
