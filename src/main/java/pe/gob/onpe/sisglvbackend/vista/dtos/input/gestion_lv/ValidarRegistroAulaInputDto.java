package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidarRegistroAulaInputDto {

	@ApiModelProperty(notes = "id de verificación es obligatorio", example = "1", required = true)
	@NotNull(message = "id de verificación es obligatorio")
	private Integer idVerificacionLv;
	
	

	
}
