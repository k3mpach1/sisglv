package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;




import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RegistrarDetRegistroSeccionCInputDto {
	@NotBlank(message = "Sección es obligatorio")
	@Size(max = 1)
	private String seccion;
	@NotBlank(message = "Número de aula es obligatorio")
	private String aula;
	@NotBlank(message = "Pabellón es obligatorio")
	@Size(max = 10, message = "Pabellón debe tener máximo 10 caracteres.")
	private String pabellon;
	@NotNull(message = "Número de piso es obligatorio")
	private Integer piso;
	@NotNull(message = "Requiere Toldo es obligatorio")
	private Integer requiereToldo;
	@NotNull(message = "ID verificación es obligatorio")
	private Integer idVerificacionLV;
	
	private Integer cantidadMesa;
	@NotNull(message = "Tipo de aula es obligatorio")
	private Integer tipoAula;
	
	@NotNull(message = "Tiene luminaria es obligatorio")
	private Integer tieneLuminaria;
	@Range(min = 0, max = 9999)
	private Integer cantidadLuminariaBuenEstado;
	@Range(min = 0, max = 9999)
	private Integer cantidadLuminariaMalEstado;
	
	@NotNull(message = "Tiene tomacorriente es obligatorio")
	private Integer tieneTomaCorriente;
	@Range(min = 0, max = 9999)
	private Integer cantidadTomacorrienteBuenEstado;
	@Range(min = 0, max = 9999)
	private Integer cantidadTomacorrienteMalEstado;
	@NotNull(message = "Tiene interruptor es obligatorio")
	private Integer tieneInterruptor;
	@Range(min = 0, max = 9999)
	private Integer cantidadInterruptorBuenEstado;
	@Range(min = 0, max = 9999)
	private Integer cantidadInterruptorMalEstado;
	
	@NotNull(message = "Tiene puerta es obligatorio")
	private Integer tienePuerta;
	@Range(min = 0, max = 9999)
	private Integer cantidadPuertaBuenEstado;
	@Range(min = 0, max = 9999)
	private Integer cantidadPuertaMalEstado;
	
	@NotNull(message = "Tiene ventana es obligatorio")
	private Integer tieneVentana;
	@Range(min = 0, max = 9999)
	private Integer cantidadVentanaBuenEstado;
	@Range(min = 0, max = 9999)
	private Integer cantidadVentanaMalEstado;
	
	@NotNull(message = "Tiene punto de internet es obligatorio")
	private Integer tienePuntoInternet;
	private Integer tienePuntoWifi;
	
}
