package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.*;

/**
 * @author glennlq
 * @created 12/14/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstadoTerminadoNuevoLocalOutputDto {
    Integer idVerificacionLv;
    Integer terminadoA;
    Integer terminadoB;
    Integer terminadoC;
    Integer terminadoD;
    Integer termiandoE;
    Integer estado;
}
