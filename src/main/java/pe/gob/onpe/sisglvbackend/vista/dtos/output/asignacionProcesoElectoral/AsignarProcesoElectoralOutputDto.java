package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

/**
 * @author glennlq
 * @created 9/15/21
 */

@Getter
@Setter
public class AsignarProcesoElectoralOutputDto {
    private Integer idProcesoElectoral;
    private Integer idProcesoElectoralPadre;
    private String idProceso;
    private String nombre;
    private String acronimo;
    private Date fechaConvocatoria;
    private Integer tipoAmbitoElectoral;
    private Integer tipoAmbitoGeografico;
    private Integer base;
    private String correlativo;
    private Integer etapa;
    private String estado;
}
