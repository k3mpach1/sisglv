package pe.gob.onpe.sisglvbackend.utils;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class ReporteUtil {

    public byte[] buildReport(HttpServletRequest request, Map<String, Object> parameters,
                              List<?> list,
                              String reportName) throws Exception {
        ServletContext sc = request.getSession().getServletContext();
        InputStream inStream = sc.getResourceAsStream(reportName);
        JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(list);
        JasperPrint jasperPrint = JasperFillManager.fillReport(inStream, parameters, list.isEmpty() ? new JREmptyDataSource() : collectionDataSource);
        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

    public InputStream buildReportStream(HttpServletRequest request, Map<String, Object> parameters,
                              List<?> list,String reportName) throws Exception {
        ServletContext sc = request.getSession().getServletContext();
        InputStream inStream = sc.getResourceAsStream(reportName);
        JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(list);
        JasperPrint jasperPrint = JasperFillManager.fillReport(inStream, parameters, list.isEmpty() ? new JREmptyDataSource() : collectionDataSource);
        return new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jasperPrint));
    }

    public InputStream getImage(String pathImage) throws Exception {
        File logoFile = new ClassPathResource(pathImage).getFile();
        return new FileInputStream(logoFile);
    }

    public void renderFile(HttpServletResponse response, byte[] datos, String filename) throws Exception {
        response.addHeader("Content-Disposition", "attachment; filename="+filename);
        response.setContentType("application/pdf");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setContentLength(datos.length);
        InputStream inputStream = new ByteArrayInputStream(datos);
        FileCopyUtils.copy(inputStream, response.getOutputStream());
        response.getOutputStream().close();
        response.getOutputStream().flush();
    }

    public void writeFile(byte[] datos, String pathBase, String filename) throws Exception {
        File newFile = new File(pathBase+"temporal", filename);
        if(!newFile.exists()) {
            FileUtils.writeByteArrayToFile(newFile,datos);
        } else
            throw new Exception("El archivo ya existe.");
    }

    public boolean verificarExisteReporteGenerado(String pathBase, String filename) throws Exception {
        File reporte = new File(pathBase+"temporal", filename);
        if(reporte.exists() && reporte.length() > 0) {
            return true;
        }
        return false;
    }

    public byte[] readReporteGenerado(String pathBase, String filename) throws Exception {
        File reporte = new File(pathBase+"temporal", filename);
        return FileUtils.readFileToByteArray(reporte);
    }



    public void renderFileExcel(HttpServletResponse response, InputStream inputStream, String filename) throws Exception {
        response.setHeader("Content-Disposition", "attachment;filename="+filename);
        response.setContentType("application/octet-stream");

        //response.addHeader("Content-Disposition", "attachment; filename="+filename);
        //response.setContentType("application/pdf");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setContentLength(inputStream.available());
        //InputStream inputStream = new ByteArrayInputStream(datos);
        FileCopyUtils.copy(inputStream, response.getOutputStream());
        response.getOutputStream().close();
        response.getOutputStream().flush();
    }

    public void renderFile(HttpServletResponse response, InputStream datos, String filename) throws Exception {
        response.addHeader("Content-Disposition", "attachment; filename="+filename);
        response.setContentType("application/pdf");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setContentLength(datos.available());
        FileCopyUtils.copy(datos, response.getOutputStream());
        response.getOutputStream().close();
        response.getOutputStream().flush();
    }

    public String getTodayStr() {
        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public InputStream mergePdf(List<InputStream> lista) throws Exception {
        ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
        PDFMergerUtility ut = new PDFMergerUtility();
        ut.addSources(lista);
        ut.setDestinationStream(bOutput);
        ut.mergeDocuments();

        ByteArrayInputStream bIntput = new ByteArrayInputStream(bOutput.toByteArray());
        return bIntput;
    }

    public String getCurrentTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now); //2016/11/16 12:08:43
    }

    public InputStream buildReportStreamExcel(HttpServletRequest request,
                                              Map<String, Object> parameters,
                                              List<?> list,
                                              String reportName) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ServletContext sc = request.getSession().getServletContext();
            InputStream inStream = sc.getResourceAsStream(reportName);
            JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(list);
            JasperPrint jasperPrint = JasperFillManager.fillReport(inStream, parameters, list.isEmpty() ? new JREmptyDataSource() : collectionDataSource);

            List<JasperPrint> asd = new ArrayList<>();
            asd.add(jasperPrint);

            JRXlsxExporter exporter = new JRXlsxExporter();
            SimpleXlsxReportConfiguration reportConfigXLS = new SimpleXlsxReportConfiguration();
            reportConfigXLS.setOnePagePerSheet(true);
            reportConfigXLS.setIgnoreGraphics(false);
            reportConfigXLS.setSheetNames(new String[]{"Hoja1"});
            exporter.setConfiguration(reportConfigXLS);
            //exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterInput(SimpleExporterInput.getInstance(asd));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
            exporter.exportReport();
        } catch (Exception e) {

        } finally {
            try{
                if(byteArrayOutputStream!=null){
                    byteArrayOutputStream.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public JasperPrint fillReportJasperPrint(HttpServletRequest request,
                                              Map<String, Object> parameters,
                                              List<?> list,
                                              String reportName,
                                              ResourceLoader resourceLoader) throws Exception {
         ServletContext sc = request.getSession().getServletContext();
         InputStream inStream = sc.getResourceAsStream(reportName);
//        Resource resource = resourceLoader.getResource("classpath:/"+reportName);
//        InputStream inStream = resource.getInputStream();
        JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(list);
        return  JasperFillManager.fillReport(inStream, parameters, list.isEmpty() ? new JREmptyDataSource() : collectionDataSource);
    }

    public JasperPrint fillReportJasperPrint(HttpServletRequest request,
                                             Map<String, Object> parameters,
                                             List<?> list,
                                             String reportName, String pathBase) throws Exception {
        File report = new File(pathBase, reportName);
        InputStream inStream = new FileInputStream(report);
        JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(list);
        return  JasperFillManager.fillReport(inStream, parameters, list.isEmpty() ? new JREmptyDataSource() : collectionDataSource);
    }


    public byte[] exportReport(JasperPrint jasperPrint) throws Exception {
        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

    public InputStream exportReportStream(JasperPrint jasperPrint) throws Exception {
        return new ByteArrayInputStream(JasperExportManager.exportReportToPdf(jasperPrint));
    }

    public InputStream exportReportExcelStream(List<JasperPrint> jasperPrintList,
                                              String[] names) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            JRXlsxExporter exporter = new JRXlsxExporter();
            SimpleXlsxReportConfiguration reportConfigXLS = new SimpleXlsxReportConfiguration();
            reportConfigXLS.setIgnoreGraphics(false);
            reportConfigXLS.setSheetNames(names);
            reportConfigXLS.setRemoveEmptySpaceBetweenRows(true);
            exporter.setConfiguration(reportConfigXLS);
            exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
            exporter.exportReport();
        } catch (Exception e) {
        } finally {
            try{
                if(byteArrayOutputStream!=null){
                    byteArrayOutputStream.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }


    public static String getNewRandomToken(int length) {
        String randomString = "";
        try {
            SecureRandom random = new SecureRandom();
            byte bytes[] = new byte[32];
            random.nextBytes(bytes);
            Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
            randomString = encoder.encodeToString(bytes);
        } catch (Exception e) {
            randomString = String.valueOf(System.currentTimeMillis());
        }
        return randomString.substring(0, length);
    }

}
